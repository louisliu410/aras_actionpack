'Developed by Kenny at Broadway
'Create Date: 2016/03/02

'System.Diagnostics.Debugger.Break() 
Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)

Dim LbcsAPV4 As bcsActionPack.ActionPack 
Dim LasInnovator As Innovator 
Dim LbolRetValue As Boolean


Try
	LasInnovator=Me.getInnovator()

	LbcsAPV4 =New  bcsActionPack.ActionPack(LasInnovator)
	LbolRetValue = LbcsAPV4.UpdateActivity2TeamIdByProject(Me)
	If LbolRetValue=false Then
    	If (PermissionWasSet=True) Then
    		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
    	End If	    
	    Throw New Exception(LbcsAPV4.ErrMessage)
	End If

Catch ex As Exception
	Throw New Exception(ex.Message)
Finally
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If
End Try

Return Me

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_UpdateActivity2TeamIdByPrj' and [Method].is_current='1'">
<config_id>B81BAB5308EC47B0A82B3146F0488C0A</config_id>
<name>bcs_UpdateActivity2TeamIdByPrj</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
