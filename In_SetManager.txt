string strMethodName = "In_SetManager";
/*
目的:將Owner的的一階主管帶到本物件的Manager上
位置:onAfterUpdate, onAfterAdd

*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

try
{
	if(this.getProperty("owned_by_id","")=="")
		return this;

	aml = "<AML>";
	aml += "<Item type='Identity' action='get' select='owned_by_id,in_leaderrole'>";
	aml += "<id>" + this.getProperty("owned_by_id","") + "</id>";
	aml += "</Item></AML>";
	Item itmOwner = inn.applyAML(aml);

	sql = "Update [" + this.getType().Replace(" ","_") + "] set managed_by_id='" + itmOwner.getProperty("in_leaderrole","") + "' where id='" + this.getID() + "'";

	itmR = inn.applySQL(sql);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetManager' and [Method].is_current='1'">
<config_id>F658AFD4377D4E72A2D211FF66FDBF7F</config_id>
<name>In_SetManager</name>
<comments>將Owner的的一階主管帶到本物件的Manager上</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
