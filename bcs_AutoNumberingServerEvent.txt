/*
 * 設定方式
 * Item Type : Numbering ItemType
 * Server Events : onAfterAdd
 */

//Modify by kenny 2016/02/21 改為:bcs_AutoNumberingBeforeEvent


Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Aras.Server.Security.Identity historyIdentity = Aras.Server.Security.Identity.GetByName("History Daemon");
Boolean PermissionWasSet2 = Aras.Server.Security.Permissions.GrantIdentity(historyIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	bcsSequence.AutoNumbering _AutoNumbering = new bcsSequence.AutoNumbering(inn);
	Item itmResult = _AutoNumbering.BCSAutoNumbering(this);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	if (PermissionWasSet2) Aras.Server.Security.Permissions.RevokeIdentity(historyIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_AutoNumberingServerEvent' and [Method].is_current='1'">
<config_id>8BBD716595F84BA989E044C7F996437D</config_id>
<name>bcs_AutoNumberingServerEvent</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
