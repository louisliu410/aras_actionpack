/*
 * Run Server Method
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	string strAct2TimeoutMessageName = "bcs_Activity2Timeout_zt";
	
	string strResult = "";
	
	bcsNotificationLite.Notification _Notification = new bcsNotificationLite.Notification(inn);
	
	strResult = _Notification.SendAct2Timeout(strAct2TimeoutMessageName);
	
	return inn.newResult(strResult);
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_SendAct2Timeout_zt' and [Method].is_current='1'">
<config_id>39E137ABF163425C88F40506DF565BDD</config_id>
<name>bcs_SendAct2Timeout_zt</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
