// URL can be something like this: 
// $[HTTP_PREFIX_SERVER]$[HTTP_HOST_SERVER]$[HTTP_PORT_SERVER]$[HTTP_PATH_SERVER]/Server/innovatorserver.aspx
// In this case need a special request to server to transform it using server settings.
int count = this.getItemCount();
for (int i = 0; i < count; i ++)
{
	Item item = this.getItemByIndex(i);
	string prop = item.getProperty("vault_url_pattern", null);
	if (prop != null)
	{
		XmlDocument inDom = new XmlDocument();
		XmlDocument outDom = new XmlDocument();
		inDom.XmlResolver = null;
		outDom.XmlResolver = null;
		inDom.LoadXml(string.Format("<url>{0}</url>", prop));
		outDom.LoadXml("<Empty />");
		CCO.Utilities.TransformVaultServerURL(inDom, outDom);
		
		Item resItem = this.newItem();
		resItem.loadAML(outDom.OuterXml);
		if(resItem.isError())
		{
	      return resItem;
		}
		
		string result = outDom.SelectSingleNode(Item.XPathResult).InnerText;
		item.setProperty("vault_url", result);
	}
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Set Federated Prop vault_url' and [Method].is_current='1'">
<config_id>4935C1E3CA954A8CBEC4E1586EB93F37</config_id>
<name>Set Federated Prop vault_url</name>
<comments>Transform vault_url_pattern property into vault_url federated property (IR-014890)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
