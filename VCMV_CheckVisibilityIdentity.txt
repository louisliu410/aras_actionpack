var identityId = this.getProperty("default_visibility_identity");
if (!String.IsNullOrEmpty(identityId))
{
    Item identity = this.newItem("Identity", "get");
	identity.setProperty("id", identityId);
	identity = identity.apply();
	
	if (identity.isError())
	{
		throw new Exception(identity.getErrorString());
	}
	
	var isAlias = identity.getProperty("is_alias");
	if(isAlias == "1")
	{
	    throw new ArgumentException(CCO.ErrorLookup.Lookup("SSVC_DefaultVisibilityIdentity"));
	}
	
	var classification = identity.getProperty("classification");
	if (classification == "System" || classification == "Team")
	{
	    throw new ArgumentException(CCO.ErrorLookup.Lookup("SSVC_SystemAndTeamIdentity"));
	}
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VCMV_CheckVisibilityIdentity' and [Method].is_current='1'">
<config_id>5EE02D4A89CC463A94CED8C91308CCA3</config_id>
<name>VCMV_CheckVisibilityIdentity</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
