Innovator innovatorInstance = this.getInnovator();
String schemaId = this.getProperty("xml_schema");
String languageCode = this.getProperty("language_code");
Item generationResult = innovatorInstance.newResult("");

if (!String.IsNullOrEmpty(schemaId))
{
	String generationAction = this.getProperty("generationAction");
	if (!String.IsNullOrEmpty(generationAction))
	{
		System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
		Aras.TDF.Base.SchemaElementFactory factory = CreateFactory(this, innovatorInstance, schemaId);
		factory.CustomContentEnabled = true;
		factory.DefaultLanguageCode = !String.IsNullOrEmpty(languageCode) ? languageCode : "en";

		switch (generationAction)
		{
			case "new":
				String elementNameProperty = this.getProperty("element_name");
				if (!String.IsNullOrEmpty(elementNameProperty))
				{
					Aras.TDF.Base.DocumentSchemaNode newElement = factory.NewElement(elementNameProperty);
					generationResult = innovatorInstance.newResult(factory.ExportElementToXml(newElement));
				}

				break;
			case "static":
				String elementNodeProperty = this.getProperty("element_node");
				if (!String.IsNullOrEmpty(elementNodeProperty))
				{
					String referenceNodesProperty = this.getProperty("reference_nodes");
					if (!String.IsNullOrEmpty(referenceNodesProperty)) {
						List<String> referenceNodesXmlList = jsSerializer.Deserialize<List<String>>(referenceNodesProperty);

						foreach (String referenceXml in referenceNodesXmlList) {
							factory.AddReference(referenceXml);
						}
					}

					Aras.TDF.Base.DocumentSchemaNode targetElement = factory.ParseElement(elementNodeProperty);
					factory.GenerateCustomContent((Aras.TDF.Base.DocumentSchemaElement) targetElement);

					generationResult = innovatorInstance.newResult(factory.ExportElementToXml(targetElement));
				}
				else
				{
					generationResult = innovatorInstance.newError(CCO.ErrorLookup.Lookup("tp_SpecifyInputData"));
				}

				break;
			case "dynamic":
				String elementNodesProperty = this.getProperty("element_nodes");
				if (!String.IsNullOrEmpty(elementNodesProperty))
				{
					String referenceNodesProperty = this.getProperty("reference_nodes");
					if (!String.IsNullOrEmpty(referenceNodesProperty)) {
						List<String> referenceNodesXmlList = jsSerializer.Deserialize<List<String>>(referenceNodesProperty);

						foreach (String referenceXml in referenceNodesXmlList) {
							factory.AddReference(referenceXml);
						}
					}

					List<String> elementNodesXmlList = jsSerializer.Deserialize<List<String>>(elementNodesProperty);
					List<Aras.TDF.Base.DocumentSchemaNode> elementsList = new List<Aras.TDF.Base.DocumentSchemaNode>();

					foreach (String elementXml in elementNodesXmlList) {
						elementsList.Add(factory.ParseElement(elementXml));
					}

					generationResult = innovatorInstance.newResult(factory.ExportElementsToXml(elementsList));
				}
				else
				{
					generationResult = innovatorInstance.newError(CCO.ErrorLookup.Lookup("tp_SpecifyInputData"));
				}

				break;
		}
	}
}
else
{
	generationResult = innovatorInstance.newError(CCO.ErrorLookup.Lookup("tp_SpecifySchemaId"));
}

return generationResult;
		}

		public static Aras.TDF.Base.SchemaElementFactory CreateFactory(Item thisItem, Innovator innovatorInstance, string schemaId)
		{
			if (thisItem == null)
			{
				throw new ArgumentException("thisItem should be not null");
			}

			var executionContext = new Aras.TDF.Base.SchemaElementExecutionContext(thisItem.getProperty("document_id"));
			var factory = new Aras.TDF.Base.SchemaElementFactory(innovatorInstance, schemaId, executionContext);
			return factory;
			
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_GenerateElementContent' and [Method].is_current='1'">
<config_id>DA594B9D910248F5888FC6CB24AC4DB1</config_id>
<name>tp_GenerateElementContent</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
