var rootItem = parent.thisItem;
var rootType = rootItem.getProperty('root_type');

if (!rootType) {
	var relItem = rootItem.getItemsByXPath('Relationships/Item[@id="{0}"]'.replace('{0}', relationshipID))
			.getItemByIndex(0);
	rootItem.removeRelationship(relItem);
	aras.AlertError('Root Type must be selected.');
}

return false;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_OnInsertRowIndexedConfType' and [Method].is_current='1'">
<config_id>03466DD4FA884C3A9A7465A8E73D1BCF</config_id>
<name>ES_OnInsertRowIndexedConfType</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
