	Item menuItems = null;

	//ItemType and Item reports
	string itemTypeId = this.getProperty("item_type_id");
	if (!string.IsNullOrEmpty(itemTypeId))
	{
		Item items;
		Item itemReports = this.newItem("Item Report", "get");
		itemReports.setProperty("source_id", itemTypeId);
		Item relReports = this.newItem("Report", "get");
		relReports.setAttribute("select", "name,label");
		relReports.setProperty("type", "item");
		itemReports.setRelatedItem(relReports);
		itemReports = itemReports.apply();
		if (!itemReports.isError())
		{
			Item reports = itemReports.getItemsByXPath(Item.XPathResultItem + "/related_id/Item");
			items = HandleMenuButtons(reports, "report:{0}:{1}", itemTypeId);
			if (items != null)
			{
				menuItems = AddSeparator();
				menuItems.appendItem(items);
			}
		}
	}

	return menuItems ?? this.getInnovator().newResult("");
}

private Item AddSeparator()
{
	Item separator = this.newItem("CommandBarSeparator");
	separator.setProperty("name", "cui_reports_sep");
	//Id is required. Id of separator should be constant.
	separator.setID("cui_reports_sep");
	return separator;
}

private static Item HandleMenuButtons(Item reports, string reportTemplate, string itemTypeId)
{
	int reportsCount = reports.getItemCount();
	if (reportsCount > 0)
	{
		Item cuiItems = reports.newItem();
		for (var i = 0; i < reportsCount; i++)
		{
			Item currentReport = reports.getItemByIndex(i);
			Item menu = currentReport.newItem("CommandBarMenu");
			string reportId = currentReport.getID();
			string reportName = string.Format(CultureInfo.InvariantCulture, reportTemplate, reportId, itemTypeId);
			menu.setProperty("name", reportName);
			menu.setProperty("label", currentReport.getProperty("label") ?? currentReport.getProperty("name"));

			menu.setProperty("action", "Add");
			menu.setID(reportId);
			menu.removeAttribute("isNew");
			menu.removeAttribute("isTemp");
			cuiItems.appendItem(menu);
		}

		// newItem() is creates empty node. It's should be deleted
		cuiItems.removeItem(cuiItems.getItemByIndex(0));
		return cuiItems;
	}

	return null;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CuiMainWindowContextMenuReports' and [Method].is_current='1'">
<config_id>75233DE96E4A45E1AF4FE9C1F318B26C</config_id>
<name>CuiMainWindowContextMenuReports</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
