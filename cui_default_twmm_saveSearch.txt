var topWindow = aras.getMostTopWindowWithAras(window);

var saveSaerch = function() {
	var relTabbar = topWindow.relationships ? topWindow.relationships.relTabbar : null;
	var selectedTabId = relTabbar ? relTabbar.GetSelectedTab() : null;
	var selectedTabWnd = selectedTabId ? relTabbar._getTab(selectedTabId).domNode.lastChild.contentWindow : null;

	if (selectedTabWnd && selectedTabWnd.searchContainer) {
		selectedTabWnd.searchContainer._saveSearch();
	}
};

if (topWindow.tearOffMenuController) {
	topWindow.tearOffMenuController.safeInvokeCommand(saveSaerch);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twmm_saveSearch' and [Method].is_current='1'">
<config_id>6ACEF1DE25A74E53BA993184549B5BA5</config_id>
<name>cui_default_twmm_saveSearch</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
