var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
var gridXmlCallback = function() {
	return workerFrame.grid.getXML(true);
};
aras.export2Office(gridXmlCallback, 'export2Excel', undefined, workerFrame.itemTypeName);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_export2Excel' and [Method].is_current='1'">
<config_id>6C8AFF52DEC6425686040CF1F6DC8D01</config_id>
<name>cui_default_mwt_export2Excel</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
