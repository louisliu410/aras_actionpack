'Developed by Kenny at Broadway
'Create Date: 2016/04/03

'System.Diagnostics.Debugger.Break() 
Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)

Dim LbwGeneric As bwInnovatorCore.CInnovator.bwGeneric
Dim LasInnovator As Innovator 
Dim LstrProperty As String
Dim LstrValue As String
Dim LstrUserId As String
Dim LstrIdentity As String=""
Dim LoUserItem As Item=Nothing
Dim LasRetItem As Item=Nothing

Try
	LasInnovator=Me.getInnovator()
' 	LstrItemId=Me.getID
' 	LstrItemType=Me.GetType

	LstrProperty = Me.getPropertyAttribute("body", "user_property")
	LstrValue = Me.getPropertyAttribute("body", "user_value")
	
    LbwGeneric =New bwInnovatorCore.CInnovator.bwGeneric 
    LbwGeneric.bwIOMInnovator=LasInnovator	
	LoUserItem=LbwGeneric.GetItemTypeObject("User", LstrProperty, LstrValue)
	If Not LoUserItem is Nothing Then
            LstrUserId = LoUserItem.getID
            LstrIdentity = LbwGeneric.GetIdentityIdByUserId(LstrUserId)	    
	End If

	LasRetItem=LasInnovator.newItem()
	LasRetItem.setProperty("identity_id",LstrIdentity)
	Return LasRetItem


Catch ex As Exception
	'Throw New Exception(ex.Message)
	Return LasRetItem
Finally
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If
End Try

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_GetIdentityIdByUserInfo' and [Method].is_current='1'">
<config_id>621D033A7CA44206BCEF0102677EB55E</config_id>
<name>bcs_GetIdentityIdByUserInfo</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
