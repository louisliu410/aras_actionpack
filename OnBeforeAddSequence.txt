if (this.getProperty("current_value") == null)
	this.setProperty("current_value", this.getProperty("initial_value"));

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnBeforeAddSequence' and [Method].is_current='1'">
<config_id>659DFCEF58044F4898031A6068B26A3B</config_id>
<name>OnBeforeAddSequence</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
