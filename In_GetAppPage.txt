 /*
目的:依據Itemtype, id, classification, mode 取得 正確的 form
傳入參數：
type :  目標物件的itemtype
id: 目標物件的 id (id與classification擇一,若兩者都存在則以 classification為主)
classification: 詢問的 classification (必須是包含階層的classification,而非最終節點的classification)
mode: edit, add, view (預設為 view)

做法:
到 View 內,依據 mode, classification 取得 對應的 page 與 formid

*/
//System.Diagnostics.Debugger.Break();
string strMethodName = "In_GetAppPage";
Innovator inn=this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
_InnH.AddLog(strMethodName,"MethodSteps");
string aml = "";
string sql = "";
string strItemtype=this.getProperty("type","");
if(strItemtype=="")
	strItemtype=this.getAttribute("type","");
string strItemId = this.getProperty("id","");
string strClass = this.getProperty("classification","");
string strMode = this.getProperty("mode","view");
string strIdentityList=Aras.Server.Security.Permissions.Current.IdentitiesList;
string strError = "";

Item itmR=null;
try{

	if( strItemtype==""){
		return inn.newError("缺少 type 參數");
	}

	if(strClass=="")
	{
		if(strItemId=="")
			return inn.newError("缺少 id 或 classification 參數");
		Item itmItem = inn.getItemById(strItemtype,strItemId);
		strClass = itmItem.getProperty("classification","");
	}

	aml = "<AML>";
	aml += "<Item type='View' action='get'>";
	aml += "<type condition='in'>'in_web_default','in_web_" + strMode + "'</type>";
	aml += "<form_classification>" + strClass + "</form_classification>";
	aml += "<source_id>";
	aml += "<Item type='ItemType' action='get' select='id'>";
	aml += "<name>" + strItemtype + "</name>";
	aml += "</Item>";
	aml += "</source_id>";
	aml += "</Item></AML>";
	Item itmView = inn.applyAML(aml);

	if(itmView.isError())
	{
		//如果沒東西就找這個mode的空白classification
		aml = "<AML>";
		aml += "<Item type='View' action='get'>";
		aml += "<type condition='in'>'in_web_default','in_web_" + strMode + "'</type>";
		aml += "<form_classification condition='is null'></form_classification>";
		aml += "<source_id>";
		aml += "<Item type='ItemType' action='get' select='id'>";
		aml += "<name>" + strItemtype + "</name>";
		aml += "</Item>";
		aml += "</source_id>";
		aml += "</Item></AML>";
		itmView = inn.applyAML(aml);
		if(itmView.isError())
		{
			itmView = inn.newItem("View");	
			string strNewMode = strMode;
			switch(strMode)
			{
				case "add":
					strNewMode = "Edit";
					break;
				default:
					strNewMode = strMode;
					break;
			}
			itmView.setProperty("in_web_page",strItemtype + strNewMode + ".html");
			itmView.setProperty("related_id","");
			itmView.setProperty("type","in_web_" + strMode);
		}
	}


	if(itmView.getItemCount()>1)
	{
		if(itmView.getItemByIndex(0).getProperty("type","")=="in_web_default")
			itmView = itmView.getItemByIndex(1);
		else
			itmView = itmView.getItemByIndex(0);
	}


	itmR = itmView;
	Item itmResponse = inn.newItem("response");
	itmResponse.setProperty("status","success");
	itmResponse.setProperty("message",itmView.getProperty("in_web_page",""));
	itmR.appendItem(itmResponse);
	return itmR;

}catch(Exception ex){
    string errMsg = "";
    if(ex.GetType().ToString()=="Aras.Server.Core.InnovatorServerException")
    {
        Aras.Server.Core.InnovatorServerException e = (Aras.Server.Core.InnovatorServerException)ex;
        XmlDocument errDom = new XmlDocument();
        e.ToSoapFault(errDom); // Retreive inner exception
         // Grab the value of the <faultstring> tags
        errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerXml;
    }
    else
    {
        errMsg=ex.Message;
    }

	if(strError=="")
	{
		strError = errMsg + "\n";

		if(ex.Source=="IOM")
		{
			//代表這是非預期的錯誤
			if(aml!="")
				strError += "無法執行AML:" + aml  + "\n";

			if(sql!="")
				strError += "無法執行SQL:" + sql  + "\n";
		}
	}	
	string strErrorDetail="";
	strErrorDetail = strMethodName + ":" + strError; 
	_InnH.AddLog(strErrorDetail,"Error");
	//strError = strError.Replace("\n","</br>");

	throw new Exception(_InnH.Translate(strError));
}


				

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetAppPage' and [Method].is_current='1'">
<config_id>DCF01AB442114651AF04633035168EA7</config_id>
<name>In_GetAppPage</name>
<comments>依據Itemtype, id, classification, mode 取得 正確的 form</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
