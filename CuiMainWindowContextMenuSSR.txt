	Item items = null;

	//ItemType and Item reports
	string itemTypeId = this.getProperty("item_type_id");
	if (!string.IsNullOrEmpty(itemTypeId))
	{
		//Sel-Service Reports
		Item userReports = this.newItem("Method", "GetSelfServiceReports");
		userReports.setProperty("base_item_type", itemTypeId);
		userReports = userReports.apply();
		if (!userReports.isError())
		{
			items = HandleMenuButtons(userReports, "userreport:{0}");
		}
	}

	return items ?? this.getInnovator().newResult("");
}

private static Item HandleMenuButtons(Item reports, string reportTemplate)
{
	int reportsCount = reports.getItemCount();
	if (reportsCount > 0)
	{
		Item cuiItems = reports.newItem();
		for (var i = 0; i < reportsCount; i++)
		{
			Item currentReport = reports.getItemByIndex(i);
			Item menu = currentReport.newItem("CommandBarMenu");
			string reportId = currentReport.getID();
			string reportName = string.Format(CultureInfo.InvariantCulture, reportTemplate, reportId);
			menu.setProperty("name", reportName);
			menu.setProperty("label", currentReport.getProperty("label") ?? currentReport.getProperty("name"));

			menu.setProperty("action", "Add");
			menu.setID(reportId);
			menu.removeAttribute("isNew");
			menu.removeAttribute("isTemp");
			cuiItems.appendItem(menu);
		}

		// newItem() is creates empty node. It's should be deleted
		cuiItems.removeItem(cuiItems.getItemByIndex(0));
		return cuiItems;
	}

	return null;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CuiMainWindowContextMenuSSR' and [Method].is_current='1'">
<config_id>A5AF3315A8DF46F5895729753C3BF170</config_id>
<name>CuiMainWindowContextMenuSSR</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
