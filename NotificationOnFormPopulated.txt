var itm = document.item;
if (!itm) {
	return;
}

var isStandardMessage = aras.getItemProperty(itm, 'is_standard_template') == '1' ? true : false;
if (isStandardMessage) {
	var customHtmlElem = getFieldByName('custom_html');
	if (customHtmlElem) {
		customHtmlElem.style.visibility = 'hidden';
	}

	var widthElem = getFieldByName('width');
	var heightElem = getFieldByName('height');

	if (widthElem) {
		widthElem.style.display = 'none';
	}

	if (heightElem) {
		heightElem.style.display = 'none';
	}
} else {
	var cssElem = getFieldByName('css');
	if (cssElem) {
		cssElem.style.display = 'none';
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='NotificationOnFormPopulated' and [Method].is_current='1'">
<config_id>8349B042E88D40CBA742C66F99EC6A32</config_id>
<name>NotificationOnFormPopulated</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
