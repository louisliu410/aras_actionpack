inArgs.switchToTabId = 'formTab';
inArgs.enabledButtonImage = '../images/ShowFormOn.svg';
aras.evalMethod('cui_ShowTab_ButtonClick', '', inArgs);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_ShowForm_ButtonClick' and [Method].is_current='1'">
<config_id>1354FAB3F5164CE2B6A5783A9CB5388C</config_id>
<name>cui_ShowForm_ButtonClick</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
