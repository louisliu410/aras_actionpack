'Created Date: 2011/05/04, Creator: Scott

'Purpose：讀取 待簽核表單，增加 ViewMode 參數

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator= Me.getInnovator()
Dim inDom As XmlDocument
Dim outDom As XmlDocument
Dim user_id As String = Me.getProperty("UserID")
Dim view_mode As String = Me.getProperty("ViewMode", "'Both'")    'ViewMode: Active or Pending or Both

inDom = New XmlDocument
inDom.loadXML("<params><inBasketViewMode>" + view_mode + "</inBasketViewMode><workflowTasks>1</workflowTasks><projectTasks>0</projectTasks><actionTasks>0</actionTasks><userID>" & user_id & "</userID></params>")

outDom = New XmlDocument

CCO.Workflow.GetAssignedActivities(inDom, outDom)

Dim result As Item= inn.newItem()
result.loadAML(outDom.OuterXML)

Return result
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetAssignedWorkflowActivities' and [Method].is_current='1'">
<config_id>898B25C1961F426A9BD64781A44FCCED</config_id>
<name>GetAssignedWorkflowActivities</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
