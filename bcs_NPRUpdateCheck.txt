/*
 * Item Type : NPR
 * Server Events : onAfterAdd, onAfterUpdate
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();

	string strAllowedPartState = "Preliminary";
	
	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.NPRUpdateCheck(this, strAllowedPartState);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_NPRUpdateCheck' and [Method].is_current='1'">
<config_id>91E9733FBC014CD4B93878B184483B09</config_id>
<name>bcs_NPRUpdateCheck</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
