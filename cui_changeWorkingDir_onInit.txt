var isNotIe = !aras.Browser.isIe();
// In Chrome an d FF browsers there is no display of the user’s working directory, because this capability cannot be supported in here.
// So this button should be disabled in Chrome and FF
var additionalData = {
	'cui_disabled': isNotIe
};
return additionalData;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_changeWorkingDir_onInit' and [Method].is_current='1'">
<config_id>52F3DDD9DB1A452EB121EE6ADF1DA216</config_id>
<name>cui_changeWorkingDir_onInit</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
