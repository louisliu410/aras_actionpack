var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onRevisionsCommand) {
	workerFrame.onRevisionsCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onRevisions' and [Method].is_current='1'">
<config_id>2577332C38044A48AB2B5D6852F79DE7</config_id>
<name>cui_default_mwt_onRevisions</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
