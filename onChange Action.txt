/*version:2*/
var name = '';
var value = '';
var srcElement = window.event ? window.event.srcElement : event.srcElement ? event.srcElement : event.target;
if (srcElement && srcElement !== document) {
	name = srcElement.name;
	if (srcElement.value) {
		value = srcElement.value.toLowerCase();
	}

}

var pLocation = aras.getItemProperty(document.item, 'location');
var pType = aras.getItemProperty(document.item, 'type');
var pItemQuery = aras.getItemProperty(document.item, 'item_query');

if (name == 'location') {
	pLocation = value;
}
if (name == 'type') {
	pType = value;
}

var _form = document.forms.MainDataForm;
var itemQuery = _form.elements['item_query'];
var _body = _form.elements.body;

if (pType == 'item') {
	_body.disabled = true;
	itemQuery.disabled = false;
} else if (pType == 'itemtype') {
	_body.disabled = false;
	itemQuery.disabled = true;
} else {
	_body.disabled = false;
	itemQuery.disabled = true;
}

if (window.handleItemChange) {
	if (pType == 'item' && pLocation == 'client') {
		handleItemChange('item_query', '<Item type="{@type}" id="{@id}" levels= "1" action="get" />');
	} else if (pType == 'item' && pLocation == 'server') {
		handleItemChange('item_query', '<Item type="{@type}" action="get"><id><xsl:value-of select="@id" /></id></Item>');
	} else {
		handleItemChange('item_query', '');
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onChange Action' and [Method].is_current='1'">
<config_id>EC0D87C174814CB7BA574593456113F7</config_id>
<name>onChange Action</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
