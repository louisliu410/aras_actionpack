// //System.Diagnostics.Debugger.Break();
/*
目的:將Itemtype加上In_WorkflowForm的機制
作法:
逐一檢查以下項目是否存在,若已存在則忽略,若不存在則創建
Property:in_current_activity,in_current_activity_id,in_wff_id,in_wfp_id,in_wfp_state
Item Action:In_AddtoWorkflow,In_ShowWFF,
Server Event:In_RemoveWFFProperty,In_RemoveWFFProperty_AfterCopy,In_DeleteWFF,In_DeleteWFF_Before,
Relationship:_WFF
Criteria:Other Criteria
*/

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Item ItemType = null;
Item itmGridEvent = null;
Item itmClientEvent = null;
Item itmServerEvent = null;

//先判斷鎖定狀態
if(this.getLockStatus()!=0)
{
	throw new Exception(_InnH.Translate("請先解鎖"));
	return this;
}

string param = "";
string aml = "";
string strResult = this.getType() + ":";
Item itmTemp = null;
Item itmResult =null;
//逐一判斷Property, Server Events, Actions,.....若已存在則不動,若不存在則加上去

	//先判斷這個物件是否有Allowed Workflow
	aml = "<AML>";
	aml += "<Item type='Allowed Workflow' action='get'>";
	aml += "<source_id>" + this.getID() + "</source_id>";
	aml += "</Item></AML>";
	itmTemp = inn.applyAML(aml);
	if(itmTemp.isError())
	{
		throw new Exception("本物件沒有流程,所以不需要加上流程機制的欄位、Action、Server Events");
	}




	itmTemp	 = inn.newItem("Property","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("name", "in_current_activity");
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		itmTemp.setProperty("is_hidden","1");
		itmTemp.setProperty("is_hidden2","1");
		itmTemp.setProperty("label","目前流程關卡","zt");
		itmTemp.setProperty("label","目前流程关卡","zc");
		itmTemp.setProperty("data_type","string");
		itmTemp.setProperty("stored_length","32");
		itmResult = itmTemp.apply();
		strResult+=",建立Property:in_current_activity";
		
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
	}
	else
		strResult+=",已存在:Property:in_current_activity";
	
	itmTemp	 = inn.newItem("Property","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("name", "in_current_activity_id");
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		itmTemp.setProperty("is_hidden","1");
		itmTemp.setProperty("is_hidden2","1");
		itmTemp.setProperty("label","目前流程關卡ID","zt");
		itmTemp.setProperty("label","目前流程关卡ID","zc");
		itmTemp.setProperty("data_type","string");
		itmTemp.setProperty("stored_length","32");
		itmResult = itmTemp.apply("add");
		strResult+=",建立Property:in_current_activity_id";
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Property:in_current_activity_id";
	
	itmTemp	 = inn.newItem("Property","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("name", "in_wff_id");
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		itmTemp.setProperty("data_source","FAC120F9746749EA996D1C84E2C05BF7");
		itmTemp.setProperty("is_hidden","1");
		itmTemp.setProperty("is_hidden2","1");
		itmTemp.setProperty("label","簽審流程資訊","zt");
		itmTemp.setProperty("label","签审流程资讯","zc");
		itmTemp.setProperty("data_type","item");
		strResult+=",建立Property:in_wff_id";
		itmResult = itmTemp.apply("add");
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Property:in_wff_id";
	
	

	itmTemp	 = inn.newItem("Property","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("name", "in_wfp_id");
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		itmTemp.setProperty("data_source","261EAC08AE9144FC95C49182ACE0D3FE");
		itmTemp.setProperty("is_hidden","1");
		itmTemp.setProperty("is_hidden2","1");
		itmTemp.setProperty("label","簽審流程","zt");
		itmTemp.setProperty("label","签审流程","zc");
		itmTemp.setProperty("data_type","item");
		
		itmResult = itmTemp.apply("add");
		strResult+=",建立Property:in_wfp_id";
		
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Property:in_wfp_id";
	
	itmTemp	 = inn.newItem("Property","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("name", "in_wfp_state");
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		
		itmTemp.setProperty("is_hidden","1");
		itmTemp.setProperty("is_hidden2","1");
		itmTemp.setProperty("label","流程狀態","zt");
		itmTemp.setProperty("label","流程状态","zc");
		itmTemp.setProperty("data_type","string");
		itmTemp.setProperty("stored_length","32");
		
		itmResult = itmTemp.apply("add");
		strResult+=",建立Property:in_wfp_state";
		
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Property:in_wfp_state";
	
	
	itmTemp	 = inn.newItem("Item Action","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("related_id", "FB3466F284104D5B8FF65D8DBCF368E6"); //In_AddtoWorkflow
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		itmResult = itmTemp.apply("add");
		strResult+=",建立Item Action:In_AddtoWorkflow";
		
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Item Action:In_AddtoWorkflow";
	
	
	itmTemp	 = inn.newItem("Item Action","get");
	itmTemp.setProperty("source_id", this.getID());
	itmTemp.setProperty("related_id", "8074F2DEA921473183EAE96352FFBBA5"); //In_ShowWFF
	itmResult = itmTemp.apply();
	if(itmResult.isError())
	{
		itmResult = itmTemp.apply("add");
		strResult+=",建立Item Action:In_ShowWFF";
		
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Item Action:In_ShowWFF";
	
	

	aml = "<AML>";
	aml += "	<Item type='Server Event' action='get'>";
	aml += "		<related_id><Item type='Method' action='get' select='id'><name>In_RemoveWFFProperty</name></Item></related_id>";
	aml += "		<server_event>onAfterUpdate</server_event>";
	aml += "		<source_id>" + this.getID() + "</source_id>";
	aml += "	</Item>			";
	aml += "</AML>";
	itmResult = inn.applyAML(aml);
	if(itmResult.isError())
	{
		itmResult = itmTemp.apply("add");
		strResult+=",建立Server Event:In_RemoveWFFProperty";

		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Server Event:In_RemoveWFFProperty";
	
	aml = "<AML>";
	aml += "	<Item type='Server Event' action='get'>";
	aml += "		<related_id><Item type='Method' action='get' select='id'><name>In_RemoveWFFProperty_AfterCopy</name></Item></related_id>";
	aml += "		<server_event>onAfterCopy</server_event>";
	aml += "		<source_id>" + this.getID() + "</source_id>";
	aml += "	</Item>			";
	aml += "</AML>";
	itmResult = inn.applyAML(aml);
	if(itmResult.isError())
	{
		itmResult = itmTemp.apply("add");
		strResult+=",建立Server Event:In_RemoveWFFProperty_AfterCopy";

		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Server Event:In_RemoveWFFProperty_AfterCopy";
	
	
	
	aml = "<AML>";
	aml += "	<Item type='Server Event' action='get'>";
	aml += "		<related_id><Item type='Method' action='get' select='id'><name>In_DeleteWFF</name></Item></related_id>";
	aml += "		<server_event>onAfterDelete</server_event>";
	aml += "		<source_id>" + this.getID() + "</source_id>";
	aml += "	</Item>			";
	aml += "</AML>";
	itmResult = inn.applyAML(aml);
	if(itmResult.isError())
	{
		itmResult = itmTemp.apply("add");
		strResult+=",建立Server Event:In_DeleteWFF";
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Server Event:In_DeleteWFF";
	
	aml = "<AML>";
	aml += "	<Item type='Server Event' action='get'>";
	aml += "		<related_id><Item type='Method' action='get' select='id'><name>In_DeleteWFF_Before</name></Item></related_id>";
	aml += "		<server_event>onBeforeDelete</server_event>";
	aml += "		<source_id>" + this.getID() + "</source_id>";
	aml += "	</Item>			";
	aml += "</AML>";
	itmResult = inn.applyAML(aml);
	if(itmResult.isError())
	{
		itmResult = itmTemp.apply("add");
		strResult+=",建立Server Event:In_DeleteWFF_Before";
		if(itmResult.isError())
		{
			throw new Exception(itmResult.getErrorString());
		}
		
	}
	else
		strResult+=",已存在:Server Event:In_DeleteWFF_Before";


	


// 4.創建一個 簽審紀錄 的頁簽
aml  = "<AML>";
aml += "<Item type='RelationshipType' action='get' select='name'>";
aml += "<source_id>";
aml += "<Item type='Itemtype' action='get' select='id'>";
aml += "<name>"+this.getProperty("name")+"</name>";
aml += "</Item>";
aml += "</source_id>";
aml += "<name condition='eq'>"+this.getProperty("name")+"_WFF</name>";
aml += "</Item></AML>";
Item RelationshipType = inn.applyAML(aml);

if(RelationshipType.isError())
{
    aml  = "<AML>";
    aml += "<Item type='RelationshipType' action='add'>";
    aml += "<label>簽核歷程</label>";
    aml += "<name>"+this.getProperty("name")+"_WFF</name>";
    aml += "<behavior>float</behavior>";
    aml += "<auto_search>1</auto_search>";
    aml += "<related_option>1</related_option>";
    aml += "<related_notnull>1</related_notnull>";
    aml += "<source_id>"+this.getID()+"</source_id>";
    aml += "<related_id>FAC120F9746749EA996D1C84E2C05BF7</related_id>";
    aml += "</Item></AML>";
    RelationshipType = inn.applyAML(aml);

    aml  = "<AML>";
    aml += "<Item type='Relationship Grid Event' action='add'>";
    aml += "<grid_event>ondeleterow</grid_event>";
    aml += "<related_id>";
    aml += "<Item type='Method' action='get' select='id'>";
    aml += "<name>In_DelRowCheck</name>";
    aml += "</Item>";
    aml += "</related_id>";
    aml += "<source_id>"+RelationshipType.getID()+"</source_id>";
    aml += "</Item></AML>";
    itmGridEvent = inn.applyAML(aml);

    aml  = "<AML>";
    aml += "<Item type='ItemType' action='get'>";
    aml += "<name>"+RelationshipType.getProperty("name")+"</name>";
    aml += "</Item></AML>";
    ItemType = inn.applyAML(aml);

    ItemType.setProperty("label","簽核歷程");
    ItemType.setProperty("allow_private_permission","0");
    ItemType.setProperty("use_src_access","1");
    ItemType = ItemType.apply("edit");

    aml  = "<AML>";
    aml += "<Item type='Property' action='add'>";
    aml += "<data_type>boolean</data_type>";
    aml += "<readonly>1</readonly>";
    aml += "<is_hidden>1</is_hidden>";
    aml += "<is_hidden2>0</is_hidden2>";
    aml += "<source_id>"+ItemType.getID()+"</source_id>";
    aml += "<label>使用中</label>";
	aml += "<sort_order>10</sort_order>";
	aml += "<name>in_isuse</name>";
	aml += "</Item></AML>";
	Item itmProperty = inn.applyAML(aml);

    aml  = "<AML>";
    aml += "<Item type='Client Event' action='add'>";
    aml += "<client_event>OnNew</client_event>";
    aml += "<related_id>";
    aml += "<Item type='Method' action='get' select='id'>";
    aml += "<name>In_CheckAddRow</name>";
    aml += "</Item>";
    aml += "</related_id>";
    aml += "<source_id>"+ItemType.getID()+"</source_id>";
    aml += "</Item></AML>";
    itmClientEvent = inn.applyAML(aml);

    aml  = "<AML>";
    aml += "<Item type='Server Event' action='add'>";
    aml += "<related_id><Item type='Method' action='get' select='id'><name>In_AvoidSaveAs</name></Item></related_id>";
    aml += "<server_event>onAfterCopy</server_event>";
	aml += "<source_id>"+ItemType.getID()+"</source_id>";
	aml += "</Item></AML>";
	itmServerEvent = inn.applyAML(aml);

    strResult+=",建立Relationship:"+this.getProperty("name")+"_WFF";
}
else
{
	strResult+=",已存在:Relationship:"+this.getProperty("name")+"_WFF";
}

//5.創建一個 Others 的 Criteria
param = "<in_itemtype>" + this.getID() + "</in_itemtype>";
Item itmCriteria = inn.applyMethod("In_CreateOtherCriteria",param);
if(itmCriteria!=null)
	strResult+=",建立Other Criteria";
else
	strResult+=",已存在:Other Criteria";

CCO.Utilities.WriteDebug("In_EnableWFFForItem", strResult);
return inn.newResult(_InnH.Translate(strResult));

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_EnableWFFForItem' and [Method].is_current='1'">
<config_id>83345F63974F43A89E2F72AA8624818F</config_id>
<name>In_EnableWFFForItem</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
