'Created Date: 2008/12/01, Creator: Scott
'Modified Date: 2010/12/15, Modified by Scott

'System.Diagnostics.Debugger.Break() 

Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)
'===========================================================================================

Dim inn As Innovator = Me.getInnovator()
Dim daf_id As String = Me.getProperty("ID")
Dim daf_state As String = Me.getProperty("State")
Dim from_state As String = Me.getProperty("FromState")
Dim to_state As String = Me.getProperty("ToState")
Dim err As Item
Dim i As Integer

Dim dd As Item = Me.newItem("DAF Document","get")
dd.setProperty("source_id", daf_id)
Dim r_dd As Item = dd.apply()
Dim count As Integer = r_dd.getItemCount()

Dim d As Item
Dim doc As Item
Dim r_doc As Item
Dim result As Item
Dim effective_date As DateTime

'===================================================================
'取得目前時間，並轉換成 Local DateTime
Dim dtNow As DateTime = System.DateTime.UtcNow
Dim tmpPtrn As String = "yyyy-MM-ddTHH:mm:ss"
Dim tmpDt As String = dtNow.toString(tmpPtrn)

tmpDt += "+0000"
tmpPtrn += "zzz"

'Dim sCntxt As I18NSessionContext = Me.newInnovator().getI18NSessionContext()
Dim sCntxt As I18NSessionContext = Me.getInnovator().getI18NSessionContext()
Dim today As DateTime = DateTime.Parse(sCntxt.ConvertToNeutral(tmpDt, "date", tmpPtrn))
'===================================================================

For i = 0 To count -1
	d = r_dd.getItemByIndex(i).getRelatedItem()
	
	'讀取 Document 內容
	doc = inn.newItem("Document", "get")
	doc.setAttribute("select", "id, item_number, revision, generation, state, serial_no, description, classification, effective_date")
	doc.setID(d.getID())
	r_doc = doc.apply()
	
	If r_doc.isError() Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If

		err = inn.newError("Fail to get document for id =  " + d.getID() + ". "  + r_doc.getErrorDetail())
		Return err 
	End If	
	
	'檢查 Document 必須處於 Unlock 狀態
	If r_doc.fetchLockStatus() <> 0 Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If

		err = inn.newError("Document = " + r_doc.getProperty("item_number") + ", document can not be lock.")
		Return err 
	End If

	'比對 From State 是否正確？
	If from_state <> "" Then
		If r_doc.getProperty("state") <> from_state Then
			If (PermissionWasSet=True) Then
				Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
			End If

			err = inn.newError("Document = " + r_doc.getProperty("item_number") + ", the state of document must be  " + from_state + ". " )
			Return err 
		End If
	End If
	
	'比對 Effective Date 不能小於今天，如果小於今天，Effective Date 就改成 今天
	If (Not r_doc.getProperty("effective_date") Is Nothing) AndAlso (r_doc.getProperty("effective_date") <> "") Then
		effective_date =  DateTime.Parse(r_doc.getProperty("effective_date"))
		
		If effective_date < today Then
			r_doc.setProperty("effective_date", today.toString("yyyy-MM-ddTHH:mm:ss"))
			r_doc.setAttribute("version","0")
			r_doc.setAttribute("serverEvents","0")
			r_doc.setAttribute("doGetItem", "0")
			r_doc.setAction("edit")
			
			result = r_doc.apply()
			
			If result.isError() Then
				If (PermissionWasSet=True) Then
					Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
				End If

				err = inn.newError("Fail to update the effective date of document. " + result.getErrorDetail())
				Return err 
			End If
		End If
	End If
	
	'將 document 推到 To State
	r_doc.setAction("PromoteItem")
	r_doc.setProperty("state", to_state)
	result = r_doc.apply()
	
	If result.isError() Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If

		err = inn.newError("Fail to promote document to " + to_state + ". " + result.getErrorDetail())
		Return err 
	End If

	d = Nothing
	doc = Nothing
	r_doc = Nothing
	result = Nothing
Next

'===========================================================================================
If (PermissionWasSet=True) Then
	Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
End If

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='AffectedDocumentPromote' and [Method].is_current='1'">
<config_id>731BAF18EACB49D9AB39E2A25CFE93A2</config_id>
<name>AffectedDocumentPromote</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
