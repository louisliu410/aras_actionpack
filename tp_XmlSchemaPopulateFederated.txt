
for(int itemIndex = 0; itemIndex < this.getItemCount(); itemIndex++)
{
	Item item = this.getItemByIndex(itemIndex);
	string content = item.getProperty("content");
	if(String.IsNullOrEmpty(content))
	{
		continue;
	}
	XmlDocument schemaDom = new XmlDocument();
	schemaDom.XmlResolver = null;
	schemaDom.LoadXml(content);
	string targetNamespace = schemaDom.DocumentElement.GetAttribute("targetNamespace");

	item.setProperty("target_namespace", targetNamespace);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_XmlSchemaPopulateFederated' and [Method].is_current='1'">
<config_id>33275B46BAE24731A325668CC0B054D4</config_id>
<name>tp_XmlSchemaPopulateFederated</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
