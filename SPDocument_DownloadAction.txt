var self = this;

// 'self' represents item of the selected row
var spFileName = self.getProperty('sp_file_name');

var tname = self.getType();
var defRequestAml =
	'<Item type=\'SPDocumentLibraryDefinition\' action=\'get\' select=\'authentication_type, sp_site_url, sp_doclib_id\'>' +
	'<doc_type_name>' + tname + '</doc_type_name>' +
	'</Item>';

var defRequest = top.aras.newIOMItem();
defRequest.loadAML(defRequestAml);
var dlDef = defRequest.apply();
if (dlDef.isError()) {
	top.aras.AlertError(dlDef.getErrorString(), dlDef.getErrorDetail());
	return;
}

var authType = dlDef.getProperty('authentication_type');
var siteUrl = dlDef.getProperty('sp_site_url');
var doclibid = dlDef.getProperty('sp_doclib_id');

var winAuth = (authType != 'DedicatedUserOnly');

sendRequest(winAuth)
	.then(function() {
			top.aras.AlertSuccess(top.aras.getResource('', 'aras_object.file_saved_as', spFileName));
		},
		function(error) {
			if (authType == 'WindowsAuthenticationPreferred') {
				return sendRequest(false);
			}
			top.aras.AlertError(top.aras.getResource('', 'sharepoint.file_download_failure') + error);
		})
	.catch(function(error) {
		top.aras.AlertError(top.aras.getResource('', 'sharepoint.file_download_failure') + error);
	});

function sendRequest(winAuth) {
	return new Promise(function(resolve, reject) {
		var xhr = new XMLHttpRequest();
		var headers = {
			'SP-Action': 'DownloadFile',
			'SP-Url': siteUrl,
			'SP-DocLibId': doclibid,
			'SP-DocName': spFileName
		};

		if (winAuth) {
			xhr.open('GET', top.aras.getServerBaseURL() + 'SharePoint/Auth/SPHelperWinAuth.aspx');
		} else {
			xhr.open('GET', top.aras.getServerBaseURL() + 'SharePoint/SPHelper.aspx');
			xhr.setRequestHeader('SP-DocTypeName', self.getType());
			Object.assign(headers, top.aras.getHttpHeadersForSoapMessage());
		}

		Object.keys(headers).forEach(function(item) {
			xhr.setRequestHeader(item, headers[item]);
		});

		xhr.responseType = 'blob';

		xhr.addEventListener('load', function() {
			if (this.status !== 200) {
				reject(new Error(this.statusText));
				return;
			}
			ArasModules.vault.saveBlob(this.response, spFileName);
			resolve();
		});

		xhr.addEventListener('error', function() {
			reject(new Error(this.statusText));
		});

		xhr.send();
	});
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocument_DownloadAction' and [Method].is_current='1'">
<config_id>0171AA7D7A964EBBA87FAC671D9E7F31</config_id>
<name>SPDocument_DownloadAction</name>
<comments>Download action for SP documents</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
