/*
目的:取得APP Project交付物 多階BOM
做法:
*/
string strMethodName = "In_GetProjectDeliverableTree";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string strError = "";
//string strUserId = inn.getUserID();
//string strIdentityId = inn.getUserAliases();

string strPrjID = this.getProperty("id","");

_InnH.AddLog(strMethodName,"MethodSteps");

Item itmMultiBOM = inn.newItem("Project","bcs_GetActivityDeliverable");
itmMultiBOM.setID(strPrjID);
itmMultiBOM = itmMultiBOM.apply();
Item itmRootCAD = inn.getItemById("Project",strPrjID);
Item itmTreeElements = BomTreeToAppTree(itmMultiBOM.getResult(),itmRootCAD,inn);

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmTreeElements;
}

private Item BomTreeToAppTree(string TreeResult, Item RootItem,Innovator inn)
{
	Item itmR = inn.newItem();
	XmlDocument xmlBomTree = new XmlDocument();
	xmlBomTree.LoadXml(TreeResult);
	XmlNodeList nlTRs = xmlBomTree.SelectNodes("//tr[@level='0']");

	//在此修改 Bom Tree 的欄位
	itmR.setProperty("gridData_rowItemID", RootItem.getID());
	itmR.setProperty("name", RootItem.getProperty("name"));
	itmR.setProperty("state", RootItem.getProperty("state"));
	itmR.setProperty("percent_compl", RootItem.getProperty("percent_compl","0"));
	itmR.setProperty("type", "");
	itmR.setProperty("source_id", "");
	itmR.setProperty("level", "0");
	
	
	BomToAppTreeChildren(itmR, nlTRs, RootItem.getID(), 0,inn);
	return itmR;
}

private Item BomToAppTreeChildren(Item Bom, XmlNodeList MeNodes, string ParentID, int ParentLevel,Innovator inn)
{
	string strMyLevel = (ParentLevel + 1).ToString();
	foreach (XmlNode MeNode in MeNodes)
	{
		Item itmChildNode = inn.newItem();
		XmlNodeList xnUserDatas = MeNode.SelectNodes("userdata");
		string ItemId = "";
		foreach (XmlNode xnUserData in xnUserDatas)
		{
			itmChildNode.setProperty(xnUserData.Attributes["key"].InnerText, xnUserData.Attributes["value"].InnerText);
			if(xnUserData.Attributes["key"].InnerText=="gridData_rowItemID")
				ItemId = xnUserData.Attributes["value"].InnerText;
		}
		XmlNodeList xnTDs = MeNode.SelectNodes("td");
		//在此修改 Bom Tree 的欄位
		itmChildNode.setProperty("name", xnTDs[0].InnerText);
		itmChildNode.setProperty("state", xnTDs[1].InnerText);
		itmChildNode.setProperty("percent_compl", xnTDs[2].InnerText);
		itmChildNode.setProperty("type", xnTDs[3].InnerText);
		itmChildNode.setProperty("source_id", ParentID);
		itmChildNode.setProperty("level", strMyLevel);
		
		//如果是 in_file 則必須要取 in_file 的 in_file欄位
		
		if(xnTDs[3].InnerText=="in_file")
		{
			 Item InFile = inn.getItemById("in_file",ItemId);
			if(!InFile.isError())
			{
				string fileid = InFile.getProperty("in_file","");
				itmChildNode.setProperty("in_file", fileid);
			}
		}
		
	
				
		Bom.appendItem(itmChildNode);
		XmlNodeList MyChildeNodes = MeNode.SelectNodes("tr");
		if (MyChildeNodes.Count > 0)
			BomToAppTreeChildren(Bom, MyChildeNodes, itmChildNode.getProperty("gridData_rowItemID"), ParentLevel + 1,inn);
	}

	return Bom;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetProjectDeliverableTree' and [Method].is_current='1'">
<config_id>D12CF9AB013F4426A7979396F991962F</config_id>
<name>In_GetProjectDeliverableTree</name>
<comments>取得APP Project交付物 多階BOM</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
