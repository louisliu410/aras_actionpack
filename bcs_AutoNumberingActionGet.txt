//debugger;

//Modify by kenny 2016/02/21 refresh
//Modify by kenny 2016/02/27

if (this.fetchLockStatus() != '0')
{
	var strError = "Item must be unlocked.";
	alert(strError);
	return null;
}

var innovator = this.getInnovator();

var strType= this.getType();
var strId= this.getId();

//Modify by kenny 2016/02/27 --------------------------------
var doc = top.aras.createXMLDocument();
doc.loadXML("<body />");
doc.documentElement.setAttribute("type", strType);
doc.documentElement.setAttribute("id", strId);
doc.documentElement.setAttribute("action", "get");
var itmResult = innovator.applyMethod("bcs_AutoNumberingByCall", doc.xml);
//var itmResult = innovator.applyMethod("bcs_AutoNumberingByCall", "<type>" + strType + "</type><id>" + strId + "</id><action>get</action>");
//-----------------------------------------------------------

if (itmResult.isError()) {
	alert(itmResult.getErrorString());
} else {
//	top.aras.refresh();
	var arrValue = itmResult.getResult().split("|&|");
	alert(arrValue[2]);
}

//Modify by kenny 2016/02/21 ------------------------------
if (typeof top.onRefresh == 'undefined') {return;}
var itm = top.aras.getItemById(strType, strId , 0);
top.aras.uiShowItemEx(itm, undefined, true);
top.onRefresh();
//---------------------------------------------------------
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_AutoNumberingActionGet' and [Method].is_current='1'">
<config_id>A42DFE61073F4ADB96AD7DD14370CF1A</config_id>
<name>bcs_AutoNumberingActionGet</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
