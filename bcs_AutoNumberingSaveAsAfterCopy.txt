/*
 * 設定方式
 * Item Type : Numbering ItemType
 * Server Events : onAfterCopy
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
	//System.Diagnostics.Debugger.Break();
	
	bcsSequence.AutoNumbering _AutoNumbering = new bcsSequence.AutoNumbering(inn);
	_AutoNumbering.IsUpdate = false;
	Item itmResult = _AutoNumbering.BCSClearNumber(this);
	
	if (itmResult.isError()) return itmResult;
	//_AutoNumbering=null;
	//_AutoNumbering = new bcsSequence.AutoNumbering(inn);
	_AutoNumbering.IsUpdate = true;
	
	//許沒有流水號設定
	_AutoNumbering.IsSeqNotEmpty=true ;
	//驗證欄位是否沒有值,如沒有填,不允許領號
	_AutoNumbering.IsCheckFieldValue=true ;
	
	itmResult = _AutoNumbering.BCSAutoNumbering(this);
	
	if (itmResult.isError()) return itmResult;	
	
	
	
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_AutoNumberingSaveAsAfterCopy' and [Method].is_current='1'">
<config_id>2EDC7C4B42DE48E78B246132CCB1E4A3</config_id>
<name>bcs_AutoNumberingSaveAsAfterCopy</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
