/*
 * 設定方式
 * Item Type : BCS Sequence Setting
 * Server Events : onAfterAdd, onAfterCopy, onAfterUpdate
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	bcsSequence.AutoNumbering _AutoNumbering = new bcsSequence.AutoNumbering(inn);
	Item itmResult = _AutoNumbering.EditBCSSequenceSetting(this, "Edit");
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_AutoNumberingSettingSave' and [Method].is_current='1'">
<config_id>22F261F4D7E34CB5BD7C173A810A5572</config_id>
<name>bcs_AutoNumberingSettingSave</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
