// //System.Diagnostics.Debugger.Break();
/*
目的:將流程表單加上in_current_activity_id屬性
作法:
*/

Innovator inn = this.getInnovator();
Item ItemType = null;
Item itmGridEvent = null;
Item itmClientEvent = null;
Item itmServerEvent = null;

//先判斷鎖定狀態
if(this.getLockStatus()!=0)
{
	throw new Exception("請先解鎖");
	return this;
}

string param = "";
string aml = "";
string sql = "";
Aras.Server.Core.InnovatorDatabase conn = CCO.DB.InnDatabase;
string strResult = "";

aml = "<AML>";
aml += "<Item type='Allowed Workflow' action='get' select='source_id(name,label)' >";
aml += "<source_id condition='ne'>FAC120F9746749EA996D1C84E2C05BF7</source_id>";
aml += "</Item></AML>";
Item itmWorkflowItems = inn.applyAML(aml);
Dictionary<string, string> ditDraftItemtype = new Dictionary<string, string>( );
for(int i=0;i<itmWorkflowItems.getItemCount();i++)
{
	Item itmSourceItem  =itmWorkflowItems.getItemByIndex(i).getPropertyItem("source_id");
	if(ditDraftItemtype.ContainsKey(itmSourceItem.getProperty("name","")))
		continue;
	
	ditDraftItemtype.Add(itmSourceItem.getProperty("name",""),itmSourceItem.getProperty("label",""));

	
	aml  = "<AML>";
	aml += "	<Item type='Property' action='add'>";
	aml += "		<data_type>string</data_type>";
	aml += "		<is_hidden>1</is_hidden>";
	aml += "		<is_hidden2>1</is_hidden2>";
	aml += "		<source_id>@ItemId</source_id>";
	aml += "		<label xml:lang='zt'>目前流程關卡ID</label>";
	aml += "		<label xml:lang='zc'>目前流程关卡ID</label>";
	aml += "		<stored_length>32</stored_length>";
	aml += "		<name>in_current_activity_id</name>";
	aml += "	</Item>";
	aml += "</AML>";
	
	sql = "select id from [Property] where source_id='" + itmSourceItem.getID() + "' and name='in_current_activity_id'";
	Aras.Server.Core.InnovatorDataSet restulDataSet = conn.ExecuteSelect(sql);
	if(restulDataSet.RowsNumber>0)
		continue;
	
	aml = aml.Replace("@ItemId",itmSourceItem.getID());
	Item itmItemtype = inn.applyAML(aml);

	strResult += itmSourceItem.getProperty("name") + ",";
	
	
}




return inn.newResult(strResult);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AddCurrentActivityId_All' and [Method].is_current='1'">
<config_id>A36C644295E84712B6F22CB1D2064B86</config_id>
<name>In_AddCurrentActivityId_All</name>
<comments>流程表單加上in_current_activity_id屬性</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
