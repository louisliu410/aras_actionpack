var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onViewAttachedCommand) {
	workerFrame.onViewAttachedCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_inbasket_onView' and [Method].is_current='1'">
<config_id>20627BDB60894C39BCA7C3093DCEA8B0</config_id>
<name>cui_default_inbasket_onView</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
