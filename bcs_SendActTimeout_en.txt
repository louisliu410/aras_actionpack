/*
 * Run Server Method
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	string strActTimeoutMessageName = "bcs_ActivityTimeout_en";
	string strResult = "";
	
	bcsNotificationLite.Notification _Notification = new bcsNotificationLite.Notification(inn);
	
	strResult = _Notification.SendActTimeout(strActTimeoutMessageName);
	
	return inn.newResult(strResult);
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_SendActTimeout_en' and [Method].is_current='1'">
<config_id>57A60748226347C2A13C71A27BA73E74</config_id>
<name>bcs_SendActTimeout_en</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
