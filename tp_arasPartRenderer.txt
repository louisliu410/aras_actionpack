return {
	constructor: function(args) {},

	RenderInnerContent: function(/*WrappedObject*/ renderObject) {
		var propertyName = renderObject.Attribute('propertyName') || 'id';
		var xmlNameRegExp = /^(?!XML)[a-z][\w0-9-]*$/i;
		var propertyValue = xmlNameRegExp.test(propertyName) ? renderObject.GetProperty(propertyName, '$null$') : '$null$';

		return '<span>' + dojox.html.entities.encode(propertyValue) + '</span>';
	},

	GetTreeName: function(/*WrappedObject*/ renderObject) {
		var treeName = this.inherited(arguments);
		var keyedName  = renderObject.GetProperty('keyed_name', '$null$');
		var propertyName = renderObject.Attribute('propertyName') || 'id';

		treeName += ' - <i>' + dojox.html.entities.encode(keyedName) + '(' + propertyName + ')</i>' ;

		return treeName;
	}
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_arasPartRenderer' and [Method].is_current='1'">
<config_id>7C526049C077470BB9A2AC0F846B3689</config_id>
<name>tp_arasPartRenderer</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
