if (arguments && arguments.length > 0) {
	if (arguments[0].srcElement) {
		if (parent.updateTreeNodeLabel) {
			parent.updateTreeNodeLabel(arguments[0].srcElement.value, 'root');
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_UpdateContentTypeLabel' and [Method].is_current='1'">
<config_id>4F5C312A05F5425FA5CA8A673D8DCBD0</config_id>
<name>cmf_UpdateContentTypeLabel</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
