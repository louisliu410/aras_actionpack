'Created Date: 2008/12/23, Creator: Scott
'Modified Date: 2009/05/15, Modified by: Scott
'Modified Date: 2010/12/15, Modified by: Scott

'Purpose：將審核中的文件從表單上移除時，文件的 Lifecycle Phase 必須從『審核中』改成『未生效』
'『審核中』：from_state
'『未生效』：to_state

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim result As Item
Dim from_state As String = "In Review"
Dim to_state As String = "Preliminary"

Dim dd As Item
Dim r_dd As Item

dd = inn.newItem("DAF Document", "get")
dd.setAttribute("select", "id, related_id(id, item_number, state)")
dd.setID(Me.getID())
r_dd = dd.apply()

If r_dd.isError() Then
	err = inn.newError("Can not find the related document.")
	Return err
End If

Dim doc As Item = r_dd.getRelatedItem() 

If doc.getProperty("state") <>  from_state Then
	Return Me
Else

'  Modified Date: 2009/05/15, Modified by: Scott
'	result = inn.applyMethod("AffectedDocumentPromote","<ID>" + doc.getID() + "</ID><State>" + doc.getProperty("state") + "</State><FromState>審核中</FromState><ToState>未生效</ToState>")
'
'	If result.isError() Then
'		err = inn.newError("Fail to promote document to 未生效. " + result.getErrorDetail())
'		Return err 
'	End If

	Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
	Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)
	'===========================================================================================

	doc.setAction("PromoteItem")
	doc.setProperty("state", to_state)
	result = doc.apply()
	
	If result.isError() Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If

		err = inn.newError("Fail to promote document to " &  to_state & ". " & result.getErrorDetail())
		Return err 
	End If
	
	'===========================================================================================
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If
	
	Return Me
End If
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SetDocumentBackToPreliminary' and [Method].is_current='1'">
<config_id>86F909D977E247A08601B404D9230DB8</config_id>
<name>SetDocumentBackToPreliminary</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
