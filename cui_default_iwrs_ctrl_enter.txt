if (this.computeCorrectControlState) {
	var isShowItemEnabled = this.computeCorrectControlState('show_item');
	if (isShowItemEnabled) {
		this.processCommand('show_item');
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_iwrs_ctrl_enter' and [Method].is_current='1'">
<config_id>7890246159A741B69B5F782F984C082C</config_id>
<name>cui_default_iwrs_ctrl_enter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
