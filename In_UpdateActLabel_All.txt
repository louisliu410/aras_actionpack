//變更任務節點的Label改成中文語系的Label
//為了讓 新版 MyInbasket可以直接看到流程任務名稱,因此要做這個動作
Innovator inn = this.getInnovator();
Item itmActs = inn.newItem("Activity");
itmActs.setProperty("state","'Active','Pending'");
itmActs.setPropertyAttribute("state","condition","in");
itmActs = itmActs.apply("get");
string strResult = "";
strResult = "Activity:" + itmActs.getItemCount();
for(int i=0;i<itmActs.getItemCount();i++)
{
    Item itmAct = itmActs.getItemByIndex(i);
    itmAct.apply("In_UpdateActLabel");
}

itmActs = inn.newItem("Activity Template");
itmActs = itmActs.apply("get");
strResult += ",Activity Template:" + itmActs.getItemCount();
for(int i=0;i<itmActs.getItemCount();i++)
{
    Item itmAct = itmActs.getItemByIndex(i);
    itmAct.apply("In_UpdateActLabel");
}

return inn.newResult(strResult);


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateActLabel_All' and [Method].is_current='1'">
<config_id>287A2A9EA5F4413E971479A80B43AA36</config_id>
<name>In_UpdateActLabel_All</name>
<comments>變更任務節點的Label改成中文語系的Label</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
