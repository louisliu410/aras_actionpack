Innovator inn = this.getInnovator();

Item agent = inn.newItem("ES_Agent", "get");
agent.setAttribute("select", "id");
agent = agent.apply();

if (!agent.isError()) {
	if (agent.getItemCount() > 0) {
		return inn.newError("There can not be more than one agent.");
	}
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_ValidateAgent' and [Method].is_current='1'">
<config_id>53DA627700E14BB881B8D15618CB94CC</config_id>
<name>ES_ValidateAgent</name>
<comments>Validate agent count</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
