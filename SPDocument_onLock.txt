//MethodTemplateName=CSharpInOut;
throw new Exception( "Can't lock SharePoint document from Innovator" );
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocument_onLock' and [Method].is_current='1'">
<config_id>4C8A1377040B45DDB87B954EDE2D1DF5</config_id>
<name>SPDocument_onLock</name>
<comments>Prevents locking SP documents by throwing an error</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
