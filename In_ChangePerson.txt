/*
目的:將關聯任務的執行者與審核者寫入執行人員與執行主管的頁簽
*/
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

string delManager = "";
string delAssignments = "";
string aml = "";

string addManager = "";
string addAssignments = "";

delAssignments = "<AML>";
delAssignments += "<Item type='In_WorkOrder_Assignments' action='delete' where=\"source_id='" +   this.getID() + "'\">";
delAssignments += "</Item>";
delAssignments += "</AML>";
Item itmDelAssignments = inn.applyAML(delAssignments);

delManager = "<AML>";
delManager += "<Item type='In_WorkOrder_Managers' action='delete' where=\"source_id='" +   this.getID() + "'\">";
delManager += "</Item>";
delManager += "</AML>";
Item itmDelManager = inn.applyAML(delManager);

aml = "<AML>";
aml += "<Item type='In_WorkOrder_Detail' action='get' orderBy='sort_order' select='in_new_assignment,sort_order,in_is_current,in_manager'>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "<in_is_current>1</in_is_current>";
aml += "</Item>";
aml += "</AML>";
Item itmChange = inn.applyAML(aml);
string strManagers = "";
for(int i=0;i<itmChange.getItemCount();i++){
	Item itmChanges = itmChange.getItemByIndex(i);

	string strManager = itmChanges.getProperty("in_manager","");
	string strNewAgt = itmChanges.getProperty("in_new_assignment","");

	strManagers += itmChanges.getPropertyAttribute("in_manager","keyed_name") + ",";

	/*
	Item itmManager = inn.getItemById("Identity",strManager);
	Item itmNewAgt = inn.getItemById("Identity",strNewAgt);
	string strMagName = itmManager.getProperty("name","");
	string strNagName = itmNewAgt.getProperty("name","");
	*/

	addAssignments = "<AML>";
	addAssignments += "<Item type='In_WorkOrder_Assignments' action='add'>";
	addAssignments += "<source_id>" + this.getID() + "</source_id>";
	addAssignments += "<related_id>" + strNewAgt + "</related_id>";
	addAssignments += "</Item>";
	addAssignments += "</AML>";
	Item itmAddAssignments = inn.applyAML(addAssignments);

    if(strManager!="")
    {
    	addManager = "<AML>";
    	addManager += "<Item type='In_WorkOrder_Managers' action='add'>";
    	addManager += "<source_id>" + this.getID() + "</source_id>";
    	addManager += "<related_id>" + strManager + "</related_id>";
    	addManager += "</Item>";
    	addManager += "</AML>";
    	Item itmAddManager = inn.applyAML(addManager);
    }
}

strManagers = strManagers.Trim(',');
string sql = "";
sql = "Update [In_WorkOrder] set [in_manager]='" + strManagers + "' where id='" + this.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ChangePerson' and [Method].is_current='1'">
<config_id>D4A9D7B195A24C8A963DD2EDE318E2D5</config_id>
<name>In_ChangePerson</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
