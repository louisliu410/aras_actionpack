//變更任務節點的Label改成中文語系的Label
//為了讓 新版 MyInbasket可以直接看到流程任務名稱,因此要做這個動作
Innovator inn = this.getInnovator();
string sql = "Update [" + this.getType().Replace(" ","_") + "] set label=label_zt where id='" + this.getID() + "'";
inn.applySQL(sql);

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateActLabel' and [Method].is_current='1'">
<config_id>B76AECBF81B74C92979AD921BCCFC1F7</config_id>
<name>In_UpdateActLabel</name>
<comments>變更任務節點的Label改成中文語系的Label</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
