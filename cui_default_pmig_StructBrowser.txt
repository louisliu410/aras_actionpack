var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame.currItemType && workerFrame.Dependencies && workerFrame.Dependencies.View) {
	workerFrame.Dependencies.View(workerFrame.itemTypeName, inArgs.rowId, false, aras);
	return;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmig_StructBrowser' and [Method].is_current='1'">
<config_id>CC9C23302DE24CA99DC4736E8FC52E5C</config_id>
<name>cui_default_pmig_StructBrowser</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
