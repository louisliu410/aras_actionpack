/*
目的:結束並完成打卡中的派工任務
接收參數:current_workorder_assid,comments
做法:
1.因為是in_workorder的任務執行中節點,因此後面的path一定也只有一條
1.依據 current_workorder_assid 取得 source_id 作為 actid
1.依據actid取得正確的PathID
2.呼叫In_VoteAct_N
*/

string strMethodName = "In_TimerecordEndAndVote";
//System.Diagnostics.Debugger.Break();
// Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
//     bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";



_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

	
try
{
    //要移除 <?xml version="1.0" encoding="utf-8"?>
    if (this.dom.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
    {
      this.dom.RemoveChild( this.dom.FirstChild);
    }
   
	string actassid = this.getProperty("current_workorder_assid","");
	Item itmAss = inn.getItemById("Activity Assignment",actassid);
	string actid = itmAss.getProperty("source_id");
	this.setProperty("actassid",actassid);
	this.setProperty("actid",actid);
	
	aml = "<AML>";
	aml += "<Item type='Workflow Process Path' action='get' select='id,name,label,related_id(cloned_as)'>";
	aml += "<source_id>" + actid + "</source_id>";
	aml += "<related_id>";
	aml += "<Item type='Activity' action='get'>";
	aml += "<cloned_as condition='is null' />";
	aml += "</Item>";
	aml += "</related_id>";
	aml += "</Item></AML>";	
	Item itmPath = inn.applyAML(aml);
	
	
	
	string pathid = itmPath.getID();
	string pathlabel = itmPath.getProperty("label");
	string complete = "1";
	
	this.setProperty("pathid",pathid);
	this.setProperty("pathlabel",pathlabel);
	this.setProperty("complete",complete);
	itmR = this.apply("In_VoteAct_N");
	
}
catch(Exception ex)
{	
// 	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
// if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_TimerecordEndAndVote' and [Method].is_current='1'">
<config_id>E898A3071F8B4AA19FC00553DDD66B29</config_id>
<name>In_TimerecordEndAndVote</name>
<comments>結束並完成打卡中的派工任務</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
