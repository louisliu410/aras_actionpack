'Create Date: 2009/12/21, Creator: Scott
'Modified Date: 2010/01/12, Modified by: Scott

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim form_id As String = Me.getProperty("FormID")
Dim act_name As String = Me.getProperty("ActivityName")
Dim sql_cmd As String = ""

sql_cmd = " Select Convert(Varchar,MAX(ACTIVITY.CLOSED_DATE), 121) As Closed_Date  "
sql_cmd = sql_cmd + " FROM WORKFLOW  "
sql_cmd = sql_cmd + "             INNER JOIN  ACTIVITY  "
sql_cmd = sql_cmd + "             INNER JOIN WORKFLOW_PROCESS_ACTIVITY  "
sql_cmd = sql_cmd + "             INNER JOIN WORKFLOW_PROCESS On WORKFLOW_PROCESS_ACTIVITY.SOURCE_ID = WORKFLOW_PROCESS.ID On  "
sql_cmd = sql_cmd + "             ACTIVITY.ID = WORKFLOW_PROCESS_ACTIVITY.RELATED_ID On WORKFLOW.RELATED_ID = WORKFLOW_PROCESS.ID  "
sql_cmd = sql_cmd + " WHERE (WORKFLOW.SOURCE_ID = '" + form_id + "') AND (ACTIVITY.NAME = N'" + act_name + "')  "

Dim result As Item = inn.applyMethod("ExecuteSQL", "<SQLCMD>" + sql_cmd + "</SQLCMD>")
Dim count As Integer = result.getItemCount()
Dim err As Item

If count <= 0 Then
	err = inn.newError("Fail to get the last approvers.")
	Return err
Else
	Dim closed_date As String = result.getResult()
	
'	sql_cmd = "Select [IDENTITY].NAME "
'	sql_cmd = sql_cmd + " FROM  WORKFLOW_PROCESS INNER JOIN "
'	sql_cmd = sql_cmd + " WORKFLOW On WORKFLOW_PROCESS.ID = WORKFLOW.RELATED_ID INNER JOIN  "
'	sql_cmd = sql_cmd + " WORKFLOW_PROCESS_ACTIVITY On WORKFLOW_PROCESS.ID = WORKFLOW_PROCESS_ACTIVITY.SOURCE_ID INNER JOIN  "
'	sql_cmd = sql_cmd + " [IDENTITY] INNER JOIN  "
'	sql_cmd = sql_cmd + " ACTIVITY_ASSIGNMENT On [IDENTITY].ID = ACTIVITY_ASSIGNMENT.RELATED_ID INNER JOIN  "
'	sql_cmd = sql_cmd + " ACTIVITY On ACTIVITY_ASSIGNMENT.SOURCE_ID = ACTIVITY.ID On WORKFLOW_PROCESS_ACTIVITY.RELATED_ID = ACTIVITY.ID  "
'	sql_cmd = sql_cmd + " WHERE (WORKFLOW.SOURCE_ID = '" + form_id + "') AND (ACTIVITY.NAME = N'" + act_name + "') AND (ACTIVITY.CLOSED_DATE = Convert(datetime, '" + closed_date + "')) "
	
	sql_cmd = "Select  [IDENTITY].NAME "
	sql_cmd = sql_cmd + " FROM [USER] INNER JOIN  "
        sql_cmd = sql_cmd + " ACTIVITY INNER JOIN  "
        sql_cmd = sql_cmd + " ACTIVITY_ASSIGNMENT On ACTIVITY.ID = ACTIVITY_ASSIGNMENT.SOURCE_ID INNER JOIN  "
        sql_cmd = sql_cmd + " WORKFLOW_PROCESS INNER JOIN  "
        sql_cmd = sql_cmd + " WORKFLOW On WORKFLOW_PROCESS.ID = WORKFLOW.RELATED_ID INNER JOIN  "
        sql_cmd = sql_cmd + " WORKFLOW_PROCESS_ACTIVITY On WORKFLOW_PROCESS.ID = WORKFLOW_PROCESS_ACTIVITY.SOURCE_ID On  "
        sql_cmd = sql_cmd + " ACTIVITY.ID = WORKFLOW_PROCESS_ACTIVITY.RELATED_ID On [USER].ID = ACTIVITY_ASSIGNMENT.CLOSED_BY INNER JOIN  "
        sql_cmd = sql_cmd + " Alias On [USER].ID = Alias.SOURCE_ID INNER JOIN  "
        sql_cmd = sql_cmd + " [IDENTITY] On Alias.RELATED_ID = [IDENTITY].ID  "
	sql_cmd = sql_cmd + " WHERE (WORKFLOW.SOURCE_ID = '" + form_id + "') AND (ACTIVITY.NAME = N'" + act_name + "') AND (ACTIVITY.CLOSED_DATE = Convert(datetime, '" + closed_date + "')) "
	
	Return inn.applyMethod("ExecuteSQL", "<SQLCMD>" + sql_cmd + "</SQLCMD>")
End If
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetLastApproversByActivity' and [Method].is_current='1'">
<config_id>89212BCD1124452093AC1B46C199C4CE</config_id>
<name>GetLastApproversByActivity</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
