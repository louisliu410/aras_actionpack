' name: PreferenceKeyedName
Dim identity_id As String = Me.getproperty("identity_id")
Dim identityreq As Item = Me.newitem("Identity","get")
identityreq.setid(identity_id)
Dim identityres As Item = identityreq.apply()
If identityres.iserror() Then
 Return(identityres)
End If
Me.setproperty("keyed_name",identityres.getproperty("keyed_name"))

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PreferenceKeyedName' and [Method].is_current='1'">
<config_id>4C1E95A5A2BF48BB9C14476F1A44DED3</config_id>
<name>PreferenceKeyedName</name>
<comments>computes the keyed name for a preference item</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
