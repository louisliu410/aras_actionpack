var adminIdentity = Aras.Server.Security.Identity.GetByName("Administrators");
bool adminPermsWasSet = Aras.Server.Security.Permissions.GrantIdentity(adminIdentity);
try
{
	Aras.ES.ServerMethods.InitSearchConfigurations(this);
}
finally
{
	if (adminPermsWasSet)
	{
		Aras.Server.Security.Permissions.RevokeIdentity(adminIdentity);
	}
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_InitSearchConfigurations' and [Method].is_current='1'">
<config_id>8C1C474BA7844B3584C416E5462C8A15</config_id>
<name>ES_InitSearchConfigurations</name>
<comments>Init configurations for ES search</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
