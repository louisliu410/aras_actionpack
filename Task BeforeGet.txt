if (this.node != null){
  //convert "my_assignment" condition to appropriate filter of "assigned_to" property
  string identityList = Aras.Server.Security.Permissions.Current.IdentitiesList;
  XmlNodeList nds = this.node.SelectNodes("my_assignment");
  foreach (XmlElement nd in nds){
  	string cond = nd.GetAttribute("condition").ToLowerInvariant();
  	bool isMy = nd.InnerText == "1" && 
  	  (string.IsNullOrEmpty(cond) || cond == "eq" || cond == "like");
  	XmlElement newNd = (XmlElement)nd.ParentNode.AppendChild(nd.OwnerDocument.CreateElement("assigned_to"));
  	newNd.InnerText = identityList;
  	newNd.SetAttribute("condition", isMy ? "in" : "not in");
    nd.ParentNode.RemoveChild(nd);
  }
}

string selectAttr = this.getAttribute("select");
if (string.IsNullOrEmpty(selectAttr) || selectAttr == "*") return this;

if (selectAttr.IndexOf("assigned_to") < 0){
	//assigned_to should be always selected to setup proper classification on AfterGet
	selectAttr += ",assigned_to";
	this.setAttribute("select", selectAttr);
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Task BeforeGet' and [Method].is_current='1'">
<config_id>1A883FBD069D4C189AF9A14F73B40878</config_id>
<name>Task BeforeGet</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
