var viewMode = inArgs.viewMode;
var topWnd = window.TopWindowHelper.getMostTopWindowWithAras();
var aras = topWnd.aras;
if (viewMode !== 'notification') {
	return aras.evalMethod('OnShowItemDefault', inDom, inArgs);
}

var itemTypeInToc = 'Discussion';
var tocItemName = 'My Innovator/Discussion';
var itemType = aras.getItemTypeForClient(itemTypeInToc);
var itemTypeID = itemType.getId();

if (itemType.getErrorCode() !== 0 && !itemTypeID) {
	return;
}

var mainTreeApplet = topWnd.tree.mainTreeApplet;
mainTreeApplet.openItem(mainTreeApplet.getParentId(tocItemName));
mainTreeApplet.selectItem(tocItemName);

topWnd.menu.setAllControlsEnabled(false);

var TOCView = aras.uiGetTOCView4ItemTypeEx(itemType.node);
var url = aras.getItemProperty(TOCView, 'start_page');
var parameters = aras.getItemProperty(TOCView, 'parameters');

if (parameters !== '') {
	parameters += '&';
}

parameters += 'itemtypeID=' + itemTypeID + '&itemtypeName=' + itemTypeInToc + '&startBookmarkId=allmessages';

aras.browserHelper.toggleSpinner(topWnd.document, true);
var workFrame = topWnd.work;
workFrame.location.replace(url + '?' + parameters);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VCN_OnShowItem' and [Method].is_current='1'">
<config_id>8A6D156789EB41B5A806E0F75DA4E39B</config_id>
<name>VCN_OnShowItem</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
