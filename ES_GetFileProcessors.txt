/*
    Get information about installed file processors
    
    Example of request:
    <Item type="Method" action="ES_GetFileProcessors">
    </Item>
    
    Example of response:
    <AML>
       <Item type="Result">
          <Relationships>
             <Item type="FileProcessor">
                <id>localhost.8082</id>
                <host>localhost</host>
                <port>8082</port>
                <vault_id>67BBB9204FE84A8981ED8313049BA06C</vault_id>
             </Item>
             <Item type="FileProcessor">
                <id>localhost.8083</id>
                <host>localhost</host>
                <port>8083</port>
                <vault_id>88BBB9204FE84A8981ED8313049BA06D</vault_id>
             </Item>
       </Item>
    </AML>
*/

return Aras.ES.ServerMethods.GetFileProcessors(this);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_GetFileProcessors' and [Method].is_current='1'">
<config_id>B5FF7484E3034D00AB6C91A9B3186433</config_id>
<name>ES_GetFileProcessors</name>
<comments>Get information about installed file processors</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
