var inn = this.getInnovator();
var seq = inn.getNextSequence(this.getProperty("order_sequence"));
this.setProperty("item_number", seq);
var itmNode = top.aras.dom.selectSingleNode( '//Item[@id="' + this.getID() + '"]');
if (itmNode) { top.aras.setItemProperty(itmNode, "item_number", seq); }
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetNextTempNumber' and [Method].is_current='1'">
<config_id>F6120AFC72A54B35A0A3C34C0CF75E8A</config_id>
<name>GetNextTempNumber</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
