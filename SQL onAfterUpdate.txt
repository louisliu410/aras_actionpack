' name: SQL onAfterUpdate
' created: 5-OCT-2005 George J. Carrette
'MethodTemplateName=VBScriptMain;

Sub Main(inDom As XmlDocument,outDom As XmlDocument,Iob As Object,Svr As Object,Ses As Object)
 cco.xml.setitemproperty(cco.xml.GetRequestItem(indom),"PROCESS","onAfterUpdate")
 cco.applyitem.ExecuteMethodByName(indom,outdom,"SQL PROCESS",True)
End Sub


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SQL onAfterUpdate' and [Method].is_current='1'">
<config_id>FA1A49CE06D04249A7C3A0AA4C39D387</config_id>
<name>SQL onAfterUpdate</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
