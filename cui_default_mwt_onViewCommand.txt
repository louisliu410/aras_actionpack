var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onViewCommand) {
	workerFrame.onViewCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onViewCommand' and [Method].is_current='1'">
<config_id>74217FBDC2554604A543BDEFA8A64ED5</config_id>
<name>cui_default_mwt_onViewCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
