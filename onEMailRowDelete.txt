var topWnd = aras.getMostTopWindowWithAras(window);
if (topWnd.onDeleteEMailRow) {
	topWnd.onDeleteEMailRow();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onEMailRowDelete' and [Method].is_current='1'">
<config_id>787E03F8264446688AEC57AC1233DA59</config_id>
<name>onEMailRowDelete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
