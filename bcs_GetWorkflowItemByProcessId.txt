'Developed by Kenny at Broadway
'Create Date: 2016/03/14

'System.Diagnostics.Debugger.Break() 
Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)

Dim LbwGeneric As bwInnovatorCore.CInnovator.bwGeneric
Dim LasInnovator As Innovator 
Dim LstrWorkflowProcessId As String
Dim LstrWFItemType As String
Dim LstrWFItemId As String
Dim LstrErrorMessage As String
Dim LwfWorkflowItem As Item=Nothing
Dim LasRetItem As Item=Nothing

Try

    LstrWFItemType=""
    LstrWFItemId=""
    LstrErrorMessage=""
    
	LasInnovator=Me.getInnovator()
	LstrWorkflowProcessId = Me.getPropertyAttribute("body", "process_id")

    LbwGeneric =New bwInnovatorCore.CInnovator.bwGeneric 
    LbwGeneric.bwIOMInnovator=LasInnovator

	If LstrWorkflowProcessId <> "" Then
		LwfWorkflowItem = LbwGeneric.GetWorkflowItemByProcessId(LstrWorkflowProcessId)
		
		If LwfWorkflowItem is nothing Then 
	         LstrErrorMessage=LbwGeneric.ErrorException
		Else
		    LstrWFItemType=LwfWorkflowItem.getType
		    LstrWFItemId=LwfWorkflowItem.getID
		End If
	End If
	
	LasRetItem=LasInnovator.newItem()
	LasRetItem.setProperty("item_type",LstrWFItemType)
	LasRetItem.setProperty("item_id",LstrWFItemId)
	LasRetItem.setProperty("message",LstrErrorMessage)
	Return LasRetItem	

Catch ex As Exception
	Throw New Exception(ex.Message)
Finally
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If
End Try

Return Me



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_GetWorkflowItemByProcessId' and [Method].is_current='1'">
<config_id>5626AAB316104EDDA3D16FB941892127</config_id>
<name>bcs_GetWorkflowItemByProcessId</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
