/*
目的:APPAction:編輯按鈕
用法:item.apply
須回傳:
app action
(若回傳 null 則代表不顯示按鈕)

位置:App Action的method
*/
string strMethodName = "In_AppBtn_Edit";
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strError = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmAction = null;

//只有檢視模式才需要處理
if(this.getProperty("sort","")=="in_web_add" || this.getProperty("sort","")=="in_web_edit")
	return  itmAction;

switch(this.fetchLockStatus())
{
	case 0:
		//測試是否可編輯
		try
		{
			this.lockItem();
			this.unlockItem();
		}
		catch (Exception ex)
		{
			return  itmAction;
		}
		break;
	case 1:
		//如果是被自己鎖定則解鎖	
		try
		{
			this.unlockItem();
		}
		catch (Exception ex)
		{
			return  itmAction;
		}
		break;
	case 2:
		return  itmAction;
}

itmAction = inn.newItem();
itmAction.setType("action");
itmAction.setProperty("href","");
itmAction.setProperty("btn_type","report");
/*
string strClick= "ih.GoToItemPage('" + this.getType() + "','" + this.getID() + "','" + this.getType().Replace(" ","_") + "Edit.html','In_Get_SingleItemInfoGeneral',{'fetchproperty':'4'})";		
*/
string strClick= "ih.GoToEditPage('" + this.getType() + "','" + this.getID()+"')";
itmAction.setProperty("click",strClick);
itmAction.setProperty("label","編輯");
itmAction.setProperty("class","");
itmAction.setProperty("data_toggle","");
itmAction.setProperty("data_target","");
	


return itmAction;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AppBtn_Edit' and [Method].is_current='1'">
<config_id>4CA6F83C017F44EAACE92C6D3D029702</config_id>
<name>In_AppBtn_Edit</name>
<comments>APPAction:編輯按鈕</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
