'Created Date: 2009/04/13, Creator: Scott
'Modified Date: 2009/05/26, Modified by: Scott

'Purpose：讀取 待完成專案工作

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator= Me.getInnovator()
Dim err As Item
Dim result As Item
Dim count As Long
Dim user_id As String = Me.getProperty("UserID")
Dim view_mode As String = Me.getProperty("ViewMode")

'================================================================================================================================
Dim identity_id_list As Item = inn.applyMethod("GetIdentityListByUserID","<UserID>" + user_id + "</UserID>")

If identity_id_list.isError() Then
	err = inn.newError("Can not find the identity id list.")
	Return err
End If

'================================================================================================================================
Dim sql_cmd As String = "Select SOURCE_ID FROM innovator.Activity2_Assignment WHERE related_id In (" + identity_id_list.getResult() + ") And STATE != 'Complete' "
result = inn.applyMethod("ExecuteSQL", "<SQLCMD>" + sql_cmd + "</SQLCMD>")

If (result.getItemCount() < 0) And (result.isError()) Then
	err = inn.newError("Can not execute SQL command." & result.getErrorDetail())
	Return err
End If

count = result.getItemCount()
Dim activity2_id_list As String = ""

For i As Integer =0 To count -1
	activity2_id_list = activity2_id_list + "'" + result.getItemByIndex(i).getProperty("source_id") + "'"
	
	If i < count -1 Then
		activity2_id_list = activity2_id_list + ", "
	End If
Next

'================================================================================================================================
sql_cmd = "Select project_number FROM innovator.project WHERE state='Active'"
result = inn.applyMethod("ExecuteSQL", "<SQLCMD>" + sql_cmd + "</SQLCMD>")

If (result.getItemCount() < 0) And (result.isError()) Then
	err = inn.newError("Can not execute SQL command." & result.getErrorDetail())
	Return err
End If

count = result.getItemCount()
Dim project_number_list As String = ""

For i As Integer =0 To count -1
	project_number_list = project_number_list + "'" + result.getItemByIndex(i).getProperty("project_number") + "'"
	
	If i < count -1 Then
		project_number_list = project_number_list + ", "
	End If
Next

'================================================================================================================================
Dim xml_str As String = "<Item type='Activity2' action='get' select='id,proj_num,keyed_name,state,date_start_sched,date_due_sched,locked_by_id,message'>"
xml_str= xml_str + "<state condition='in'>" + view_mode + "</state>"
xml_str= xml_str + "<Or>" 

If (activity2_id_list.length = 0) Then
	xml_str= xml_str + "<id condition='is null' />"  
Else
	xml_str= xml_str + "<id condition='in'>" + activity2_id_list + "</id>"  
End If  

xml_str= xml_str + "<managed_by_id condition='in'>" + identity_id_list.getResult() + "</managed_by_id>"  
xml_str= xml_str + "</Or>"
xml_str= xml_str + "<proj_num condition='is not null'/>" 

If (project_number_list.length > 0) Then
	xml_str= xml_str + "<proj_num condition='in'>" + project_number_list + "</proj_num>"
End If

xml_str= xml_str + "</Item>"

Dim a2 As Item = inn.newItem()
a2.loadAML(xml_str)
result = a2.apply()

If (result.getItemCount() < 0) And (result.isError()) Then
	err = inn.newError("Fail to get assigned project activities." & result.getErrorDetail())
	Return err
End If

Return result
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetAssignedProjectActivities' and [Method].is_current='1'">
<config_id>21CA2010747249D4A1C93A893A27AE9A</config_id>
<name>GetAssignedProjectActivities</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
