//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Dictionary<string, string> dct = new Dictionary<string, string>(this.getItemCount());
for(int i=0;i<this.getItemCount();i++)
{
	Item itmXAffectedItem =this.getItemByIndex(i);
	Item itmAffectedItem =itmXAffectedItem.getRelatedItem();
	if(itmAffectedItem==null)
		continue;
	string strType = itmAffectedItem.getPropertyAttribute("new_item_id","type",itmAffectedItem.getPropertyAttribute("affected_id","type",""));
	string strID = itmAffectedItem.getProperty("new_item_id",itmAffectedItem.getProperty("affected_id",""));
	
	if(strType =="")
		continue;
	
	Item itmNewItem = inn.getItemById(strType, strID);
	string itemtype = itmNewItem.getProperty("itemtype");	
	
	string source_type = null;
	
	if (!dct.TryGetValue(itemtype, out source_type))
	{
		string searchCriteria = "id";
		XmlElement itTypeNode = CCO.Cache.GetItemTypeFromCache(ref itemtype, ref searchCriteria);
		source_type = itTypeNode.GetAttribute("name");
		dct.Add(itemtype, source_type);
	}

	itmAffectedItem.setPropertyAttribute("affected_type","source_type",source_type);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='in_PopulateAffectedType' and [Method].is_current='1'">
<config_id>88A346DA5FDF44C7B47F2570618DB959</config_id>
<name>in_PopulateAffectedType</name>
<comments>補上實際的Affected Type名稱</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
