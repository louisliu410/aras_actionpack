var userItem = aras.getLoggedUserItem();
var identityItem = userItem.selectSingleNode('Relationships/Item[@type=\'Alias\']/related_id/Item[@type=\'Identity\']');
var identityId = aras.getItemProperty(identityItem, 'id');

var qry = aras.newQryItem('Preference');
qry.setSelect('id');
qry.setCriteria('identity_id', identityId);
res = qry.execute();
if (!res) {
	return false;
}

var pref = res.selectSingleNode('//Item[@type=\'Preference\']');
if (pref) {
	var prefId = aras.getItemProperty(pref,'id');
	aras.purgeItem('Preference', prefId, true);
}

aras.resetUserPreferences();
aras.makeItemsGridBlank(false);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Reset preferences' and [Method].is_current='1'">
<config_id>3B14F94F243F49ECAF250BED4F06E292</config_id>
<name>Reset preferences</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
