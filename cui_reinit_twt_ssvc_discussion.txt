if (inArgs.isReinit) {
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calc_tearoff_states');
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}
	return {'cui_disabled': !inArgs.eventState.isDiscussion};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_twt_ssvc_discussion' and [Method].is_current='1'">
<config_id>7460CEC24363413BA54B15DA65725110</config_id>
<name>cui_reinit_twt_ssvc_discussion</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
