var disabled = true;
if (inArgs.isReinit) {
	if (!inArgs.eventState.InBasketEventState) {
		var states = aras.evalMethod('cui_reinit_inbasket_calc_states', '', {eventType: inArgs.eventType});
		inArgs.eventState.InBasketEventState = states.InBasketEventState;
	}

	if (inArgs.eventState.InBasketEventState) {
		var inBasketEventState = inArgs.eventState.InBasketEventState;
		var isFunctionDisabledBasedOnItemTypes = false;
		for (var i = 0; i < inBasketEventState.itemTypesNames.length; i++) {
			if (isFunctionDisabled(inBasketEventState.itemTypesNames[i], 'Lock')) {
				isFunctionDisabledBasedOnItemTypes = true;
				break;
			}
		}
		disabled = !(inBasketEventState.lockFlg && !isFunctionDisabledBasedOnItemTypes);
	}
}

return {'cui_disabled': disabled, 'cui_invisible': disabled};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_pmig_claim' and [Method].is_current='1'">
<config_id>3C6386EE895F494ABC53537CD2061569</config_id>
<name>cui_reinit_pmig_claim</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
