
			var dataAccessLayer = new DataAccessLayer(getInnovator());
			var businessLogic = new BusinessLogic(dataAccessLayer, getInnovator());
			return businessLogic.GetResultItem();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
		internal class BusinessLogic
		{
			private IDataAccessLayer dataAccessLayer;
			private Innovator innovator;
			internal BusinessLogic(IDataAccessLayer dataAccessLayer, Innovator innovator)
			{
				this.dataAccessLayer = dataAccessLayer;
				this.innovator = innovator;
			}

			internal Item GetResultItem()
			{
				Item resultItem = innovator.newItem("Feature License");
				var licId = dataAccessLayer.ConsumeIzendaLicense();

				if (String.IsNullOrEmpty(licId) || !Aras.Server.Core.Utilities.IsIdValid(licId, false))
				{
					return resultItem;
				}
				resultItem.setAction("get");
				resultItem.setID(licId);
				resultItem = dataAccessLayer.ApplyAsAdministrator(resultItem);
				if (resultItem.isError())
				{
					return resultItem;
				}
				else
				{
					string licData = resultItem.getProperty("additional_license_data", "");
					string tmpLicId = resultItem.getID();

					//to return additional_license_data only
					resultItem = innovator.newItem(resultItem.getAttribute("type"));
					resultItem.setAttribute("id", tmpLicId);
					resultItem.setProperty("additional_license_data", licData);
				}
				return resultItem;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
		internal interface IDataAccessLayer
		{
			string ConsumeIzendaLicense();		
			Item ApplyAsAdministrator(Item item);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
		internal class DataAccessLayer : IDataAccessLayer
		{
			private Innovator innovator;

			internal DataAccessLayer(Innovator innovator)
			{
				this.innovator = innovator;
			}

			public Item ApplyAsAdministrator(Item item)
			{
				var identity = Aras.Server.Security.Identity.GetByName("Administrators");
				bool permissionWasSet = false;
				try
				{
					permissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(identity);
					return item.apply();
				}
				finally
				{
					if (permissionWasSet)
					{
						Aras.Server.Security.Permissions.RevokeIdentity(identity);
					}
				}
			}

			public string ConsumeIzendaLicense()
			{
				return innovator.ConsumeLicense("Aras.SelfServiceReporting");
			}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetSelfReportingLicenseData' and [Method].is_current='1'">
<config_id>1229085D30A040558812639385ABDF11</config_id>
<name>GetSelfReportingLicenseData</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
