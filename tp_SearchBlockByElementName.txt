var searchNames = inArgs.itemContext ? inArgs.itemContext.searchNames : null;

if (searchNames) {
	var rootOR = inDom.ownerDocument.createElement('OR');
	var innerAND = inDom.ownerDocument.createElement('AND');
	var innerOR = inDom.ownerDocument.createElement('OR');
	var conditionNode;
	var i;

	inDom.appendChild(rootOR);
	conditionNode = inDom.ownerDocument.createElement('root_element_type');
	conditionNode.text = 'Multiple';
	rootOR.appendChild(conditionNode);

	conditionNode = inDom.ownerDocument.createElement('root_element_type');
	conditionNode.text = 'Empty';
	rootOR.appendChild(conditionNode);

	conditionNode = inDom.ownerDocument.createElement('root_element_type');
	conditionNode.text = 'Unknown';
	rootOR.appendChild(conditionNode);

	// append AND condition
	rootOR.appendChild(innerAND);
	innerAND.appendChild(innerOR);

	conditionNode = inDom.ownerDocument.createElement('root_element_type');
	conditionNode.text = 'Single';
	innerAND.appendChild(conditionNode);

	searchNames = searchNames.length ? searchNames : ['aras:unrealnodename'];

	for (i = 0; i < searchNames.length; i++) {
		conditionNode = inDom.ownerDocument.createElement('root_element_name');
		conditionNode.text = searchNames[i];

		innerOR.appendChild(conditionNode);
	}
}

inDom.removeAttribute('LastModifiedOn');

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_SearchBlockByElementName' and [Method].is_current='1'">
<config_id>75B4E4AE3B6044B2ACDAC224FE29FC8E</config_id>
<name>tp_SearchBlockByElementName</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
