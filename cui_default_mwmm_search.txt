var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.searchContainer) {
	workerFrame.searchContainer.runSearch();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_search' and [Method].is_current='1'">
<config_id>0DD4F7DDEC26419B8D2E26D4113B9D1E</config_id>
<name>cui_default_mwmm_search</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
