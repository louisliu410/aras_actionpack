'(c) Copyright by IRES Corporation (Aras GC), 2011-2020.
'Created Date: 2011/01/13, Creator: Scott

'System.Diagnostics.Debugger.Break() 

Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)
'===========================================================================================

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim to_state As String = "Obsolete"
Dim item_type_name As String = Me.GetType()
Dim item_type_id As String = Me.getAttribute("typeId", "")
Dim item_id As String = Me.getID()
Dim item_type As Item
Dim itm As Item
Dim item_config_id As String

'=================================================================================================================
'1.取得 ItemType Name
If item_type_name = "" Then
	item_type = inn.getItemById("ItemType", item_type_id)
	
	If (item_type Is Nothing) OrElse (item_type.isError()) Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If
		
		err  = inn.newError("Fail to get the ItemType. ItemType ID = " & item_type_id)
		Return err
	End If
	
	item_type_name = item_type.getProperty("name")
End If

'=================================================================================================================
'2.取得 item_config_id
itm = inn.getItemById(item_type_name, item_id)

If (itm Is Nothing) OrElse (itm.isError()) Then
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If
		
	err = inn.newError("Fail to get the itm. ItemType Name = " & item_type_name & ", Item ID = " & item_type_id)
	Return err
End If

item_config_id = itm.getProperty("config_id")

'=================================================================================================================
'3.取得前一個生效版本存在？
Dim pre_release_aml As String = "<Item type='" + item_type_name + "' action='get'>"
pre_release_aml = pre_release_aml + "<config_id>" + item_config_id + "</config_id>"
pre_release_aml = pre_release_aml + "<generation condition='in'>SELECT MAX(generation) FROM [" + item_type_name + "] WHERE config_id='" + item_config_id + "' AND is_released='1' AND is_current='0'</generation>"
pre_release_aml = pre_release_aml+ "</Item>"
			
Dim result As item = inn.applyAML("<AML>" + pre_release_aml  + "</AML>")
			
If Not result.isError() Then   '存在前一版
	'如果存在前一個生效版本，取出他的 ID
	Dim pre_released_id As String = result.getProperty("id")
	Dim pre_released_item As Item = inn.getItemById(item_type_name, pre_released_id)
	
	' 將前一個生效版本推到 Obsolete
	pre_released_item.setAction("PromoteItem")
	pre_released_item.setProperty("state", to_state)
	result = pre_released_item.apply()
	
	If result.isError() Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If

		err = inn.newError("Fail to promote " + item_type_name + " to " + to_state + ". " + result.getErrorString())
		Return err 
	End If
End If


If (PermissionWasSet=True) Then
	Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
End If

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PromotePreReleaseToObsolete' and [Method].is_current='1'">
<config_id>5238CE6F05DC4CD0B1186171A41ECA5A</config_id>
<name>PromotePreReleaseToObsolete</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
