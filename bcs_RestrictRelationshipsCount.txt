/*
 * 設定方式
 * Activity : onAfterAdd,OnAfterUpdate
 * 最多:ActionPack.RestrictRelationshipType.Max   至少:ActionPack.RestrictRelationshipType.Min
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.RestrictRelationshipsCount(this, "NPR Part", 1, bcsActionPack.ActionPack.RestrictRelationshipType.Min);
	
	if (itmResult.isError()) return itmResult;

    return this;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_RestrictRelationshipsCount' and [Method].is_current='1'">
<config_id>67F57F1CB9FD452DA965C113E8C29764</config_id>
<name>bcs_RestrictRelationshipsCount</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
