Item project = this.getPropertyItem("project");//clone of the project (not the original project)
string projectNumber = project.getProperty("project_number");
string newDeliverablePrefix = projectNumber + "-"; // you can set it, e.g., to ""
string newDeliverableSuffix = ""; // you can set it, e.g., to "-" + projectNumber

string newFilePrefix = projectNumber + "-"; //you can set it, e.g., to ""
string newFileSuffix = ""; // you can set it, e.g., to "-" + projectNumber

int indexDeliverables = 0;
while (true)
{
	//setting projectDeliverableName
	Item deliverable = this.getPropertyItem("deliverable" + indexDeliverables);
	if (deliverable == null)
	{
		break;
	}

	string deliverableNameOld = deliverable.getProperty("deliverable_name");
	deliverable.setProperty("deliverable_name", newDeliverablePrefix + deliverableNameOld + newDeliverableSuffix);	
	indexDeliverables++;
			
	//setting file names of project Deliverable		
	int indexFiles = 0;
	while (true)
	{	
		string fileNameOld = deliverable.getProperty("file_name" + indexFiles);
		if (string.IsNullOrEmpty(fileNameOld))
		{
			break;
		}
			
		deliverable.setProperty("file_name" + indexFiles, newFilePrefix + fileNameOld + newFileSuffix);
		indexFiles++;
	}
}
		
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Project_CloneNumberingAndNaming' and [Method].is_current='1'">
<config_id>D5D321588C2C4568A3D9BA1DC36F953E</config_id>
<name>Project_CloneNumberingAndNaming</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
