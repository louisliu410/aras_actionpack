if (inArgs.isReinit) {
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calculate_states', '', {eventType: inArgs.eventType});
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}

	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame) {
		return {'cui_disabled': !(inArgs.eventState.isLock && !isFunctionDisabled(workerFrame.itemTypeName, 'Lock'))};
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_lock' and [Method].is_current='1'">
<config_id>B2852BE14C6348CBBEFC398ECE62A6E2</config_id>
<name>cui_reinit_mwt_lock</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
