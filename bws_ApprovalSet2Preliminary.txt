// Grant 'Aras PLM' permissionsa
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
Boolean PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
string rootXPath = "/*/*/Result/Item/related_id/";

//Approval Promote
string affItemRelType = "Approval DOC Review";
string FromState = "In Review";
string toState = "Preliminary";

// Retrieve the Affected Items
Item affItemRel = this.newItem(affItemRelType, "get");
affItemRel.setAttribute("select", "related_id");
affItemRel.setProperty("source_id", this.getID());
Item affItem = affItemRel.createRelatedItem("Approval", "get");
affItem.setAttribute("select", "related_id(keyed_name,state,itemtype)");
affItemRel = affItemRel.apply();
if (affItemRel.getItemCount() < 0)
{
	return inn.newError("Error retrieving Affected Approval: " + affItemRel.getErrorDetail());
}

Item result;
result = inn.applyMethod("bcs_AffectedPromote","<ID>" + this.getID() + "</ID><State>" + this.getProperty("state") + "</State><FromState>" + FromState +"</FromState><ToState>" + toState +"</ToState><ReleationshipItemtype>" + affItemRelType + "</ReleationshipItemtype><KeyNameField>item_number</KeyNameField><EndNodeItemtype>Approval</EndNodeItemtype>");


//Part Promote
string affItemRelType2 = "Approval Part Review";


// Retrieve the Affected Items
Item affItemRel2 = this.newItem(affItemRelType2, "get");
affItemRel2.setAttribute("select", "related_id");
affItemRel2.setProperty("source_id", this.getID());
Item affItem2 = affItemRel2.createRelatedItem("Part", "get");
affItem2.setAttribute("select", "related_id(keyed_name,state,itemtype)");
affItemRel2 = affItemRel2.apply();
if (affItemRel2.getItemCount() < 0)
{
	return inn.newError("Error retrieving Affected Part： " + affItemRel2.getErrorDetail());
}

Item result2;
result2 = inn.applyMethod("bcs_AffectedPromote","<ID>" + this.getID() + "</ID><State>" + this.getProperty("state") + "</State><FromState>" + FromState +"</FromState><ToState>" + toState +"</ToState><ReleationshipItemtype>Approval Part Review</ReleationshipItemtype><KeyNameField>item_number</KeyNameField><EndNodeItemtype>Part</EndNodeItemtype>");



if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bws_ApprovalSet2Preliminary' and [Method].is_current='1'">
<config_id>481B6B478B114BEBA38614873B980143</config_id>
<name>bws_ApprovalSet2Preliminary</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
