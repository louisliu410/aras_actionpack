if(top.aras.isDirtyEx(inDom) || top.aras.isNew(inDom))
{
  if(top.aras.confirm(top.aras.getResource("plm", "changeitem.saveit")))
  {
    var tmpOnSaveCommand;
    if (top.onSaveCommand) tmpOnSaveCommand = top.onSaveCommand;
    if (top.main && top.main.work && top.main.work.onSaveCommand) tmpOnSaveCommand = top.main.work.onSaveCommand;
    tmpOnSaveCommand();
  }
  else
  {
    return;
  }
}

var aml = "<Item type='Method' action='PE_ChangeItemTransition'>" + inDom.xml + "</Item>";
var soap_res = top.aras.soapSend("ApplyMethod", aml);
var message_string;
if (soap_res.getFaultCode() != 0)
{
  message_string = soap_res.getFaultString();
  //fault_string = fault_string.replace(/\n/g, "<br/>");
}
else
{
  message_string = top.aras.getResource("plm", "changeitem.completenesschecksucceeded", inDom.getAttribute('type'));
}


var param = new Object();
param.title  = "Completeness Check Results";
param.aras   = top.aras;
param.message_string = message_string;
  
var form = top.aras.getItemByName("Form", "PE_ChangeItemValidationDialog", "width,height");
  
var width  = top.aras.getItemProperty(form, "width");
var height = top.aras.getItemProperty(form, "height");
if (!width)  width  = 600;
if (!height) height = 250;
    
param.formId = form.getAttribute("id");
var options = {
  dialogWidth: width,
  dialogHeight: height
};
var wnd = top.aras.getMainWindow();
wnd = wnd === top ? wnd.main : top;
res = top.aras.modalDialogHelper.show('DefaultPopup', wnd, param, options, 'ShowFormAsADialog.html');

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_CompletenessCheck' and [Method].is_current='1'">
<config_id>88C2283FD1664AAB9C51321624232F35</config_id>
<name>PE_CompletenessCheck</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
