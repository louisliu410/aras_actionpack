var xpath = "//Item/Relationships/Item/related_id/Item[@id='" + relatedID + "']";
var relitm = item.selectSingleNode(xpath);
if (relitm == null) {
   return
}

if (relitm.getAttribute('type')=="Non Conf  Mat Purge") {
   var partid=top.aras.getItemProperty(item,"part");
   var plantid=top.aras.getItemProperty(item,"corp_plant");
   top.aras.setItemProperty(relitm,'part',partid);
   top.aras.setItemProperty(relitm,'corp_plant',plantid);
}
if (relitm.getAttribute('type')=="QS Stop Ship") {
   var partid=top.aras.getItemProperty(item,"part");   
   top.aras.setItemProperty(relitm,'part',partid);
}
if (relitm.getAttribute('type')=="Hold Notice") {
   var partid=top.aras.getItemProperty(item,"part");
   var buid=top.aras.getItemProperty(item,"corp_bu");
   var plantid=top.aras.getItemProperty(item,"corp_plant");
   top.aras.setItemProperty(relitm,'part',partid);
   top.aras.setItemProperty(relitm,'corp_bu',buid);
   top.aras.setItemProperty(relitm,'corp_plant',plantid);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='QS Containment OnInsert' and [Method].is_current='1'">
<config_id>BD89FBC1B15F483A8A407E3EB65A261E</config_id>
<name>QS Containment OnInsert</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
