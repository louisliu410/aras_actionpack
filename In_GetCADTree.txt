/*
目的:取得APP CAD 多階BOM
做法:
*/
string strMethodName = "In_GetCADTree";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string strError = "";
//string strUserId = inn.getUserID();
//string strIdentityId = inn.getUserAliases();

string strCADID = this.getProperty("id","");

_InnH.AddLog(strMethodName,"MethodSteps");

Item itmMultiBOM = inn.newItem("CAD","bcs_PE_GetMultilCADBom");
itmMultiBOM.setID(strCADID);
itmMultiBOM = itmMultiBOM.apply();
Item itmRootCAD = inn.getItemById("CAD",strCADID);
Item itmTreeElements = BomTreeToAppTree(itmMultiBOM.getResult(),itmRootCAD,inn);

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmTreeElements;
}

private Item BomTreeToAppTree(string TreeResult, Item RootItem,Innovator inn)
{
	Item itmR = inn.newItem();
	XmlDocument xmlBomTree = new XmlDocument();
	xmlBomTree.LoadXml(TreeResult);
	XmlNodeList nlTRs = xmlBomTree.SelectNodes("//tr[@level='0']");

	//在此修改 Bom Tree 的欄位
	itmR.setProperty("gridData_rowItemID", RootItem.getID());
	itmR.setProperty("item_number", RootItem.getProperty("item_number"));
	itmR.setProperty("revision", RootItem.getProperty("major_rev"));
	itmR.setProperty("state", RootItem.getProperty("state"));
	itmR.setProperty("sequence", RootItem.getProperty("sort_order"));
	
	if (RootItem.fetchLockStatus() == 0)
		itmR.setProperty("locked_by", "");
	else if (RootItem.fetchLockStatus() == 1)
		itmR.setProperty("locked_by", "me");
	else
		itmR.setProperty("locked_by", "others");
	itmR.setProperty("name", RootItem.getProperty("name"));
	
	itmR.setProperty("source_id", "");
	itmR.setProperty("level", "0");
	BomToAppTreeChildren(itmR, nlTRs, RootItem.getID(), 0,inn);
	return itmR;
}

private Item BomToAppTreeChildren(Item Bom, XmlNodeList MeNodes, string ParentID, int ParentLevel,Innovator inn)
{
	string strMyLevel = (ParentLevel + 1).ToString();
	foreach (XmlNode MeNode in MeNodes)
	{
		Item itmChildNode = inn.newItem();
		XmlNodeList xnUserDatas = MeNode.SelectNodes("userdata");
		foreach (XmlNode xnUserData in xnUserDatas)
		{
			itmChildNode.setProperty(xnUserData.Attributes["key"].InnerText, xnUserData.Attributes["value"].InnerText);
		}
		XmlNodeList xnTDs = MeNode.SelectNodes("td");
		//在此修改 Bom Tree 的欄位
		itmChildNode.setProperty("item_number", xnTDs[0].InnerText);
		itmChildNode.setProperty("revision", xnTDs[1].InnerText);
		itmChildNode.setProperty("state", xnTDs[2].InnerText);
		itmChildNode.setProperty("sequence", xnTDs[3].InnerText);
		
		if (xnTDs[4].InnerXml.Trim() == "<emptytag />")
			itmChildNode.setProperty("locked_by", "");
		else if (xnTDs[4].InnerXml.Contains("LockedByMe"))
			itmChildNode.setProperty("locked_by", "me");
		else
			itmChildNode.setProperty("locked_by", "others");
		itmChildNode.setProperty("name", xnTDs[5].InnerText);
		
		itmChildNode.setProperty("source_id", ParentID);
		itmChildNode.setProperty("level", strMyLevel);
				
		Bom.appendItem(itmChildNode);
		XmlNodeList MyChildeNodes = MeNode.SelectNodes("tr");
		if (MyChildeNodes.Count > 0)
			BomToAppTreeChildren(Bom, MyChildeNodes, itmChildNode.getProperty("gridData_rowItemID"), ParentLevel + 1,inn);
	}

	return Bom;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetCADTree' and [Method].is_current='1'">
<config_id>6CB2C9944D5243D8B4E0F525E3811709</config_id>
<name>In_GetCADTree</name>
<comments>取得APP CAD 多階BOM</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
