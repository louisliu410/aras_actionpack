var retObj = {};
retObj['is_alias'] = {filterValue: '0', isFilterFixed: true,};
retObj.classification = {filterValue: 'Team', isFilterFixed: true,};
return retObj;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='TeamIdentity_ClassFilter' and [Method].is_current='1'">
<config_id>DF862E6E4C4646BF825D3725A92829BA</config_id>
<name>TeamIdentity_ClassFilter</name>
<comments>Sets a filter so that only Team identities may be chosen. Called OnSearchDialog on the team_role property of Team Identity relationship.</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
