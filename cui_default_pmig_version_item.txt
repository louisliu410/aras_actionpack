var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
var itemId = workerFrame.grid.getSelectedId();

if (!itemId) {
	return;
}

var item = aras.getItemById(workerFrame.itemTypeName, itemId, 0);
if (!item) {
	return;
}

var itemTypeName = item.getAttribute('type');
var res = aras.versionItem(itemTypeName, itemId);
if (res && workerFrame.isItemsGrid) {
	var id = res.getAttribute('id');
	if (workerFrame.grid.getRowIndex(id) === -1) {
		workerFrame.deleteRow(item);
		workerFrame.updateRow(res);
		workerFrame.onSelectItem(id);
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmig_version_item' and [Method].is_current='1'">
<config_id>6881B6AABCFA4621AAFD9B4A40BCE391</config_id>
<name>cui_default_pmig_version_item</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
