var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onViewProcessCommand) {
	workerFrame.onViewProcessCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_inbasket_viewProcess' and [Method].is_current='1'">
<config_id>875228DF516F44D4B0023343F343A1C0</config_id>
<name>cui_default_inbasket_viewProcess</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
