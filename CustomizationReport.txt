Innovator innov=this.getInnovator();
I18NSessionContext cntx=innov.getI18NSessionContext();
string id = this.getID();
Item dbu=innov.getItemByKeyedName("DatabaseUpgrade",id);
string since=dbu.getProperty("applied_on");
Item qry1 = this.newItem("SQL","SQL PROCESS");
qry1.setProperty("name","CustomizationReport");
qry1.setProperty("PROCESS","CALL");
qry1.setProperty("ARG1", id);
Item res = qry1.apply();
if (res.isError()) {return res;	}
Item qry2=this.newItem("Header");
qry2.setProperty("applied_on",since);
qry2.setProperty("db",CCO.DB.DatabaseName);
if (res.getResult()=="") {res=qry2;}
else {res.appendItem(qry2);}

return  res;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CustomizationReport' and [Method].is_current='1'">
<config_id>E394BF41A3464140B7E274FA3C8F1819</config_id>
<name>CustomizationReport</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
