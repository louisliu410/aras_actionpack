'Created Date: 2010/09/16, Creator: Scott

'Purpose：Promote to InReview

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim result As Item

result = inn.applyMethod("AffectedPartPromote","<ID>" + Me.getID() + "</ID><State>" + Me.getProperty("state") + "</State><FromState>Preliminary</FromState><ToState>In Review</ToState><RelName>PNRF Part</RelName>")

If result.isError() Then
	err = inn.newError("Fail to promote part to InReview state. " + result.getErrorString() + " " + result.getErrorDetail())
	Return err 
End If

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PNRFPromotePartToInReview' and [Method].is_current='1'">
<config_id>35397188F5814C6E834C97F1A678CA65</config_id>
<name>PNRFPromotePartToInReview</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
