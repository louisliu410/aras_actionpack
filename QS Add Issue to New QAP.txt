
//Modify by kenny 2016/07/09
//debugger;

var myItem = this;
var newQAP = myItem.newItem('QAP','add');
newQAP.setProperty('problem_stmt', myItem.getProperty('description',''));
newQAP.setProperty('part', myItem.getProperty('part_id',''));
newQAP.setProperty('customer', myItem.getProperty('customer_id',''));
newQAP.setProperty('manufacturer', myItem.getProperty('supplier_id',''));
newQAP.setProperty('owned_by_id', myItem.getProperty('owned_by_id',''));
newQAP.setProperty('part', myItem.getProperty('part',''));

//Modify by kenny 2016/07/09 -------------------------------------------------
var strVariable = "";
var asInnovator = this.getInnovator();
var queryItem = asInnovator.newItem('Variable','get');
queryItem.setProperty('name','bcs_QSIssueToQAP');
queryItem = queryItem.apply();
if (queryItem!==null){
    if (queryItem.isError()===false) {
        if (queryItem.getItemCount()>0) strVariable=queryItem.getItemByIndex(0).getProperty("value");
    }
}

if (strVariable === null) strVariable = "";
if (strVariable !== "")
{
    var resultFst = strVariable.split(";");
    for (var i=0;i<resultFst.length ;i++){
        var resultNext = resultFst[i].split("@");
        if (resultNext.length == 2){
            if (",problem_stmt,customer,manufacturer,owned_by_id,part".indexOf(resultNext[1])<0){
                //alert(resultNext[1] + ":" + resultNext[0] + "=" + myItem.getProperty(resultNext[0], ""))
                newQAP.setProperty(resultNext[1], myItem.getProperty(resultNext[0], ""));
            }
        }
    }                 
}
//------------------------------------------------------------------------------

var newRel = myItem.newItem('QAP Quality Events','add');
newRel.setProperty('itemtype','Issue');
newRel.setPropertyItem('related_id',myItem );
newQAP.addRelationship(newRel);

var res=newQAP.apply();
newQAP = res.getItemByIndex(0);

top.aras.uiShowItemEx(newQAP.node,undefined,true);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='QS Add Issue to New QAP' and [Method].is_current='1'">
<config_id>271E668B5BBB4439BCACBEF0702062EE</config_id>
<name>QS Add Issue to New QAP</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
