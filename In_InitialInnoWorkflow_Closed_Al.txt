/*
目的:填滿簽核物件、Activity,Workflow Process等 Item 的 Inno Workflow 屬性
做法:
1.從 Workflow Process 抓到 ControlledItem與Current Activity
2.更新 Controlled Item, Activity, Workflow Process
*/
string strMethodName = "In_InitialInnoWorkflow_Closed_All";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Aras.Server.Core.InnovatorDatabase conn = CCO.Variables.InnDatabase;
string strError = "";
//string strUserId = inn.getUserID();
//string strIdentityId = inn.getUserAliases();

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

//Closed,Cancelled
aml = "<AML>";
aml += "<Item type='Workflow Process Activity' action='get' select='source_id(state,name),related_id(name,label,state)'>";
aml += "<source_id>";
aml += "<Item type='Workflow Process' action='get'>";
aml += "<state condition='in'>'Cancelled','Closed'</state>";
aml += "</Item>";
aml += "</source_id>";
aml += "<related_id>";
aml += "<Item type='Activity' action='get'>";
aml += "<is_end>1</is_end>";
aml += "<state>Closed</state>";
aml += "</Item>";
aml += "</related_id>";
aml += "</Item></AML>";
CCO.Utilities.WriteDebug("In_InitialInnoWorkflow_Closed_All","Get Closed,Cancelled Workflow Process-Start");
Item itmWPAs = inn.applyAML(aml);
CCO.Utilities.WriteDebug("In_InitialInnoWorkflow_Closed_All","Get Closed,Cancelled Workflow Process-End");


string strWorkflowProcessNames = "";
for(int i=0;i<itmWPAs.getItemCount();i++)
{
	Item itmWPA = itmWPAs.getItemByIndex(i);
	if(itmWPA.isNew())
		continue;
	Item itmWFP = itmWPA.getPropertyItem("source_id");
	Item itmControlledItem = _InnH.GetControlledItem(itmWPA.getPropertyItem("source_id"));
	if(itmControlledItem.isError())
		continue;
	
		string strCurrentActivityLabel =  itmWPA.getPropertyItem("related_id").getProperty("label","");
	if(strCurrentActivityLabel=="")
		strCurrentActivityLabel =  itmWPA.getPropertyItem("related_id").getProperty("name","");	

	
	sql = "Update [" + itmControlledItem.getType().Replace(" ","_") + "] set ";
	sql += " in_current_activity=N'" + strCurrentActivityLabel + "'";
	sql += " ,in_current_activity_id='" + itmWPA.getProperty("related_id") + "'";
	sql += " ,in_wfp_id='" + itmWPA.getProperty("source_id") + "'";
	sql += " ,in_wfp_state='" + itmWPA.getPropertyItem("source_id").getProperty("state") + "'";
	sql += " where id='" + itmControlledItem.getID() + "'";	
	sql += ";";

	string strKeyedName = itmControlledItem.getProperty("keyed_name");			
	strKeyedName.Replace("\n"," ");
		if(strKeyedName.Length>200)
			strKeyedName = strKeyedName.Substring(0,200);
		
	
	string name = "name";
	string type = itmControlledItem.getType();
	XmlElement itTypeNode = CCO.Cache.GetItemTypeFromCache(ref type, ref name);
	string strItemTypeLabel = itTypeNode.GetAttribute("label_plural");
	
	
	sql += "Update [Workflow_Process] set ";
	sql += "in_current_activity=N'" + strCurrentActivityLabel + "'";
	sql += ",in_current_activity_name=N'" + itmWPA.getPropertyItem("related_id").getProperty("name","")  + "'";
	sql += ",in_current_activity_id=N'" + itmWPA.getProperty("related_id") + "'";
	sql += ",keyed_name=N'" + strKeyedName + "'";
	sql += ",[name]=N'" + strKeyedName + "'";
	sql += ",in_config_id='" + itmControlledItem.getProperty("config_id","") + "'";
	sql += ",in_item_id='" + itmControlledItem.getProperty("id","") + "'";
	sql += ",in_itemtype='" + itmControlledItem.getType() + "'";
	sql += ",in_itemtype_label=N'" + strItemTypeLabel + "'";
	sql += " where id='" + itmWPA.getProperty("source_id") + "'";
	sql += ";";
	
	sql += "Update [Activity] set ";
	sql += "in_config_id='" + itmControlledItem.getProperty("config_id","") + "'";
	sql += ",in_itemtype='" + itmControlledItem.getType() + "'";
	sql += ",in_wfp_id='" + itmWPA.getProperty("source_id") + "'";
	sql += " where id ='" + itmWPA.getProperty("related_id") + "'";
	sql += ";";
	
	conn.ExecuteSQL(sql);
	
	strWorkflowProcessNames += itmWPA.getPropertyItem("source_id").getProperty("name") + ",";
	string strLog = "WFP:" + (i+1).ToString() + "/" + itmWPAs.getItemCount().ToString();
	strLog += ",[" + itmWFP.getProperty("state") + "]";
	strLog += "-[" + itmControlledItem.getProperty("keyed_name") + "]";
	strLog += "-[" + itmWPA.getPropertyAttribute("related_id","keyed_name") + "]";
	CCO.Utilities.WriteDebug("In_InitialInnoWorkflow_Closed_All",strLog);
		
	
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
CCO.Utilities.WriteDebug("In_InitialInnoWorkflow_Closed_All",strWorkflowProcessNames);
return inn.newResult(strWorkflowProcessNames);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_InitialInnoWorkflow_Closed_Al' and [Method].is_current='1'">
<config_id>AE29C62474754198B09C8384595D63E5</config_id>
<name>In_InitialInnoWorkflow_Closed_Al</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
