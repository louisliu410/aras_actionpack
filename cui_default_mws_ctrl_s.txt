if (this.work && this.work.grid) {
	var workFrame = this.work;
	var selectedIds = workFrame.grid.getSelectedItemIds();
	if (selectedIds.length) {
		var isSaveEnabled;
		var isFuncDisabled = isFunctionDisabled(workFrame.itemTypeName, 'Save');
		for (var i = 0; i < selectedIds.length; i++) {
			var itemNode = aras.getItemById(workFrame.itemTypeName, selectedIds[i], 0);
			if (itemNode) {
				var userId = aras.getCurrentUserID();
				var isTemp = aras.isTempEx(itemNode);
				var discoverOnlyFlg = (itemNode.getAttribute('discover_only') == '1');
				var lockedByMe = aras.getItemProperty(itemNode, 'locked_by_id') == userId;
				isSaveEnabled = ((isTemp || lockedByMe) && !discoverOnlyFlg) && aras.getFromCache(selectedIds[i]);
				if (!isSaveEnabled) {
					return;
				}
			} else {
				isSaveEnabled = false;
				break;
			}
		}

		if (isSaveEnabled) {
			this.menu.processCommand('save');
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mws_ctrl_s' and [Method].is_current='1'">
<config_id>2B1BF29712C649139BCBA87B35DE0B5E</config_id>
<name>cui_default_mws_ctrl_s</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
