'Create Date: 2010/11/02, Creator: Scott

'System.Diagnostics.Debugger.Break() 

Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)
'===========================================================================================

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim ident_id As String = Me.getProperty("IdentityID", "")
Dim result_str As String = "ok"

Dim ident As Item = inn.newItem("Identity", "get")
ident.setProperty("id", ident_id)
ident.setAttribute("select", "id, name")
Dim result As Item = ident.apply()

If result.isError() Then
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If
	
	err = inn.newError(result.getErrorString() & " " & result.getErrorDetail())
	Return err
End If

Dim ident_name As String = result.getProperty("name", "")

If ident_name = "Creator" Then
	result_str = "created_by_id"
ElseIf ident_name = "Manager" Then
	result_str = "managed_by_id"
ElseIf ident_name = "Owner" Then
	result_str = "owned_by_id"
End If

'=====================================================================================================
If (PermissionWasSet=True) Then
	Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
End If

Return inn.newResult(result_str)
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='RecognizeBuildInIdentity' and [Method].is_current='1'">
<config_id>F99AAFFC5DA34377B58232E40E9DEFAC</config_id>
<name>RecognizeBuildInIdentity</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
