var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onPurgeCommand) {
	workerFrame.onPurgeCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onPurge' and [Method].is_current='1'">
<config_id>0AF14F6A40694EA487FBB1631E82F68C</config_id>
<name>cui_default_mwmm_onPurge</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
