/*
 * Run Server Method
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	string strAct2ReminderMessageName = "bcs_Activity2Reminder_zt";
	string strResult = "";
	
	bcsNotificationLite.Notification _Notification = new bcsNotificationLite.Notification(inn);
	
	strResult = _Notification.SendAct2Reminder(strAct2ReminderMessageName);
	
	return inn.newResult(strResult);
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_SendAct2Reminder_zt' and [Method].is_current='1'">
<config_id>0F572EBDF9EB4D3F973DD037BEB40758</config_id>
<name>bcs_SendAct2Reminder_zt</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
