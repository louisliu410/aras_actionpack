//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Item itmInWFFs = inn.newItem("In_WorkflowForm","get");
itmInWFFs.setProperty("xin_wfp_state","Active");
itmInWFFs.setPropertyAttribute("xin_wfp_id","condition","is not null");
itmInWFFs = itmInWFFs.apply();
string sql  = "";
for(int i=0;i<itmInWFFs.getItemCount();i++)
{
	Item itmInWFF = itmInWFFs.getItemByIndex(i);
	sql = "Update workflow_process set ";
	sql += "in_classification=N'" + itmInWFF.getProperty("xin_classification","") + "'";
	sql += ",in_config_id=N'" + itmInWFF.getProperty("in_config_id","") + "'";
	sql += ",in_current_activity=N'" + itmInWFF.getProperty("xin_current_activity","") + "'";
	sql += ",in_current_activity_id=N'" + itmInWFF.getProperty("xin_current_activity_id","") + "'";
	sql += ",in_current_activity_name=N'" + itmInWFF.getProperty("xin_current_activity_name","") + "'";
	sql += ",in_description=N'" + itmInWFF.getProperty("xin_description","") + "'";
	sql += ",in_generation=N'" + itmInWFF.getProperty("xin_generation","") + "'";
	sql += ",in_item_id=N'" + itmInWFF.getProperty("in_item_id","") + "'";
	sql += ",in_itemtype=N'" + itmInWFF.getProperty("in_itemtype","") + "'";
	sql += ",in_itemtype_label=N'" + itmInWFF.getProperty("xin_itemtype_label","") + "'";
	sql += ",in_wff_id=N'" + itmInWFF.getID() + "'";
	sql += " where id='" + itmInWFF.getProperty("xin_wfp_id","") + "'";
	inn.applySQL(sql);
}

return this;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateWFFToWFP_All' and [Method].is_current='1'">
<config_id>31DE4FC94CA444C192EA41E85497FFA2</config_id>
<name>In_UpdateWFFToWFP_All</name>
<comments>一次性,將Workflow Form 轉移到 WorkflowProcess</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
