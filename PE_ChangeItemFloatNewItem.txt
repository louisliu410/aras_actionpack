string new_item_id = this.getProperty("new_item_id");
if(string.IsNullOrEmpty(new_item_id))
{
  return this;
}

string versionedItemId;
Aras.Server.Core.InnovatorDatabase conn = CCO.Variables.InnDatabase;
string itemTypeDbName = CCO.DB.GenerateTableName("Change Controlled Item");

Aras.Server.Core.Utilities.IsIdValid(new_item_id, true);

string sqlQuery = "SELECT TOP(1) last_version.ID FROM " + itemTypeDbName + " cur_version INNER JOIN " + itemTypeDbName + " last_version ON cur_version.id = '" + new_item_id + "' and cur_version.config_id = last_version.config_id and last_version.is_current = '1'";

Aras.Server.Core.InnovatorDataSet result = conn.ExecuteSelect(sqlQuery);

if (result.RowsNumber <= 0)
{
	const string errorString = "Item last version is not found.";
	const string errorDetail = "config_id = '{0}'";
	Item errorItem = this.getInnovator().newError(errorString);
	errorItem.setErrorDetail(string.Format(errorDetail,new_item_id));
	return errorItem;
}

versionedItemId = result["id"].ToString();

if(versionedItemId == new_item_id)
{
  return this;
}

Item callframe = this.newItem("SQL","SQL PROCESS");
callframe.setProperty("name","affectedItemFloatNewItem");
callframe.setProperty("PROCESS","CALL");
callframe.setProperty("ARG1", this.getID());
callframe.setProperty("ARG2", versionedItemId);
callframe = callframe.apply();
  
return callframe;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_ChangeItemFloatNewItem' and [Method].is_current='1'">
<config_id>B74AECD2D2414D38B8D967B897E59CB7</config_id>
<name>PE_ChangeItemFloatNewItem</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
