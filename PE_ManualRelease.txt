// PE_ManualRelease
// Called from a server action. Releases an item outside of change control

Innovator inn = this.getInnovator();
string thisType = this.getType();
string thisName = this.getProperty("keyed_name", "");
string state = this.getProperty("state", "");
bool isInPreliminaryState = String.Equals(state, "Preliminary", StringComparison.Ordinal);
bool isInManualChangeState = String.Equals(state, "Manual Change", StringComparison.Ordinal);

// Item must be in Preliminary or Manual Change state
if (!isInPreliminaryState && !isInManualChangeState)
{
	return inn.newError(CCO.ErrorLookup.Lookup("PE_ManualRelease_MustBeInPreliminaryState", thisType));
}

// Make sure the current user is in the Owner identity
string ownedById = this.getProperty("owned_by_id");
if (!CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, ownedById))
{
	return inn.newError(CCO.ErrorLookup.Lookup("PE_ManualRelease_YouMustBeAMember", this.getPropertyAttribute("owned_by_id","keyed_name","Owner")));
}

Item checkResult = this.apply("PE_CheckChangeItemUsage");
if(checkResult.isError())
{
  	return checkResult;
}

// Promote the item to Released
Item resItem = this.promote("Released", "Manual Release");
if(resItem.isError())
{
  return resItem;
}

Item promoteditem = this.newItem(thisType, "get");
promoteditem.setID(this.getID());
promoteditem = promoteditem.apply();

return resItem;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_ManualRelease' and [Method].is_current='1'">
<config_id>B5119D8F8FDB4F80A2FBD362E10FD994</config_id>
<name>PE_ManualRelease</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
