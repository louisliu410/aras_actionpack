/*version:2*/
var dataType = getRelationshipProperty(relationshipID, 'data_type');
var res = false;

if ((dataType == 'string' || dataType == 'ml_string' || dataType == 'mv_list' || dataType == 'md5' ||
	dataType == 'filter list' || dataType == 'color list' || dataType == 'list') && (propertyName == 'stored_length')) {
	res = true;
}

if (((dataType == 'float') || (dataType == 'decimal')) && ((propertyName == 'prec') || (propertyName == 'scale'))) {
	res = true;
}

if (((dataType == 'list') || (dataType == 'sequence') || (dataType == 'item') ||
	(dataType == 'filter list') || (dataType == 'color list') || (dataType == 'mv_list')) && (propertyName == 'data_source')) {
	res = true;
}

function runDialog() {
	params.content = 'foreignPropSelectTree.html';
	params.title = aras.getResource('', 'foreignpropselecttree.foreign_prop_selec_tree');
	aras.getMostTopWindowWithAras(window).ArasModules.Dialog.show('iframe', params).promise.then(function(res) {
		if (res) {
			setRelationshipProperty(relationshipID, 'data_source', res['data_source']);
			var relNd = item.selectSingleNode('Relationships/Item[@type="' + relationshipTypeName + '" and @id="' + relationshipID + '"]');
			aras.setItemProperty(relNd,'foreign_property', res['foreign_property']);
		}
	});
}

if (dataType == 'foreign' && propertyName == 'data_source') {
	if (!item.getAttribute('propsLoaded')) {
		aras.getItemRelationships('ItemType', item.getAttribute('id'), 'Property');
		item.setAttribute('propsLoaded', '1');
	}

	var params = {
		aras: aras,
		item: item,
		dialogHeight: 600,
		dialogWidth: 250,
		resizable: true
	};

	setTimeout(runDialog,10);
}
return res;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='On Property Edit Start Cell' and [Method].is_current='1'">
<config_id>A18EAEA1BBDF4878A10EA94F924317DC</config_id>
<name>On Property Edit Start Cell</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
