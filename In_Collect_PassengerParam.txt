/*
    本方法由物件本體呼叫使用。    
    會尋找物件中innp_開頭的tag，將其值包裹為一個Inn_Param物件回傳。
    同時刪除物件中所包含的node，以避免這些資料影響到後續作業。
*/
Innovator inn=this.getInnovator();
Item itmR=null;
System.Xml.XmlNodeList passengerNodes=this.node.ChildNodes;

foreach(XmlNode node in passengerNodes){
    Item po=inn.newItem("Inn_Param");
    po.setProperty("Inn_ParamLabel",node.Name);
    po.setProperty("Inn_ParamValue",node.InnerText);
    if(itmR==null){itmR=po;}else{
        itmR.appendItem(po);    
    }
    
}



return itmR;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Collect_PassengerParam' and [Method].is_current='1'">
<config_id>EC296E81342E4A38A24D8A2D1AE91628</config_id>
<name>In_Collect_PassengerParam</name>
<comments>抓取需要被傳回前端的資料，並抹除該資料在本體中的存在。內部使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
