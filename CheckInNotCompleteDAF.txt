'Created Date: 2008/12/23, Creator: Scott

'Purpose：審核中的文件無法加入另外一張 DAF 表單

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim result As Item

Dim dd As Item 
Dim daf As Item 
Dim doc As Item 
Dim r_doc As Item

doc = inn.newItem("Document", "get")
doc.setAttribute("select", "id, item_number")
doc.setID(Me.getProperty("related_id"))
r_doc = doc.apply()

If r_doc.isError() Then
	err = inn.newError("Can not find this Document.")
	Return err
End If

dd = inn.newItem("DAF Document", "get")
dd.setAttribute("select", "id, source_id(id, daf_number, name, state)")

doc = inn.newItem("Document", "get")
doc.setAttribute("select", "id, item_number")
doc.setProperty("item_number", r_doc.getProperty("item_number"))

daf = inn.newItem("DAF", "get")
daf.setProperty("state", "'Complete', 'Cancel'")
daf.setPropertyCondition("state", "not in")

dd.setPropertyItem("source_id", daf)
dd.setRelatedItem(doc)

result = dd.apply()

If (result.isError()) And (result.getItemCount() = -1) Then
	err = inn.newError(result.getErrorString() & " " & result.getErrorDetail())
	Return err
ElseIf result.getItemCount() = 0 Then
	Return Me
Else
	err = inn.newError("審核中的文件無法加入另外一張 DAF 表單")
	Return err
End If
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CheckInNotCompleteDAF' and [Method].is_current='1'">
<config_id>CAAEF6E0E49E47BA8A10462F0381BDE0</config_id>
<name>CheckInNotCompleteDAF</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
