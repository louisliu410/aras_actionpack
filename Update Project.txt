'inDom format:
'  <Item type='Method' action='Update Project'>
'    <body project_id='PROJECT_ID_VAL'/>
'  </Item>
' OR
'  <Item type='Project' id='PROJECT_ID_VAL'>
'    ....
'  </Item>

Dim i As Innovator = Me.getInnovator()
Dim projId As String
Dim tmpNd As XmlElement
If Me.node.getAttribute("type")="Method" Then
  tmpNd = CType(Me.node.selectSingleNode("body"), XmlElement)
  If IsNothing(tmpNd) Then Return i.newError("body is not specified")
  projId = tmpNd.getAttribute("project_id")
Else
  projId = Me.node.getAttribute("id")
End If
Dim methNm As String = "PM_fireCustomProjectHook"
Return i.applyMethod(methNm, String.Format("<body project_id='{0}' prop_name='update_method'/>", projId))
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Update Project' and [Method].is_current='1'">
<config_id>95B06B6860974698B07C42C9A0446706</config_id>
<name>Update Project</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
