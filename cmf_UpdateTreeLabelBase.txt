if (arguments && arguments.length > 0) {
	if (arguments[0].srcElement) {
		if (parent.updateTreeNodeLabel) {
			parent.updateTreeNodeLabel(arguments[0].srcElement.value, document.item.getAttribute('id'));
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_UpdateTreeLabelBase' and [Method].is_current='1'">
<config_id>9F3C985993EE4DC799504891DEF52964</config_id>
<name>cmf_UpdateTreeLabelBase</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
