Dim id As String= Me.getID()
Dim new_filename As String  = Me.getProperty("filename")
Dim innovator as Innovator = Me.getInnovator()
Dim query As item = innovator.newItem("File", "copyAsNew")
query.setID(id)
query.setAttribute("do_lock", "0")
query.setAttribute("useInputProperties", "1")
query.setProperty("filename", new_filename)
Dim result as Item = query.apply()

If result.isError() Then
	Throw New Aras.Server.Core.InnovatorServerException(result.dom)
End If

Dim new_id as String = result.getID()
query = innovator.newItem("File", "replaceFile")
query.setAttribute("id", id)
query.setAttribute("newId", new_id)
query = query.apply()

If query.isError() Then
	Throw New Aras.Server.Core.InnovatorServerException(query.dom)
End If

Return result

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='RenameFile' and [Method].is_current='1'">
<config_id>8448FBC559394A91A517F316F0347FAF</config_id>
<name>RenameFile</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
