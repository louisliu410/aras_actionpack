string ruleId = "87205524639B49A5BEBDFF573CC96639"; //ConversionRule 'Aras 3D CAD to PDF Conversion'
Innovator inn = this.getInnovator();
Item query = inn.newItem("ConversionRule", "get");
query.setID(ruleId);
query = query.apply();
if (query.isError())
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_ConversionRuleNotFound"));
}

string fileId = this.getProperty("native_file");
if (String.IsNullOrEmpty(fileId))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_CadNotContainFile"));
}
query = inn.newItem();
query.loadAML(@"
<AML>
	<Item type='File' action='get' id='" + fileId + @"' select='file_type' />
</AML>");
query = query.apply();
if (query.isError())
{
	throw new Exception(query.getErrorString());
}
string fileType = query.getProperty("file_type");
query = inn.newItem();
query.loadAML(@"
<AML>
	<Item type='ConversionTask' action='CreateConversionTask'>
		<file_id>" + fileId + @"</file_id>
		<file_type>" + fileType + @"</file_type>
		<rule_id>" + ruleId + @"</rule_id>
		<user_data><![CDATA[<Data><ConvertToTypes><type>PDF</type></ConvertToTypes></Data>]]></user_data>
	</Item>
</AML>");
query = query.apply();
return query;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_ConvertCADToPDF' and [Method].is_current='1'">
<config_id>B7D3428C413741BF8B7D215C645963DB</config_id>
<name>VC_ConvertCADToPDF</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
