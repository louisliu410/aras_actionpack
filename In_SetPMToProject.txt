/*
目的:立案單儲存時,將PM謄寫到專案的PM與Team
做法:
位置:
*/

//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";

try{
    
    Item ControlledItem = _InnH.GetInnoControlledItem(this);
    
    if(ControlledItem.isError()){
        return inn.newError(ControlledItem.getErrorString());
    }
    
    aml = "<AML>";
    aml += "<Item type='Project' action='get'>";
    aml += "<in_number>"+ControlledItem.getProperty("item_number","")+"</in_number>";
    aml += "</Item></AML>";
    Item itmProject = inn.applyAML(aml);
    
    aml = "<AML>";
    aml += "<Item type='Team' action='get'>";
    aml += "<id>"+itmProject.getProperty("team_id","")+"</id>";
    aml += "</Item></AML>";
    Item itmTeam = inn.applyAML(aml);
    
    aml = "<AML>";
    aml += "<Item type='Identity' action='get'>";
    aml += "<name>Team Manager</name>";
    aml += "</Item></AML>";
    Item itmIdentity = inn.applyAML(aml);
    
    aml = "<AML>";
    aml += "<Item type='Team Identity' action='delete' where=\"[team_role]='" + itmIdentity.getID() + "'\">";
    aml += "<source_id>"+itmTeam.getID()+"</source_id>";
    aml += "</Item></AML>";
    Item itmTeamIdentityDel = inn.applyAML(aml);
    
    sql = "Update Project set ";
    sql += "owned_by_id = '" + ControlledItem.getProperty("in_pm","") + "'";
    sql += ",in_dept = '" + ControlledItem.getProperty("in_dept","") + "'";
    sql += " where id = '" + itmProject.getID() + "'";
    inn.applySQL(sql);
    
    sql = "Update Team set ";
    sql += "owned_by_id = '" + ControlledItem.getProperty("in_pm","") + "'";
    sql += " where id = '" + itmProject.getProperty("team_id","") + "'";
    inn.applySQL(sql);
    
    aml = "<AML>";
    aml += "<Item type='Team Identity' action='add' where=\"[team_role]='" + itmIdentity.getID() + "'\">";
    aml += "<source_id>"+itmTeam.getID()+"</source_id>";
    aml += "<related_id>"+ControlledItem.getProperty("in_pm")+"</related_id>";
    aml += "<team_role>"+itmIdentity.getID()+"</team_role>";
    aml += "</Item></AML>";
    Item itmTeamIdentityAdd = inn.applyAML(aml);
}
catch(Exception ex)
{
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
    
    string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
    
    strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
    strMethodName = strMethodName.Replace("EventArgs)","");
    
    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
    string strError = ex.Message + "\n";
    
    if(aml!="")
        strError += "無法執行AML:" + aml  + "\n";
        
    if(sql!="")
        strError += "無法執行SQL:" + sql  + "\n";
        
    string strErrorDetail="";
    
    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
    
    throw new Exception(_InnH.Translate(strError));
}
if(PermissionWasSet)
Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetPMToProject' and [Method].is_current='1'">
<config_id>8E866B147E4F43A487C2C90A96BE520B</config_id>
<name>In_SetPMToProject</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
