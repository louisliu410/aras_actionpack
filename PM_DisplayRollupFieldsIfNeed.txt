var actItm = document.ActivityItem;
if (!actItm) actItm = document.item;
var doDisplayRollupFields = actItm.selectSingleNode("Relationships/Item[@type='Activity2 Assignment' and string(@action)!='delete']");
if (!doDisplayRollupFields)
{
  var isFromTree = actItm.selectSingleNode("ancestor::Item[@type='Project' or @type='Project Template']");
  if (!isFromTree)
  {
    var aml2send = "<Item type='Activity2 Assignment' action='get' select='id'><source_id>" + actItm.getAttribute("id") + "</source_id></Item>";
    var r = top.aras.applyItem(aml2send);
    if (r && r.indexOf("Item")>-1)
    {
      doDisplayRollupFields = true;
    }
  }
}

if (doDisplayRollupFields)
{
  var nms2rollupNms = {date_start_act:"rollup_date_start_act",  work_est:"rollup_work_est", percent_compl:"rollup_percent_compl"};
  for (var nm in nms2rollupNms)
  {
    var rollupNm = nms2rollupNms[nm];
    var spn = getFieldByName(nm);//this function is defined on a Form.
    var rollupSpn = getFieldByName(rollupNm);
    if (spn) spn.style.display = "none";
    if (rollupSpn) rollupSpn.style.display = "inline";
  }
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_DisplayRollupFieldsIfNeed' and [Method].is_current='1'">
<config_id>E40310CA12994622B05382F8C54EB0B5</config_id>
<name>PM_DisplayRollupFieldsIfNeed</name>
<comments>Displays some Rollup fields and hide corresponding non-Rollup fields if need</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
