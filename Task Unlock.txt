var isMyTask = top.aras.getItemProperty(inDom, "my_assignment");
if (isMyTask == "1")
{
    top.main.work.onUnlockCommand(true, [inDom.getAttribute("id")]);
}
else
{
    var msg = top.aras.getResource("", "inbasket.lock_unlock_error", top.aras.getItemProperty(inDom, "keyed_name"));
    top.aras.AlertError(msg);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Task Unlock' and [Method].is_current='1'">
<config_id>90B56007E69C4AB4B686F4997710ECFB</config_id>
<name>Task Unlock</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
