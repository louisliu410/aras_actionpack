var innovator = aras.newIOMInnovator();

var elementItem = innovator.newItem('cmf_ElementType', 'get');
elementItem.setAttribute('select', 'source_id');
elementItem.setID(this.getProperty('source_id'));
elementItem = elementItem.apply();

var item = innovator.newItem('cmf_ContentType', '');
item.setID(elementItem.getProperty('source_id'));
item.setAttribute('method_action', 'ConvertPropertyToElement');
item.setProperty('property_id', this.getID());
item.setProperty('element_id', this.getProperty('source_id'));

var response = item.apply('cmf_SchemaDefinitionHandler');
if (response.isError()) {
	aras.AlertError(response);
	return;
}

var winProperty = aras.uiFindWindowEx(this.getID());
if (winProperty) {
	winProperty.close();
}

var winElement = aras.uiFindWindowEx(this.getProperty('source_id'));
if (winElement) {
	winElement.onRefresh();
}

var winDocument = aras.uiFindWindowEx(elementItem.getProperty('source_id'));
if (winDocument) {
	winDocument.onRefresh();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ConvertPropertyToElement' and [Method].is_current='1'">
<config_id>3EBFDAE407614B3A9BA68D52F5CA4EB0</config_id>
<name>cmf_ConvertPropertyToElement</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
