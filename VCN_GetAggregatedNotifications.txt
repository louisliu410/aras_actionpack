string itemTypeName = this.getProperty("message_type");
string identityList = this.getProperty("identity_list");
string userId = this.getProperty("user_id");

Item resultItem = this.newItem(itemTypeName, "get");
Item identityFilter = this.newItem("Identity", "get");
identityFilter.setPropertyAttribute("id", "condition", "in");
identityFilter.setProperty("id", identityList);
resultItem.setPropertyItem("target", identityFilter);

var res = resultItem.apply();
if(res.isError())
{
    return this.getInnovator().newResult("");
}

int messageCount = res.getItemCount();

string title;
if(messageCount == 1)
{
    title = CCO.ErrorLookup.Lookup("SSVC_GetNotificationMessage");
}
else
{
    title = CCO.ErrorLookup.Lookup("SSVC_GetNotificationMessages", messageCount.ToString());
}

// We return not existing aggregated SSVC Notification Message
// with unique userId because SSVC Notification Message can be only one per user
Item notificationMessage = this.newItem(itemTypeName);
notificationMessage.setProperty("title", title);
notificationMessage.setProperty("count", messageCount.ToString());
notificationMessage.setAttribute("id", userId);
notificationMessage.setProperty("type", "Standard");
notificationMessage.setProperty("acknowledge", "Once");

Item result = this.newItem();
result.loadAML("<Result>" + notificationMessage.dom.InnerXml + "</Result>");
return result;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VCN_GetAggregatedNotifications' and [Method].is_current='1'">
<config_id>4440E5FD68F24608ABB6026A31362CFE</config_id>
<name>VCN_GetAggregatedNotifications</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
