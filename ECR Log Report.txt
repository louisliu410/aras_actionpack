var innovator = new Innovator();
	var queryItem = new Item("ECR","do_l10n");
	queryItem.setAttribute("initial_action","get");
	var ecrs = queryItem.apply();
	
	for (var i=0; i<ecrs.getItemCount(); i++){
	 var thisEcr = ecrs.getItemByIndex(i);
	 var ecrId = thisEcr.getID();
	 queryItem = new Item("ECN ECR","get");
	 queryItem.setProperty("related_id",ecrId);
	 queryItem.setAttribute("select","source_id(item_number)");
	 var ecn = queryItem.apply();
	 if (ecn.getItemCount() > 0) {
	  var thisEcn = ecn.getItemByIndex(0).getPropertyItem("source_id");
	  var ecnNum = thisEcn.getProperty("item_number");
	  thisEcr.setProperty("ECN",ecnNum);
	 }
	}
	var report = innovator.getItemByKeyedName("Report","ECR Log Report");
	var style = report.getProperty("xsl_stylesheet");
	var html = ecrs.applyStylesheet(style,"text");
	return html;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ECR Log Report' and [Method].is_current='1'">
<config_id>BA6563DDD6434E87B1A907F8B88507E1</config_id>
<name>ECR Log Report</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
