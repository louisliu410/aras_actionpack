/*
目的:取得個人專案任務的內容
做法:
1.列出專案任務
2.計算已逾期(結束日為紅色)的數量
3.計算即將逾期(結束日為黃色)的數量

*/


//System.Diagnostics.Debugger.Break();
string strMethodName = "In_Get_UserTasksAct2";

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;
string str_r = "";
Item rst=inn.newItem();

string strOutput1 = "";
string strOutput2 = "";
string strInput1 = this.getProperty("in_input1","");	//取得Identity id
string strInput2 = this.getProperty("in_input2","");

string strUserId = inn.getUserID();
string strIdentityId = inn.getUserAliases();




if(strInput1!="")
{
	Item itmUser = _InnH.GetUserByAliasIdentityId(strInput1);
	strUserId = itmUser.getID();
	strIdentityId = strInput1;
}

itmR = inn.newItem("tmp");
try
{
    DateTime dtNow = System.DateTime.Now;
    DateTime firstOfThisMonth = new DateTime(dtNow.Year, dtNow.Month, 1);
    DateTime firstOfNextMonth = firstOfThisMonth.AddMonths(1);
    DateTime lastOfThisMonth = firstOfNextMonth.AddSeconds(-1);

	//收到的任務
	aml = "<AML>";
	aml += "<Item type='InBasket Task' action='get'>";
	aml += "<itemtype>9DC36DF7F0234A3E8CFFE2A20AB78CCE</itemtype>";
	aml += "<status condition='in'>'Active','Pending'</status>";
	aml += "<my_assignment>1</my_assignment>";
	aml += "</Item>";
	aml += "</AML>";	
	Item itmMyAct2Tasks = inn.applyAML(aml);


	//收到的專案任務
	int intGreen=0;
	int intRed=0;
	int intYellow=0;
	int intPending=0;
	int intActive = 0;
	string strStartCss = "";
	string strDueCss = "";
	if(!itmMyAct2Tasks.isError())
	{
		for(int i=0;i<itmMyAct2Tasks.getItemCount();i++)
		{

			Item itmMyAct2Task = itmMyAct2Tasks.getItemByIndex(i);
			itmMyAct2Task.setProperty("due_date_short",itmMyAct2Task.getProperty("due_date").Split('T')[0]);
			itmMyAct2Task.setProperty("start_date_short",itmMyAct2Task.getProperty("start_date").Split('T')[0]);

			itmMyAct2Task.setProperty("subject",itmMyAct2Task.getPropertyAttribute("container","keyed_name",""));		
			itmMyAct2Task.setProperty("container_type",itmMyAct2Task.getPropertyAttribute("container_type_id","keyed_name",""));
			string strStartColor = "";
			string strDueColor = "";
			if(itmMyAct2Task.getProperty("status","")=="Active")
			{
				intActive++;
				itmMyAct2Task.setType("act2_active");
				
				//處理css
				string strCss = itmMyAct2Task.getProperty("css","");
				if(strCss!="")
				{
					string strStartCssString = ".start_date{background-color:";
					string strDueCssString = ".due_date{background-color:";
					int intStartCssStart = strCss.IndexOf(strStartCssString);
					int intDueCssStart = strCss.IndexOf(strDueCssString);
					strStartColor = strCss.Substring(intStartCssStart + strStartCssString.Length,7);
					strDueColor = strCss.Substring(intDueCssStart + strDueCssString.Length,7);
					
					if(itmMyAct2Task.getProperty("status","")=="Active")
					{
						
						switch(strStartColor)
						{
							case "#FF0000":
								strStartCss = "red";
								break;
							case "##00FF00":
								strStartCss = "green";
								break;
							case "#FFFF00":
								strStartCss = "yellow";
								break;
						}
		
						switch(strDueColor)
						{
							case "#FF0000":
								strDueCss = "red";
								intRed++;
								break;
							case "#00FF00":
								strDueCss = "green";
								intGreen++;
								break;
							case "#FFFF00":
								strDueCss = "yellow";
								intYellow++;
								break;
						}
					}
	 
				}
			}
			else
			{
				intPending ++;
				itmMyAct2Task.setType("act2_pending");
				strDueCss = "gray";
				strStartCss = "gray";
			}
				
			
			
			
		
			itmMyAct2Task.setProperty("start.colorclass",strStartCss);
			itmMyAct2Task.setProperty("due.colorclass",strDueCss);
			
			

			itmMyAct2Task.setProperty("start.color",strStartColor);
			itmMyAct2Task.setProperty("due.color",strDueColor);
			

			//itmMyAct2Task.setType("act2_task");			
			rst.addRelationship(itmMyAct2Task);
		}
	}


	rst.setProperty("act2task_count",itmMyAct2Tasks.getItemCount().ToString());
	rst.setProperty("act2task_red_count",intRed.ToString());
	rst.setProperty("act2task_yellow_count",intYellow.ToString());
	rst.setProperty("act2task_green_count",intGreen.ToString());
	rst.setProperty("act2task_pending_count",intPending.ToString());
	rst.setProperty("act2task_active_count",intActive.ToString());
	
	rst.setType("method");
	rst = rst.apply("In_AppendExtraProperties");



}
catch(Exception ex)
{   
    //if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


    string strError = ex.Message + "\n";

    if(aml!="")
        strError += "無法執行AML:" + aml  + "\n";

    if(sql!="")
        strError += "無法執行SQL:" + sql  + "\n";

    string strErrorDetail="";
    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    _InnH.AddLog(strErrorDetail,"Error");
    strError = strError.Replace("\n","</br>");
    throw new Exception(_InnH.Translate(strError));
}


return rst;
//return inn.newResult(rst.dom.InnerXml);
		
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_UserTasksAct2' and [Method].is_current='1'">
<config_id>2B6C473C54D34ACCA2755CE2B9783844</config_id>
<name>In_Get_UserTasksAct2</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
