var imageSrc = "../images/Message.svg";	
var query = top.aras.newIOMItem("tmp", "tmp");
	query.loadAML(document.item.xml);
	query.setAttribute("action", "PE_EditAML");
	query = query.apply();
	if (query.isError()) {
		top.aras.AlertError(query.getErrorDetail(), query.getErrorString(), query.getErrorSource());
		return;
	}
	
	var nds = document.item.selectNodes("Relationships/Item[@type='Part AML' and (string(@action)!='' or related_id/Item[string(@action)!=''])]")
	for (var i = 0; i < nds.length; i++) {
		nds[i].parentNode.removeChild(nds[i]);
	}
	var fr = document.getElementById("relationshipsGrid").contentWindow;
	var ids = fr.gridApplet.getSelectedItemIds(";").split(";");
	var selectedID = (ids.length == 1 && ids[0] ? ids[0] : "");
	fr.processCommand("search");
	if (selectedID) {
		fr.gridApplet.setSelectedRow(selectedID, false, true);
	}

	if (!top.window.PopupNotification && statusbar) {
		var popupNotification = popup_control.GetNotificationControl();
		popupNotification.AddOrUpdate(popupNotification.Count, top.aras.getResource("plm", "launch_aml_editor.saved_successfully"), imageSrc, 1);
		popup_control.ShowPopup(timeout);
	}
parent.args.dialog.close();
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_AmlEditorSaveClick' and [Method].is_current='1'">
<config_id>A92E33B2D7124F3DB49D25C65DAEE58F</config_id>
<name>PE_AmlEditorSaveClick</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
