var inn = this.getInnovator();
var cntx = inn.getI18NSessionContext();

require([
	'ES/Scripts/Classes/Utils'
], function(Utils) {
	var utils = new Utils({
		arasObj: aras
	});

	var message = utils.getResourceValueByKey('message.reindex_all_items');
	showConfirmationDialog(message, function() {
		//Reset all configurations
		var localOffset = cntx.getCorporateToLocalOffset();
		var localTime = new Date();
		var corporateTime = new Date(localTime.getTime() + localOffset * 60 * 1000);
		var corporateTimeInNeutralFormat = utils.convertDateToNeutralFormat(corporateTime);

		var indexedConfigurationItms = inn.newItem('ES_IndexedConfiguration', 'get');
		indexedConfigurationItms.setAttribute('select', 'id');
		indexedConfigurationItms = indexedConfigurationItms.apply();
		if (indexedConfigurationItms.isError()) {
			return aras.AlertError(indexedConfigurationItms.getErrorString());
		}

		var count = indexedConfigurationItms.getItemCount();
		for (var i = 0; i < count; i++) {
			var indexedConfigurationItm = indexedConfigurationItms.getItemByIndex(i);
			var id = indexedConfigurationItm.getID();

			indexedConfigurationItm = inn.newItem('ES_IndexedConfiguration', 'edit');
			indexedConfigurationItm.setID(id);
			indexedConfigurationItm.setAttribute('doGetItem', '0');
			indexedConfigurationItm.setProperty('reset_on', corporateTimeInNeutralFormat);
			indexedConfigurationItm.apply();
		}
	});
});

//------------------------------------------------------------------------------

function showConfirmationDialog(message, callback) {
	var params = {
		buttons: {
			btnYes: aras.getResource('', 'common.yes'),
			btnCancel: aras.getResource('', 'common.cancel')
		},
		dialogHeight: 150,
		dialogWidth: 250,
		center: true,
		resizable: true,
		defaultButton: 'btnCancel',
		message: message,
		aras: aras,
		content: 'groupChgsDialog.html'
	};

	var topWnd = aras.getMostTopWindowWithAras();
	(topWnd.main || topWnd).ArasModules.Dialog.show('iframe', params).promise.then(function(resButton) {
		if (resButton === 'btnYes') {
			callback();
		}
	});
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_ReindexAll' and [Method].is_current='1'">
<config_id>AF52584EAD99455B86AF49BCAD99473E</config_id>
<name>ES_ReindexAll</name>
<comments>Reindex all items</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
