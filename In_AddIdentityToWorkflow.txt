/*
目的:動態增刪派工單的當下簽核人員
做法:
1.取得目前 in_is_current 的派工單細項的項次,抓到相同項次的所有人
2.判斷派工細項中的每個人若不存在於目前的簽審人員則加上去
3.將不包含於 in_iscurrent的 activity assignment 全數刪除
*/

string strMethodName = "In_AddIdentityToWorkflow";
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string strID = "";
string sql = "";

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

if(this.getProperty("state","") != "In Process")
{
    return this;
}

aml  = "<AML>";
aml += "<Item type='In_WorkOrder_Detail' action='get'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "<in_is_current>1</in_is_current>";
aml += "</Item></AML>";
Item itmWorkOrderDetails = inn.applyAML(aml);

if(itmWorkOrderDetails.getItemCount()==0)
{
    throw new Exception("查無執行中的任務,請指定執行中任務");
}
string strSortOrder = itmWorkOrderDetails.getItemByIndex(0).getProperty("sort_order","0");
aml  = "<AML>";
aml += "<Item type='In_WorkOrder_Detail' action='get'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "<sort_order>" + strSortOrder + "</sort_order>";
aml += "</Item></AML>";
itmWorkOrderDetails = inn.applyAML(aml);


//取得目前執行中的Activity
aml  = "<AML>";
aml += "<Item type='Workflow Process Activity' action='get'>";
aml += "<related_id>";
aml += "<Item type='Activity' action='get'>";
aml += "<state>Active</state>";
aml += "</Item>";
aml += "</related_id>";
aml += "<source_id>"+this.getProperty("in_wfp_id")+"</source_id>";
aml += "</Item></AML>";
Item itmWorkflowProcessActivity = inn.applyAML(aml);
string strActivityId =  itmWorkflowProcessActivity.getProperty("related_id");
string strAssignmentIds = "";
string strWorkOrderDetailIds = "";
string strDesc = "";
//2.判斷派工細項中的每個人若不存在於目前的簽審人員則加上去
for(int i=0;i<itmWorkOrderDetails.getItemCount();i++)
{
	Item itmWorkOrderDetail = itmWorkOrderDetails.getItemByIndex(i);
	sql = "select * from [Activity_Assignment] where [Activity_Assignment].related_id='" + itmWorkOrderDetail.getProperty("in_new_assignment") + "' and [Activity_Assignment].source_id='" + strActivityId + "'";
	Item itmAss = inn.applySQL(sql);
	if(itmAss.getResult()=="")
	{
		//代表這個人原本不存在於簽核人員中,所以要新增
		aml  = "<AML>";
		aml += "<Item type='Activity Assignment' action='add'>";
		aml += "<is_required>1</is_required>";
		aml += "<source_id>"+itmWorkflowProcessActivity.getProperty("related_id")+"</source_id>";
		aml += "<related_id>"+itmWorkOrderDetail.getProperty("in_new_assignment")+"</related_id>";
		aml += "<in_is_cloned>1</in_is_cloned>";
		aml += "</Item></AML>";
		Item itmAddActivityAssignment = inn.applyAML(aml);
	}
	strAssignmentIds += itmWorkOrderDetail.getProperty("in_new_assignment") + ",";
	strWorkOrderDetailIds += itmWorkOrderDetail.getProperty("id") + ",";

	strDesc +=itmWorkOrderDetail.getPropertyAttribute("in_new_assignment","keyed_name") + ":\n";
	strDesc +=itmWorkOrderDetail.getProperty("in_name","") + ":\n";
	strDesc += itmWorkOrderDetail.getProperty("in_description","") + "\n" ;
	strDesc += "---------------------------------\n";		

}

sql = "Update [In_WorkOrder] set in_current_details=N'" + strDesc + "' where id='" + this.getID() + "'";
inn.applySQL(sql);

strAssignmentIds = strAssignmentIds.Trim(',');
strAssignmentIds = "'" + strAssignmentIds.Replace(",","','") + "'";

strWorkOrderDetailIds = strWorkOrderDetailIds.Trim(',');
strWorkOrderDetailIds = "'" + strWorkOrderDetailIds.Replace(",","','") + "'";

//3.將不包含於 in_iscurrent的 activity assignment 全數刪除
sql = "select * from [Activity_Assignment] where [Activity_Assignment].source_id='" + strActivityId + "' and [Activity_Assignment].related_id not in (" + strAssignmentIds + ")";
Item itmAsss = inn.applySQL(sql);
string strAssIds = _InnH.GetCSV(itmAsss,"id");
if(strAssIds!="")
{
	strAssIds = "'" + strAssIds.Replace(",","','") + "'";
	aml  = "<AML>";
	aml += "<Item type='Activity Assignment' action='delete' where=\"id in (" +  strAssIds + ")\">";
	aml += "</Item></AML>";
	Item itmDelActivityAssignment = inn.applyAML(aml);


	//若刪到原始非cloned的,就要指定第一筆(無論有無新增)設定它的
	sql = "select id from [Activity_Assignment] where [Activity_Assignment].source_id='" + strActivityId + "' and [Activity_Assignment].in_is_cloned=0";
	itmAsss = inn.applySQL(sql);
	if(itmAsss.getResult()=="")
	{
		//代表原始已被刪掉
		sql = "select top 1 id from [Activity_Assignment] where [Activity_Assignment].source_id='" + strActivityId + "' and [Activity_Assignment].in_is_cloned=1";
		itmAsss = inn.applySQL(sql);
		sql = "update [Activity_Assignment] set  in_assignment_type='relationship',in_is_cloned='0',in_param1='In_WorkOrder_Assignments' where id='" + itmAsss.getID() + "'";
		itmAsss = inn.applySQL(sql);
	}	
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AddIdentityToWorkflow' and [Method].is_current='1'">
<config_id>CB972DB9FBF84734B1BF800677E781A0</config_id>
<name>In_AddIdentityToWorkflow</name>
<comments>態增刪派工單的當下簽核人員</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
