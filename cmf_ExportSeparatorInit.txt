var items = aras.evalMethod('cmf_GetViewsForCommandBars', '', inArgs);
return {
	'cui_visible': items.length > 1
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ExportSeparatorInit' and [Method].is_current='1'">
<config_id>937DE22CA11940FCBB4A60D80D7EE49E</config_id>
<name>cmf_ExportSeparatorInit</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
