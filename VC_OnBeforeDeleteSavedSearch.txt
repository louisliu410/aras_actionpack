string savedSearchId = this.getID();
Aras.Server.Core.InnovatorDatabase conn = CCO.Variables.InnDatabase;
Aras.Server.Core.InnovatorDataSet result;

string sqlCommand = String.Format("SELECT id FROM {0} WHERE related_id = '{1}'", conn.GetTableName("ForumSearch"), savedSearchId);
result = conn.ExecuteSelectCommand(sqlCommand);
if (!result.Eof)
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_FailedToDeleteSavedSearch"));
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_OnBeforeDeleteSavedSearch' and [Method].is_current='1'">
<config_id>19C0B9D2BB0046638F3C1DFA5F313582</config_id>
<name>VC_OnBeforeDeleteSavedSearch</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
