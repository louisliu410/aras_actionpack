if (inArgs.isReinit) {
	//Workflow Map, Form, Life Cycle Map, CMF Admin Panel Toolbar
	if (window.itemTypeName === 'cmf_ContentType') {
		return {'cui_disabled': true};
	}
	var node;
	if (window.itemTypeName === 'Form' || window.itemTypeName === 'Life Cycle Map') {
		node = window.item;
	} else {
		node = window.currWFNode;
	}

	if (node) {
		var isTemp = aras.isTempEx(node);
		return {'cui_disabled': !(isEditMode && !isTemp)};
	}
	//Normal Toolbar
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calc_tearoff_states');
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}
	return {'cui_disabled': !inArgs.eventState.isUndo};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_twt_undo' and [Method].is_current='1'">
<config_id>B183FCD2B653448BA67D321BF56377BD</config_id>
<name>cui_reinit_twt_undo</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
