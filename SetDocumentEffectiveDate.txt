'Created Date: 2008/12/05, Creator: Scott

'System.Diagnostics.Debugger.Break() 

Dim plmIdentity As Aras.Server.Security.Identity = Aras.Server.Security.Identity.GetByName("Aras PLM")
Dim PermissionWasSet As Boolean = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity)
'===========================================================================================

Dim inn As Innovator = Me.getInnovator()
Dim err As Item

'===================================================================
'取得目前時間，並轉換成 Local DateTime
Dim dtNow As DateTime = System.DateTime.UtcNow
Dim tmpPtrn As String = "yyyy-MM-ddTHH:mm:ss"
Dim tmpDt As String = dtNow.toString(tmpPtrn)

tmpDt += "+0000"
tmpPtrn += "zzz"

Dim sCntxt As I18NSessionContext = Me.newInnovator().getI18NSessionContext()
Dim today As DateTime = DateTime.Parse(sCntxt.ConvertToNeutral(tmpDt, "date", tmpPtrn))
'===================================================================

Dim doc As Item
Dim r_doc As Item
Dim result As Item
Dim effective_date As DateTime
Dim i As Integer

'讀取 Document 內容
doc = inn.newItem("Document", "get")
doc.setAttribute("select", "id, item_number, state, effective_date")
doc.setID(Me.getID())
r_doc = doc.apply()
	
If r_doc.isError() Then
	If (PermissionWasSet=True) Then
		Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
	End If

	err = inn.newError("Fail to get document for id =  " + Me.getID() + ". "  + result.getErrorDetail())
	Return err 
End If

'比對 Effective Date 不能小於今天，如果小於今天，Effective Date 就改成 今天
If (Not r_doc.getProperty("effective_date") Is Nothing) AndAlso (r_doc.getProperty("effective_date") <> "") Then
	effective_date =  DateTime.Parse(r_doc.getProperty("effective_date"))
Else
	effective_date = today
End If
	
If effective_date <= today Then
	r_doc.setProperty("effective_date", today.toString("yyyy-MM-ddTHH:mm:ss"))

	r_doc.setAttribute("version","0")
	r_doc.setAttribute("serverEvents","0")
	r_doc.setAttribute("doGetItem", "0")
	r_doc.setAction("edit")
	
	result = r_doc.apply()
	
	If result.isError() Then
		If (PermissionWasSet=True) Then
			Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
		End If

		err = inn.newError("Fail to update the effective date of document. " + result.getErrorDetail())
		Return err 
	End If	
End If

doc = Nothing
r_doc = Nothing
result = Nothing

'===========================================================================================
If (PermissionWasSet=True) Then
	Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity)
End If
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SetDocumentEffectiveDate' and [Method].is_current='1'">
<config_id>A5061ECD21B24E14AE92C2EFA5A7BEA6</config_id>
<name>SetDocumentEffectiveDate</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
