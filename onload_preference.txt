var maxCheck = 30;
function disableTabs() {
	if (!parent.document.frames.relationships || !parent.document.frames.relationships.currTabID) {
		if (--maxCheck === 0) { return; }
		setTimeout(disableTabs, 50);
		return;
	}

	if (!document.item) {
		return;
	}
	var identityId = aras.getItemProperty(document.item, 'identity_id');
	var identityNode = aras.getItemById('Identity', identityId);
	var identityName = aras.getItemProperty(identityNode, 'name');

	var allowedType = 2; // User
	if (identityName == 'World') {
		allowedType = 1; // Site
	}

	var appl = parent.relationships.relTabbar;

	var qry = aras.newQryItem('PreferenceTypes');
	qry.setLevels(1);
	var res = qry.execute();
	if (!res) {
		return;
	}

	var prefTabTypes = res.selectNodes('//Item[@type=\'PreferenceTabTypes\']');

	for (var i = 0; i < prefTabTypes.length; i++) {
		var prefTabType = prefTabTypes[i];

		var prefType = aras.getItemProperty(prefTabType, 'preference_tab_type');
		var prefITName = aras.getItemProperty(prefTabType, 'preference_tab');

		prefType = parseInt(prefType);
		if (isNaN(prefType) || prefType > 3 || prefType < 1) {
			prefType = 3;
		}

		if ((prefType & allowedType) === 0) {
			var xml = '' +
			'<Item type=\'RelationshipType\' action=\'get\' select=\'label\'>' +
				'<relationship_id>' +
					'<Item type=\'ItemType\' select=\'id\'>' +
						'<name>' + prefITName + '</name>' +
					'</Item>' +
				'</relationship_id>' +
			'</Item>';
			res = aras.applyItem(xml);
			if (typeof (res) != 'string' && res !== '') {
				continue;
			}
			var tmpDom = aras.createXMLDocument();
			tmpDom.loadXML(res);
			var labelNode = tmpDom.selectSingleNode('//label');
			if (!labelNode) {
				continue;
			}
			var label = labelNode.text;
			appl.removeTabByLabel(labelNode.text);
		}
	}

}

disableTabs();

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onload_preference' and [Method].is_current='1'">
<config_id>24423E1E496C4BD0B428820168F67293</config_id>
<name>onload_preference</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
