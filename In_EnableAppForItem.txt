/*
目的:讓Itemtype可以用於APP環境
做法:
1.補上 in_web_view, in_web_list, in_web_relationship, in_web_edit, in_web_add
2.補上 常用的 app action
3.補上 relationship type 的 in_web_list
*/
string strMethodName = "In_EnableAppForItem";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Aras.Server.Core.InnovatorDatabase conn = CCO.DB.InnDatabase;
string strError = "";
//string strUserId = inn.getUserID();
//string strIdentityId = inn.getUserAliases();

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

sql = "Update [Property] set in_web_view=sort_order where in_web_view is null and is_hidden=0 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_add=sort_order where in_web_add is null and is_hidden=0 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_edit=sort_order where in_web_edit is null and is_hidden=0 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_list=sort_order where in_web_list is null and is_hidden=0 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_relationship=sort_order where in_web_relationship is null and is_hidden=0 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_view=0 where in_web_view is null and is_hidden=1 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_add=0 where in_web_add is null and is_hidden=1 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_edit=0 where in_web_edit is null and is_hidden=1 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_list=0 where in_web_list is null and is_hidden=1 and source_id='" + this.getID() + "';";
sql += "Update [Property] set in_web_relationship=0 where in_web_relationship is null and is_hidden=1 and source_id='" + this.getID() + "';";
conn.ExecuteSQL(sql);

aml = "<AML>";
aml += "<Item type='RelationshipType' action='get'>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";
Item itmRels = inn.applyAML(aml);

for(int i=0;i<itmRels.getItemCount();i++)
{
	Item itmRel = itmRels.getItemByIndex(i);
	sql = "Update [Property] set in_web_view=sort_order where in_web_view is null and is_hidden=0 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_add=sort_order where in_web_add is null and is_hidden=0 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_edit=sort_order where in_web_edit is null and is_hidden=0 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_list=sort_order where in_web_list is null and is_hidden=0 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_relationship=sort_order where in_web_relationship is null and is_hidden=0 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_view=0 where in_web_view is null and is_hidden=1 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_add=0 where in_web_add is null and is_hidden=1 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_edit=0 where in_web_edit is null and is_hidden=1 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_list=0 where in_web_list is null and is_hidden=1 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	sql += "Update [Property] set in_web_relationship=0 where in_web_relationship is null and is_hidden=1 and source_id='" + itmRel.getProperty("relationship_id") + "';";
	conn.ExecuteSQL(sql);
}

	string[] FlowActions = new string[] {"In_AppBtn_SendFlow","In_AppBtn_Signoff","In_AppBtn_Vote"};
	//有 Allowed Workflow 才掛 FlowActions
	sql = "select id from [Allowed_Workflow] where source_id='" + this.getID() + "'";
	Aras.Server.Core.InnovatorDataSet restulDataSet = conn.ExecuteSelect(sql);
	if(restulDataSet.RowsNumber>0)
	{
		for(int i=0;i<FlowActions.Length;i++)
		{
			aml = "<AML>";
			aml += "	<Item type='Item Action' action='@action'>";
			aml += "		<related_id><Item type='Action' action='get' select='id'><name>" + FlowActions[i] + "</name></Item></related_id>";
			aml += "		<source_id>" + this.getID() + "</source_id>";
			aml += "	</Item>			";
			aml += "</AML>";
			Item itmAction = inn.applyAML(aml.Replace("@action","get"));
			if(itmAction.isError())
			{			
				itmAction = inn.applyAML(aml.Replace("@action","add"));
			}
		}
	}
	
	
    //ViewActions
	string[] ViewActions = new string[] {"In_AppBtn_ArasView"};
	for(int i=0;i<ViewActions.Length;i++)
	{
		aml = "<AML>";
		aml += "	<Item type='Item Action' action='@action'>";
		aml += "		<related_id><Item type='Action' action='get' select='id'><name>" + ViewActions[i] + "</name></Item></related_id>";
		aml += "		<source_id>" + this.getID() + "</source_id>";
		aml += "	</Item>			";
		aml += "</AML>";
		Item itmAction = inn.applyAML(aml.Replace("@action","get"));
		if(itmAction.isError())
		{			
			itmAction = inn.applyAML(aml.Replace("@action","add"));
		}
	}
	
	
	

	

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return inn.newResult("已更新完成");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_EnableAppForItem' and [Method].is_current='1'">
<config_id>F42366600AA248A198876E8B201FF9B1</config_id>
<name>In_EnableAppForItem</name>
<comments>讓Itemtype可以用於APP環境</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
