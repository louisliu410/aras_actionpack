ChooseCMItemDialog = function (itemTypeName)
{
	document.isEditMode = true;
	this.ItemTypeName = itemTypeName;
	//this.ChangeItemList = top.aras.newArray("PR", "ECN", "ECR", "Simple ECO", "Simple MCO", "Express ECO", "Express DCO");
	this.ChangeItemList = top.aras.newArray("Simple ECO", "Express DCO");
	this.ChangeTypeInput = document.querySelector("select[name='change_type_input']");
	this.ItemInput = document.querySelector("select[name='item_input']");

	var preferences = this.GetDefaultChangesPreferences();
	var defaultChangeType = "ECR";
	switch(itemTypeName)
	{
		case "Document":
			defaultChangeType = preferences["change_type_document"];
			break;
		case "CAD":
			defaultChangeType = preferences["change_type_cad_document"];
			break;
		case "Part":
			defaultChangeType = preferences["change_type_part"];
			break;
	}
    
	this.PopulateChangeType(defaultChangeType);
	this.PopulateChangeItem();
}

ChooseCMItemDialog.prototype.PopulateChangeType = function ChooseCMItemDialog_PopulateChangeType(defaultChangeType)
{
	this.ChangeTypeInput.remove(0);
	
	for(var i = 0; i < this.ChangeItemList.length; i++)
	{ 
		var changeItemName = this.ChangeItemList[i];
		if(this.ItemTypeName == "Part" && changeItemName == "Express DCO")
			continue;
		
		if((this.ItemTypeName == "Document" || this.ItemTypeName == "CAD") && (changeItemName == "Express ECO" || changeItemName == "Simple MCO"))
			continue;  

		//Modified by BCS Upgrade Team 2014/6/11
		if(this.ItemTypeName == "SAD" && changeItemName == "Express DCO")
			continue;
		    
		var option = this.AddOptionToDropDown(this.ChangeTypeInput, changeItemName);
		if(defaultChangeType == changeItemName)
			option.selected = true;
	}
	top.aras.updateDomSelectLabel(this.ChangeTypeInput);
}

ChooseCMItemDialog.prototype.PopulateChangeItem = function ChooseCMItemDialog_PopulateChangeItem()
{
	var numberOfDisplayedChangeItems = 5;
	//var selectedType = this.ChangeTypeInput[this.ChangeTypeInput.selectedIndex].text;
	//debugger;
	var selectedType = this.ChangeTypeInput[this.ChangeTypeInput.selectedIndex].id;
	var names = this.GetLatestCreatedChangeItems(selectedType,5);
	
	while(this.ItemInput.length != 0)
	{
		this.ItemInput.remove(0);
	}
	
	this.AddOptionToDropDown(this.ItemInput, "create", top.aras.getResource("PLM", "pe_change_management.create_new"));
	
	for(var i = 0; i < names.length && i < numberOfDisplayedChangeItems; i++)
	{
		this.AddOptionToDropDown(this.ItemInput, names[i]);  
	}
	
	if(names.length > numberOfDisplayedChangeItems)
		this.AddOptionToDropDown(this.ItemInput, "search", top.aras.getResource("PLM", "pe_change_management.search"));
	top.aras.updateDomSelectLabel(this.ItemInput);	
}

ChooseCMItemDialog.prototype.GetLatestCreatedChangeItems = function ChooseCMItemDialog_GetLatestCreatedChangeItems(typeName, count)
{
	var aml = '' +
	'<AML>' +
	'  <Item type="' + typeName + '" action="get" select="keyed_name" pagesize="' + count + 1 + '" page="1" orderBy="created_on desc"/>' +
	'</AML>';
	
	var resultXml = top.aras.applyAML(aml);
	var resultDom = top.aras.createXMLDocument();
	resultDom.loadXML(resultXml);
	
	var nodes = resultDom.selectNodes("//Item/keyed_name");
	var chanteItemNames = new Array();
	
	for(var i = 0; i < nodes.length; i++)
	{
		chanteItemNames.push(nodes[i].text);
	}
	return chanteItemNames;
}



ChooseCMItemDialog.prototype.AddOptionToDropDown = function ChooseCMItemDialog_AddOptionToDropDown(dropdown, optionId, optionName)
{
	debugger;
	if(this.ChangeTypeInput==dropdown)
	{
		var q = top.aras.getItemByKeyedName("ItemType",optionId);
		optionName = top.aras.getItemProperty(q,"label");
	}
	var newOption = document.createElement("option");
	if(!optionName)
		optionName = optionId;
	
	newOption.id = optionId;
	newOption.text = optionName;
	dropdown.add(newOption);
	return newOption;
}

ChooseCMItemDialog.prototype.GetDefaultChangesPreferences = function ChooseCMItemDialog_GetDefaultChangesPreferences()
{
	var preferencesAml = '' +
	'<AML>' +
	'<Item type="PE_MainPreferences" action="get" select="change_type_part,change_type_document,change_type_cad_document">' +
	'  <source_id>' +
	'    <Item type="Preference" action="get">' +
	'      <identity_id>' +
	'        <Item type="Identity" action="get">' +
	'          <name>World</name>' +
	'        </Item>' +
	'      </identity_id>' +
	'    </Item>' +
	'  </source_id>' +
	'</Item>' +
	'</AML>';
	
	var preferenceXml = top.aras.applyAML(preferencesAml);
	var preferenceDom = top.aras.createXMLDocument();
	preferenceDom.loadXML(preferenceXml);
	preferenceDom = preferenceDom.documentElement;
	
	this.PreferenceItem = preferenceDom;
	
	var result = top.aras.newObject();
	
	result["change_type_part"] = top.aras.getItemProperty(preferenceDom, "change_type_part", "ECR");
	result["change_type_document"] = top.aras.getItemProperty(preferenceDom, "change_type_document", "ECR");
	result["change_type_cad_document"] = top.aras.getItemProperty(preferenceDom, "change_type_cad_document", "ECR");
	
	return result;
}

ChooseCMItemDialog.prototype.Ok = function ChooseCMItemDialog_Ok()
{
	var result = top.aras.newObject();
	var selectedId = this.ItemInput[this.ItemInput.selectedIndex].id;
	//result.type = this.ChangeTypeInput[this.ChangeTypeInput.selectedIndex].text;
	result.type = this.ChangeTypeInput[this.ChangeTypeInput.selectedIndex].id;
	
	switch(selectedId)
	{
		case "create":
			result.action = selectedId;
			break;
		case "search":
			result.action = selectedId;
			break;
		default:
			result.name = selectedId;
			result.action = "open";
			break;
	}
	//Modify by kenny 2016/03/16 ------
	//top.returnValue = result;
	//top.window.close();
	parent.args.dialog.result = result;
	parent.args.dialog.close();	
	//---------------------------------
}
ChooseCMItemDialog.prototype.Cancel = function ChooseCMItemDialog_Cancel()
{
	//Modify by kenny 2016/03/16 ------
	//top.window.close();
	parent.args.dialog.close();
	//---------------------------------
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_ChooseCMItem_API' and [Method].is_current='1'">
<config_id>B3D6E181DCE04064A22DFB6E0B8FAAEC</config_id>
<name>PE_ChooseCMItem_API</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
