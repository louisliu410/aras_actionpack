string userId = this.getID();
if (String.IsNullOrEmpty(userId))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_FailedGetSharedForumsIdNotSet"));
}
List<string> identityIds = GetIdentitiesForUser(userId);
Item forums = GetForumsSharedWithIdentities(identityIds);
if (forums.isError())
{
	throw new Exception(String.Format(CultureInfo.InvariantCulture, CCO.ErrorLookup.Lookup("SSVC_FailedGetSharedForumsNoForums"), userId));
}
else
{
	return forums;
}
}

private Item GetForumsSharedWithIdentities(List<string> identities)
{
	Item forums = this.newItem("Forum", "get");
	forums.setProperty("forum_type", "Regular");
	forums.setProperty("state", "Active");
	Item sharedWith = this.newItem("ForumSharedWith", "get");
	sharedWith.setProperty("shared_with_id", String.Join(", ", identities.ToArray()));
	sharedWith.setPropertyCondition("shared_with_id", "in");
	Item wantViewBy = this.newItem("ForumWantViewBy", "get");
	Item mustViewBy = this.newItem("ForumMustViewBy", "get");
	forums.addRelationship(sharedWith);
	forums.addRelationship(wantViewBy);
	forums.addRelationship(mustViewBy);
	return forums.apply();
}

private List<string> GetIdentitiesForUser(string userId)
{
	Item identitiesIds = this.newItem("user", "VC_GetAllUserIdentities");
	identitiesIds.setID(userId);
	identitiesIds = identitiesIds.apply();
	List<string> result = identitiesIds.getResult().Split('|').ToList();
	return result;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_GetForumsSharedWithUser' and [Method].is_current='1'">
<config_id>C5AF4353E257489C9742A12F29964D06</config_id>
<name>VC_GetForumsSharedWithUser</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
