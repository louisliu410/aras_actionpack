Item res = null;

var adminIdentity = Aras.Server.Security.Identity.GetByName("Administrators");
bool adminPermsWasSet = Aras.Server.Security.Permissions.GrantIdentity(adminIdentity);
try
{
	res = Aras.ES.ServerMethods.Search(this);
}
finally
{
	if (adminPermsWasSet)
	{
		Aras.Server.Security.Permissions.RevokeIdentity(adminIdentity);
	}
}

return res;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_Search' and [Method].is_current='1'">
<config_id>B578299243B24A62AE10AEE95EA5F58E</config_id>
<name>ES_Search</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
