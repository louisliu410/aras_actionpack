'Create Date: 2008/02/12, Creator: Scott
'Modified Date: 2008/10/15, Modified By: Scott

'System.Diagnostics.Debugger.Break() 

Dim sql_str As String = Me.getProperty("SQLCMD")
Dim inn As Innovator = Me.getInnovator()
Dim err As Item

Dim result As Item = inn.applySQL(sql_str)

'2008/10/15: 修改判斷錯誤的條件
If (result.isError() = True) And (result.getItemCount() = -1) Then
	err = inn.newError(result.getErrorDetail())
	err.setErrorString(result.getErrorString())
	Return err
End If

Return result
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ExecuteSQL' and [Method].is_current='1'">
<config_id>5564F5E0FA6B4FB3B1BF545586523090</config_id>
<name>ExecuteSQL</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
