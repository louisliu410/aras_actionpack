/*
 * Workflow Map : BCS Simple ECO Workflow
 * Activity : Execute change
 * Server Events : On Close
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	string strWFDocName = "Simple ECO";
	string strRelationshipName = "Simple ECO Affected Item";
	string strAffectedItem = "Affected Item";
	//{ { "Part BOM", "" } } or { { "Part BOM", "BOM Substitute" }, { "Part Alternate", "" } }
	string[,] strArrRelationship = { { "Part BOM", "BOM Substitute" } };
	string strPartState = "Released,Temp-Released";
	int intLevel = 10;
	string strPropertyName = "item_number";
	
	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.CheckChildPartStateViaWorkflow(this, strWFDocName, strRelationshipName, strAffectedItem, strArrRelationship, strPartState, intLevel, strPropertyName);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_CheckPartStateSimpleECO' and [Method].is_current='1'">
<config_id>D5BA486E138A4FF4BD0E1070E61A0291</config_id>
<name>bcs_CheckPartStateSimpleECO</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
