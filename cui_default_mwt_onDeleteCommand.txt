var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onDeleteCommand) {
	workerFrame.onDeleteCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onDeleteCommand' and [Method].is_current='1'">
<config_id>C30383E2BD794488A63F924313C62BF3</config_id>
<name>cui_default_mwt_onDeleteCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
