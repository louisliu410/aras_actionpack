var xpath = "//Item/Relationships/Item/related_id/Item[@id='" + relatedID + "']";
var relitm = item.selectSingleNode(xpath);
if (relitm == null) {
   return
}

if (relitm.getAttribute('type')=="Audit") {
   var plantid=top.aras.getItemProperty(item,"corp_plant");
   top.aras.setItemProperty(relitm,'corp_plant',plantid);
}

if (relitm.getAttribute('type')=="NCR") {
   var partid=top.aras.getItemProperty(item,"part");
   top.aras.setItemProperty(relitm,'part',partid);
   
   var prtdescid=top.aras.getItemProperty(item,"part_description");
   top.aras.setItemProperty(relitm,'part_description',prtdescid);
   
   var plantid=top.aras.getItemProperty(item,"corp_plant");
   top.aras.setItemProperty(relitm,'corp_plant',plantid);
}

if (relitm.getAttribute('type')=="QS Issue") {
   var partid=top.aras.getItemProperty(item,"part");
   top.aras.setItemProperty(relitm,'part',partid);
   
   var prtdescid=top.aras.getItemProperty(item,"part_description");
   top.aras.setItemProperty(relitm,'part_description',prtdescid);
   
   var corpbuid=top.aras.getItemProperty(item,"corp_bu");
   top.aras.setItemProperty(relitm,'corp_bu',corpbuid);
   
   var corpplantid=top.aras.getItemProperty(item,"corp_plant");
   top.aras.setItemProperty(relitm,'corp_plant',corpplantid);
   
   var customerid=top.aras.getItemProperty(item,"customer");
   top.aras.setItemProperty(relitm,'customer_id',customerid);
}

if (relitm.getAttribute('type')=="QS Logbook") {
   var qslogprtid=top.aras.getItemProperty(item,"part");
   top.aras.setItemProperty(relitm,'part',qslogprtid);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='QS Quality Event OnInsert' and [Method].is_current='1'">
<config_id>E1A8406B889F443492A966B9A12CC16F</config_id>
<name>QS Quality Event OnInsert</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
