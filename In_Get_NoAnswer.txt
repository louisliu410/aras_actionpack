/*

主要用來應付需要Mapper功能但卻不需要抓取資料的場合。
Mapper改良後應該就不會再需要此方法。
*/

Item passengerParams=this.apply("In_Collect_PassengerParam");
int passengerAmt=passengerParams.getItemCount();
for(int b=0;b<passengerAmt;b++){
    Item passenger=passengerParams.getItemByIndex(b);
    this.setProperty(passenger.getProperty("Inn_ParamLabel",""),passenger.getProperty("Inn_ParamValue",""));
    this.addRelationship(passenger);    
}



return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_NoAnswer' and [Method].is_current='1'">
<config_id>D1937D32392F4208AEE0AB22AD5CFA62</config_id>
<name>In_Get_NoAnswer</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
