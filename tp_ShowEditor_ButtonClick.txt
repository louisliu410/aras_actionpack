if (aras.isTempEx(aras.getMostTopWindowWithAras(window).item)) {
	return; // this is (highly) not necessary because button is hidden
}

inArgs.switchToTabId = 'editor_container';
inArgs.enabledButtonImage = '../images/TechDocEditorOn.svg';
inArgs.disabledButtonIds = ['show_form'];
inArgs.disabledButtonIcons = ['../images/ShowFormOff.svg'];
aras.evalMethod('cui_ShowTab_ButtonClick', '', inArgs);

// method is called manually from non-CUI code, so send event manually too
var evnt = document.createEvent('Event');
evnt.locationName = 'ItemWindowSidebar';
evnt.changeType = 'click';
evnt.commandBarTarget = 'tp_show_editor';
evnt.initEvent('commandBarChanged', true, false);
document.dispatchEvent(evnt);

return {
	'cui_event_sent': true,
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_ShowEditor_ButtonClick' and [Method].is_current='1'">
<config_id>A12F21EBA44747B1BBEE6E4C87343A29</config_id>
<name>tp_ShowEditor_ButtonClick</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
