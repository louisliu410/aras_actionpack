this.value = this.value.toUpperCase();

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Field ToUpperCase' and [Method].is_current='1'">
<config_id>4CA4B4070A5D4B61BB255AC7C5B83D2A</config_id>
<name>Field ToUpperCase</name>
<comments>attach this as a Form Field Event</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
