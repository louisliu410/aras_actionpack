if (this.computeCorrectControlState) {
	var cmd;
	var isUnlockEnabled = this.getUnlockFlg();
	var isLockEnabled = this.computeCorrectControlState('lock');
	if (isUnlockEnabled) {
		cmd = 'unlock';
	} else if (isLockEnabled) {
		cmd = 'lock';
	}

	if (cmd) {
		this.processCommand(cmd);
		this.focus();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_iwrs_ctrl_l' and [Method].is_current='1'">
<config_id>45A65C509B6F49BD98C556B37BEE331C</config_id>
<name>cui_default_iwrs_ctrl_l</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
