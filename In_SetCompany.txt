/*
目的:自動帶出CREATOR 的 Company
位置: client onAfterNew
*/
//can be used on any Item Type with the 
//"OnAfterNew" Event (Client Events tab)
//will set the owned_by_id to the current user's id (creator)
//debugger;
if("" === this.getProperty("in_company","")){

    var aliasId = this.getInnovator().getUserAliases();
    if(aliasId)
    {
        aliasId = aliasId.substr(0,32);
        var alias = this.getInnovator().getItemById("Identity",aliasId);
        if(alias.getItemCount()==1){

            this.setProperty("in_company",alias.getProperty("in_company"));
        }
    }
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetCompany' and [Method].is_current='1'">
<config_id>AEBBC77DFEED4FB19EA6510238231BDD</config_id>
<name>In_SetCompany</name>
<comments>自動帶出CREATOR 的 Company</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
