Innovator inn = this.getInnovator();

string emails = this.getPropertyAttribute("body", "emails");
string subject = this.getPropertyAttribute("body", "subject");
string body = this.getPropertyAttribute("body", "body");

if (string.IsNullOrEmpty(emails) || string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(body))
{
	return inn.newError("Invalid input parameters. Can't be null or empty");	
}

Item query = this.newItem("User", "get");
query.setID(this.getPropertyAttribute("body", "userid"));
query.setAttribute("select", "email");
query = query.apply();
if (query.isError())
{
	return query;
}

string fromEmail = query.getProperty("email");
if (string.IsNullOrEmpty(fromEmail))
{
	return inn.newError("E-mail address is not specified");	
}

var emailsList = emails.Split(',');
foreach (string email in emailsList)
{
	System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(fromEmail, email, subject, body);
	msg.IsBodyHtml = true;
	CCO.Email.SetupSmtpMailServerAndSend(msg);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='TR_SendInvitesToPartners' and [Method].is_current='1'">
<config_id>6A8D59E546A1457F915724809DF25833</config_id>
<name>TR_SendInvitesToPartners</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
