/*
 * Lifrcycle : SAF Lifecycle
 * Life Cycle Transition : to Preliminary
 * Server Events : Before
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	//{ { "From State 1", "To State 1" }, { "From State 2", "To State 2" } }
	string[,] strArrSADState = { { "In Review", "Preliminary" } };
	string[,] strArrPartState = { { "In Review", "Preliminary" } };
	
	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.SAFPromoteToPreliminary(this, strArrSADState, strArrPartState);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_SAFPromoteToPreliminary' and [Method].is_current='1'">
<config_id>68A6E7986D6047B2A887ACCC40EF34E2</config_id>
<name>bcs_SAFPromoteToPreliminary</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
