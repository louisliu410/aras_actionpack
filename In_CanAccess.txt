//判斷目前的使用者對於物件本體是否有權限
/*
用法:
context.setAttribute("check_action","Access的Attribute"); //can_update, can_delete.....
Item itmCanAccess = context.apply("In_CanAccess"); //回傳 yes, no
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string action = this.getAttribute("check_action"); //can_update, can_delete.....
if(action=="can_update")
{
	//如果是can update 則直接以 lock unlock 來驗證
	try{
		this.lockItem();
		this.unlockItem();
		return inn.newResult("yes");
	}
	catch(Exception ex)
	{
		throw new Exception(_InnH.Translate("可鎖定的使用者才能建立新版本"));
		return inn.newResult("no");
	}
}



string identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;
string sql = "";
string strPermissionID = this.getProperty("permission_id", "");
if (strPermissionID == "")
{
	sql = "select permission_id from [" + this.getType().Replace(" ", "_") + "] where id='" + this.getID() + "'";
	Item itmContext = inn.applySQL(sql);
	strPermissionID = itmContext.getProperty("permission_id", "");
}

if (!identity_list.Contains("'"))
{
	identity_list = identity_list.Replace(" ", "");
	identity_list = identity_list.Replace(",", "','");
	identity_list = "'" + identity_list + "'";
}

//處理 Owner, Manager, Creator,不使用 Replace是因為如果Access內同時有指定Identity名稱與Owner, 可能會被覆蓋掉
//Owner: 如果有自己正好是Owner,則補上 538B300BB2A347F396C436E9EEE1976C 
if (identity_list.Contains("'" + this.getProperty("owned_by_id", "") + "'"))
{
	identity_list = identity_list+ ",'538B300BB2A347F396C436E9EEE1976C'";
}


//Manager: 如果有自己正好是Manager,則補上 9200A800443E4A5AAA80D0BCE5760307 
if (identity_list.Contains("'" + this.getProperty("managed_by_id", "") + "'"))
{
	identity_list = identity_list + ",'9200A800443E4A5AAA80D0BCE5760307'";
}

//Creator:  如果有自己正好是Creator,則補上 8FE5430B42014D94AE83246F299D9CC4 

if (inn.getUserID()==this.getProperty("created_by_id", ""))
{
	identity_list = identity_list + ",'8FE5430B42014D94AE83246F299D9CC4'";
}



sql = "select * from [Access] where source_id='" + strPermissionID + "' and [" + action + "]=1 and related_id in(" + identity_list + ")";
Item itmAccess = inn.applySQL(sql);
if (itmAccess.getResult() == "")
	return inn.newResult("no");
else
	return inn.newResult("yes");


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CanAccess' and [Method].is_current='1'">
<config_id>D427969C65A04ECA8323EFE2B3058C45</config_id>
<name>In_CanAccess</name>
<comments>判斷目前的使用者對於物件本體是否有權限</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
