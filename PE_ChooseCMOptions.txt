//Action Pack5 Modified On: 2016/03/18

var formNd = top.aras.getItemByName("Form", "PE_ChooseCMOptions", 0);

if (formNd)
{
  var param = new Object();
  param.title = "Choose CM Options";
  param.formId = formNd.getAttribute("id");
  param.aras = top.aras;

  var width = top.aras.getItemProperty(formNd, "width");
  var height = top.aras.getItemProperty(formNd, "height");
  
  //Modify by kenny 2016/03/18 ----
  width=parseInt(width)+20;
  height=parseInt(height)+30;
  //--------------------------------

  var options = {
    dialogWidth: width,
    dialogHeight: height
  };
  top.aras.modalDialogHelper.show('DefaultPopup', top.aras.getMainWindow().main, param, options, 'ShowFormAsADialog.html');
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_ChooseCMOptions' and [Method].is_current='1'">
<config_id>62B796EB90B448D4B342BE3136E09437</config_id>
<name>PE_ChooseCMOptions</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
