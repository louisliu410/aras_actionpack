if (aras.getItemProperty(item, 'is_alias') == '1') {
	if (!aras.confirm(aras.getResource('', 'com.aras.innovator.core.on_member_insert_warning'))) {
		deleteRelationship();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnMemberInsert' and [Method].is_current='1'">
<config_id>36F656B8D6C54A0D840E494AA8DDA572</config_id>
<name>OnMemberInsert</name>
<comments>warning when adding a member to an Alias Identity </comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
