/*
目的:APPAction:簽審記錄按鈕
用法:item.apply
須回傳:
app action
(若回傳 null 則代表不顯示按鈕)

位置:App Action的method
*/
string strMethodName = "In_AppBtn_Signoff";

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strError = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmAction = null;
//有流程
if(this.getProperty("in_wfp_id","")!="")
{
	itmAction = inn.newItem();
	itmAction.setType("action");
	itmAction.setProperty("href","");
	itmAction.setProperty("btn_type","report");
	string strClick= "ih.GoToItemPage('" + this.getType() + "','" + this.getID() + "','SignoffView.html','In_GetSignOff_N',{'workflow_process_id':'" + this.getProperty("in_wfp_id","") + "'})";
	itmAction.setProperty("click",strClick);	
	itmAction.setProperty("label","紀錄");
    itmAction.setProperty("class","");
    itmAction.setProperty("data_toggle","");
    itmAction.setProperty("data_target","");
	
}


return itmAction;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AppBtn_Signoff' and [Method].is_current='1'">
<config_id>8F4A3269DCC94D6EB301CD3ADE71A6EF</config_id>
<name>In_AppBtn_Signoff</name>
<comments>APPAction:簽審記錄按鈕</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
