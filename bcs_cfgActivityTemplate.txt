//debugger;

//Modify by kenny 2016/02/27

var xpath = "//Item[@id='" +inArgs.itemSelectedID +  "']/itm_workflow_map/@keyed_name";
var WorkflowKeyedName =inArgs.itemContext.selectSingleNode(xpath).text;
var source_id=WorkflowKeyedName;

var inn = this.getInnovator();

var aml = "<AML>"+
	"<Item type='Workflow Map Activity' action='get' select='related_id'>" +
	"<source_id><Item type='Workflow Map' action='get' select='id'>"+
	"<keyed_name>" + source_id + "</keyed_name>"+
	"<is_current>1</is_current>"+
	"</Item></source_id>"+
	"</Item></AML>";

var res = inn.applyAML(aml);

//var r = res.getItemsByXPath("//Item[@Type='Activity Template']");

var count = res.getItemCount();

var idArray = new Array();
for (i=0; i < count; i++) {
  var item = res.getItemByIndex(i);
  //idArray[i]=item.getRelatedItemID();
  idArray[i]=item.getProperty("related_id","");
}

//USE THE JOIN FUNCTION TO CONVERT THE ARRAY TO A COMMA DELIMITED
//STRING
var idlist=idArray.join(",");

//***SET THE idlist ATTRIBUTE ON THE QRYITEM TO THE LIST
inArgs.QryItem.item.setAttribute("idlist", idlist);

//Modify by kenny 2016/02/27 ---
//return inArgs;
//------------------------------

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_cfgActivityTemplate' and [Method].is_current='1'">
<config_id>0EA0DFA0B2344C0F91A90CC7B75E0D5F</config_id>
<name>bcs_cfgActivityTemplate</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
