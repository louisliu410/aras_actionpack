var topWindow = aras.getMostTopWindowWithAras(window);
var treeFrame = topWindow.tree;
if (treeFrame && treeFrame.SavedSearchInToc) {
	var ssInToc = new treeFrame.SavedSearchInToc(inArgs.rowId);
	ssInToc.delSavedSearch();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmtoc_delSavedSearch' and [Method].is_current='1'">
<config_id>F7C4707C74C04F29A29C4485E3E1BFA4</config_id>
<name>cui_default_pmtoc_delSavedSearch</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
