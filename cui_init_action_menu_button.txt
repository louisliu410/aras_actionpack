var hidden = true;
var canExecute = true;
if (inArgs.control && inArgs.control.additionalData) {
	var additionalData = JSON.parse(inArgs.control.additionalData);
	switch (additionalData.actionType || additionalData.separatorType) {
		case 'generic':
			hidden = false;
			break;
		case 'itemtype':
			if (inArgs.itemTypeId) {
				hidden = false;
			}
			break;
		case 'item':
			if (inArgs.itemId) {
				hidden = false;
			}
			break;
		default:
			break;
	}

	canExecute = aras.canInvokeActionImpl(additionalData.canExecuteMethodName, additionalData.location);
}

return {'cui_invisible': hidden, 'cui_disabled': !canExecute};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_init_action_menu_button' and [Method].is_current='1'">
<config_id>30977B697C514ED1A56ED3492EB8971D</config_id>
<name>cui_init_action_menu_button</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
