'Create Date: 2009/05/27, Creator: Scott
'Modified Date: 2010/08/09, Modified By: Scott

'Purpose: TimeZone1 - TimeZone2 之間差距幾分鐘

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim time_zone1 As String = Me.getProperty("TimeZone1")
Dim time_zone2 As String = Me.getProperty("TimeZone2")

'In 9.2.0 Aras.Server.CoreCS.DateTimeConvert.OffsetBetweenTimeZones 不存在
'Dim offset As Integer = Aras.Server.CoreCS.DateTimeConverter.OffsetBetweenTimeZones(time_zone1, time_zone2, DateTime.UtcNow)
Dim offset As Integer = Aras.I18NUtils.DateTimeConverter.OffsetBetweenTimeZones(time_zone1, time_zone2, DateTime.UtcNow)
Return inn.newResult(offset.toString())
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='DateTimeOffsetBetweenTimeZones' and [Method].is_current='1'">
<config_id>46594B5BDCB640FB8C174E0BDB17ABB5</config_id>
<name>DateTimeOffsetBetweenTimeZones</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
