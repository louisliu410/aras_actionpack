var control = inArgs.control;
if (control && control['_item_Experimental'] && control['_item_Experimental'].ownerDocument) {
	var context = control['_item_Experimental'].ownerDocument.defaultView;
	if (context && context.onSaveAndCloseCommand) {
		return context.onSaveAndCloseCommand();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twt_save_close' and [Method].is_current='1'">
<config_id>52B298D31F124090835F869F0EF24EB3</config_id>
<name>cui_default_twt_save_close</name>
<comments>Structure Toolbar Return selected Button</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
