'inDom format:
'<Item type='Method' action='server_update_schedule'><body project_id='project_id_value'/></Item>

Dim inn As Innovator = Me.getInnovator()

Dim body As XmlElement = CType(Me.dom.selectSingleNode("//Item[@type='Method']/body"), XmlElement)
Dim id As String = ""
If Not IsNothing(body) Then id = body.getAttribute("project_id")
If id = "" Then Return inn.newError("No project id is specified.")

'+++ permissions check +++
Dim typeID As String = ""
Dim q As Item = Me.newItem("ItemType", "get")
q.setAttribute("select", "id")
q.setProperty("name", "Project")
Dim r As Item = q.apply()
If r.getItemCount() < 1 Then
	Return inn.newError("Cannot find Project Item Type.")
Else
	typeID = r.getItemByIndex(0).getID()
End If

Dim tmpResponseDom As New XmlDocument()
If Not CCO.Permissions.GetPermissions(id, typeID, "can_update", tmpResponseDom) Then _
	Return inn.newError("Insufficient permissions to update Project item.")
'--- permissions check ---

Dim tzName As String = CCO.Utilities.GetVarValue("CorporateTimeZone", "")
If tzName = "" Then _
	Return inn.newError("CorporateTimeZone Variable is not set.")

Dim callframe As Item = Me.newItem("SQL", "SQL PROCESS")
callframe.setproperty("name", "update_schedule")
callframe.setproperty("PROCESS","CALL")
callframe.setproperty("ARG1", id)
callframe.setproperty("ARG2", tzName)

Dim resultframe As Item = callframe.apply()
If resultframe.isError() Then Return(resultframe)

Dim result_text As String = resultframe.getproperty("result_text")
If result_text <> "SUCCESS" Then _
	Return inn.newError("Method server_update_schedule failed: " + result_text)

Return inn.newResult("SUCCESS")
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='server_update_schedule' and [Method].is_current='1'">
<config_id>7DC85B0668134E949B9212D7CE199265</config_id>
<name>server_update_schedule</name>
<comments>Calls the SQL code for updating a schedule on the server</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
