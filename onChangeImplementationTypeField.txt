/* Purpose is to show warning about: the PolyItem, when saved,
 drops the standard ItemType DB table in favor of a DB View,
 selecting an ItemType to be a polyitem cannot be reversed. */
var prevValueKey = 'implementation_type_previous_value_system';
var sysTa = document.getElementById(prevValueKey);
var prevValue = sysTa.value;
sysTa.value = this.value;

if (this.value == 'polymorphic' && this.value != prevValue) {
	var res = aras.confirm(aras.getResource('', 'imports_core.poly_cannot_be_reversed_to_non_poly'), window);
	if (!res) {
		handleItemChange('implementation_type', prevValue);
		sysTa.value = prevValue;
	}
}

if (prevValue == 'polymorphic' && this.value != prevValue && (document.item.getAttribute('isTemp') !== '1')) {
	aras.AlertError(aras.getResource('', 'imports_core.poly_cannot_be_reversed_to_non_poly_error'), window);
	handleItemChange('implementation_type', prevValue);
	sysTa.value = prevValue;
}

aras.evalItemMethod('ItemType Form onload', document.item);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onChangeImplementationTypeField' and [Method].is_current='1'">
<config_id>CC30E72FB0DF47E0AA2941997E8B0449</config_id>
<name>onChangeImplementationTypeField</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
