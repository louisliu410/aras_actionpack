// Called from the Simple MCO lifecycle. Promotes the Affected Part between Released 
//  and Manual Change states.

// Grant 'Aras PLM' permissions
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

string fromState = "Manual Change";
if (String.Equals(this.getProperty("state", ""), "New", StringComparison.Ordinal))
	fromState = "Released";
	
string toState = "Manual Change";
if (String.Equals(this.getProperty("state", ""), "In Review", StringComparison.Ordinal))
	toState = "Released";

Innovator innovator = this.getInnovator();

string aml = String.Format(CultureInfo.InvariantCulture, 
	"<AML>" +
	"  <Item type='Simple MCO Part' action='get' select='related_id(state)'>" + 
	"    <source_id>{0}</source_id>" + 
	"    <related_id condition='is not null'/>" + 
	"  </Item>" +
	"</AML>", 
	this.getAttribute("id"));
	
Item q = innovator.applyAML(aml);
if (q.isError() || q.isEmpty()) 
	return innovator.newError("No Affected Parts found");

for (int i = 0; i < q.getItemCount(); i++)
{
  Item affPart = q.getItemByIndex(i).getPropertyItem("related_id");
  if (!String.Equals(affPart.getProperty("state", ""), fromState, StringComparison.Ordinal))
    return innovator.newError("The Affected Part must be in " + fromState + " state");

  affPart.setAction("PromoteItem");
  affPart.setProperty("state", toState);
  affPart = affPart.apply();
  if (affPart.isError()) 
	  return affPart;
}

// Revoke 'Aras PLM' permissions
if (PermissionWasSet) 
	Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_MCOAffectedPartPromotion' and [Method].is_current='1'">
<config_id>1440A839F7734C899FAA6453F1F7615F</config_id>
<name>PE_MCOAffectedPartPromotion</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
