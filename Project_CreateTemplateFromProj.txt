var errorMsg = "";

if (top.aras.isTempEx(this.node) || top.aras.isDirtyEx(this.node) || top.aras.isNew(this.node)) {
	errorMsg = top.aras.getResource("Project", "project.cannot_be_cloned", "Project");
	top.aras.AlertError(errorMsg);
	return;
}

var clone = this.newItem();
clone.loadAML("<AML>" + this.node.xml + "</AML>");
clone.setAttribute("projectCloneMode", "CreateTemplateFromProject");
var newTemplate = clone.apply("Project_CloneProjectOrTemplate");
if (newTemplate.isError()) {
	 top.aras.AlertError(newTemplate.getErrorString(), newTemplate.getErrorDetail());
}
top.aras.uiShowItemEx(newTemplate.node);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Project_CreateTemplateFromProj' and [Method].is_current='1'">
<config_id>6F6309B004AF41668800D4665C8E78C3</config_id>
<name>Project_CreateTemplateFromProj</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
