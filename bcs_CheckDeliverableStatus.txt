string bcs_is_deliverable_released = this.getProperty("bcs_is_deliverable_released","0");
if(bcs_is_deliverable_released != "1")
	return this;

Innovator inn = this.getInnovator();
Item a2dels = inn.newItem("Activity2 Deliverable","get");
a2dels.setProperty("source_id",this.getID());
Item r = a2dels.apply();
if(r.getItemCount()<=0)
	return this;

string invalidateKeyedname="";
for(int i=0;i<r.getItemCount();i++)
{
	Item del = r.getItemByIndex(i).getRelatedItem();
	string state = del.getProperty("state","");
	if(state!="Released")
		invalidateKeyedname += del.getProperty("keyed_name","") + ",";
}

if(invalidateKeyedname!="")
{
	string errstring = "以下文件不是Released狀態:{0}請先發行文件後再完成本任務";
	Item um = inn.getItemByKeyedName("UserMessage","bcsAPV4_DeliverableIsNotReleased");
	if(!um.isError())
		errstring=um.getProperty("text","以下文件不是Released狀態:{0}請先發行文件後再完成本任務");
	errstring = String.Format(errstring,invalidateKeyedname);
	throw new Exception(errstring);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_CheckDeliverableStatus' and [Method].is_current='1'">
<config_id>4E5AB32F01D2402ABD097ADC5794514F</config_id>
<name>bcs_CheckDeliverableStatus</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
