XmlElement inItem = (XmlElement)this.dom.SelectSingleNode("//Item[@type='Method' and @action='GetItemsForStructureBrowser']/Item");
if (inItem != null)
{
    XmlDocument resDOM = CCO.XML.CreateXMLDocument();
    XmlElement result = CCO.XML.MakeBorders(ref resDOM, true);

    // MyStructureBrowser sb = new MyStructureBrowser(CCO.Variables.InnDatabase, CCO.Cache);
    Aras.Server.Core.StructureBrowser sb = new Aras.Server.Core.StructureBrowser(ref CCO);

    int levelsToSelect;
    if (!Int32.TryParse(inItem.GetAttribute("levels"), out levelsToSelect))
        levelsToSelect = 2;

    result.InnerXml = sb.GetItemsForStructureBrowser(inItem.GetAttribute("type"), 
        inItem.GetAttribute("id"), 
        inItem.GetAttribute("compareToITName"), 
        inItem.GetAttribute("compareToId"), 
        levelsToSelect);
    
    Aras.IOM.Item resItem = this.newItem("");
    resItem.loadAML(resDOM.OuterXml);
    return resItem;
}
else
    return null;


// StructureBrowser class can be overriden.
// Methods available for overriding: GetKeyedNameForItem, CompareItems.
// Just uncomment next code and change MyStructureBrowser to required.

//}
//    public class MyStructureBrowser : Aras.Server.Core.StructureBrowser
//    {
//      public MyStructureBrowser(Aras.Server.Core.InnovatorDatabase dbConnection, Aras.Server.Core.Cache cache) : base(dbConnection, cache)
//      {
//      }
//	
//      public string CompareItems(XmlElement itemType, string firstItemId, string compareToId, string levelsToSelect)
//      {
//        return "";
//      }
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetItemsForStructureBrowser' and [Method].is_current='1'">
<config_id>F47D87B2066F45F3A601C01D303581D1</config_id>
<name>GetItemsForStructureBrowser</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
