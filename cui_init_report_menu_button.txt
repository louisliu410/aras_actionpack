var hidden = true;
if (inArgs.control && inArgs.control.additionalData) {
	var additionalData = JSON.parse(inArgs.control.additionalData);
	switch (additionalData.reportType || additionalData.separatorType) {
		case 'generic':
			hidden = false;
			break;
		case 'ssr':
		case 'itemtype':
			if (inArgs.itemTypeId) {
				hidden = false;
			}
			break;
		case 'item':
			if (inArgs.itemId) {
				hidden = false;
			}
			break;
		default:
			break;
	}
}

return {'cui_invisible': hidden};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_init_report_menu_button' and [Method].is_current='1'">
<config_id>F4FD1A40EA0144898DB308DE45A3C5B5</config_id>
<name>cui_init_report_menu_button</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
